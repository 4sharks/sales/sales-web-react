// DropArea.jsx
import { useDrop } from 'react-dnd';

const DropArea = ({ children, onLabelDrop, index }) => {
  const [{ isOver }, drop] = useDrop({
    accept: 'LABEL',
    drop: (item, monitor) => {
      onLabelDrop(item, index);  // تمرير مؤشر الوجهة
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
    }),
  });

  return (
    <div ref={drop} className={`relative ${isOver ? 'bg-blue-100' : ''}`}>
      {children}
    </div>
  );
};

export default DropArea;