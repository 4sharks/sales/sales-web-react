import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';

const handleConvertToPDF = async ({
  elementId = 'printable-content',
  fileName = 'document.pdf',
  quality = 4
}) => {
  try {
    const element = document.getElementById(elementId);
    if (!element) {
      console.error(`Element with ID "${elementId}" not found`);
      return;
    }

    // التقاط الصورة مباشرة بدون تعديل الأنماط
    const canvas = await html2canvas(element, {
      scale: quality,
      useCORS: true,
      allowTaint: true,
      scrollX: 0,
      scrollY: 0,
      width: element.scrollWidth,
      height: element.scrollHeight,
      windowWidth: element.scrollWidth,
      windowHeight: element.scrollHeight,
      foreignObjectRendering: true,
      removeContainer: false,
      backgroundColor: '#FFFFFF',
      logging: false
    });

    const pdf = new jsPDF({
      orientation: 'portrait',
      unit: 'mm',
      format: 'a4',
      compress: true
    });

    const imgData = canvas.toDataURL('image/png', 1.0);

    const pageWidth = pdf.internal.pageSize.getWidth();
    const pageHeight = pdf.internal.pageSize.getHeight();

    // هوامش صغيرة جداً
    const margins = {
      top: 3,
      bottom: 3,
      left: 3,
      right: 3
    };

    const contentWidth = pageWidth - (margins.left + margins.right);
    const contentHeight = pageHeight - (margins.top + margins.bottom);

    const widthRatio = contentWidth / canvas.width;
    const heightRatio = contentHeight / canvas.height;
    const ratio = Math.min(widthRatio, heightRatio);

    const canvasWidth = canvas.width * ratio;
    const canvasHeight = canvas.height * ratio;

    let currentPosition = 0;
    while (currentPosition < canvasHeight) {
      if (currentPosition > 0) {
        pdf.addPage();
      }

      pdf.addImage(
        imgData,
        'PNG',
        margins.left,
        margins.top - currentPosition * ratio,
        canvasWidth,
        canvasHeight
      );

      currentPosition += contentHeight / ratio;
    }

    pdf.save(fileName);
  } catch (error) {
    console.error('Error generating PDF:', error);
    throw error;
  }
};

export default handleConvertToPDF;