import React from 'react'
import SalesReports from '../components/reports/SalesReports'
import {HiOutlineDocumentReport} from 'react-icons/hi'

const Reports = () => {
    return (
        <div className='flex flex-col  '>
            <div className='flex-1 flex gap-1 items-center  text-slate-500'>
                <HiOutlineDocumentReport size={18}/>
                <span>التقارير</span>
            </div>
            <SalesReports/>
        </div>
    )
}

export default Reports