import { useParams } from "react-router-dom";
 import { useCurrent, useFetch } from "../../hooks";
import withGuard from "../../utils/withGuard";
import { MwSpinner } from "../../components/ui";
import InvFormCreateInvoiceWithQuote from "../../components/quote/OsCreateInvoiceWithQuote/InvFormCreateInvoiceWithQuote";


const OsCreateInvoiceWithQuote = () => {
    const {currentTenantId,currentCompanyId,currentBranchId} = useCurrent();
    const params = useParams();
    const invID = params.id;
    const {data,loading,error} = useFetch(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/quotes/show/${params.id}`)
    if(!invID) return ;

    console.log('data',data);

    return (
        <div className="  w-full h-full">
            {!loading && !error && data ? <InvFormCreateInvoiceWithQuote
                tenantId = {currentTenantId}
                tenantUsername ={params.tenant}
                companySelected = {currentCompanyId}
                branchSelected = {currentBranchId}
                invData = {data}
            /> : <MwSpinner className='flex  items-center justify-center'/>
            }
        </div>
    )
}

export default withGuard(OsCreateInvoiceWithQuote)