import React, { useState , useEffect} from 'react'
import {getAllSalesman,getAllSalesmanTypes} from '../services/salesmanService';
import ListSalesmans from '../components/Salesmans/ListSalesmans'
import { useCurrent } from '../hooks';
import { MwButton } from '../components/ui';
import { Link, useParams, useLocation } from 'react-router-dom';
import {LiaUsersSolid} from 'react-icons/lia'
import {MdOutlineAddBox} from 'react-icons/md'
import {MdGroups2} from 'react-icons/md'
import {FaUsersBetweenLines} from 'react-icons/fa6'
import SalesmanTypesList from '../components/Salesmans/salesman-types/SalesmanTypesList';

const Salesmans = () => {
    const location = useLocation();
    const [tabActive,setTabActive] = useState(location.state?.activeTab || 'Salesman');
    
    const [salesmansTypes,setSalesmanTypes] = useState([]); 
    const [salesmans,setSalesman] = useState([]); 
    const [loading, setLoading] = useState(false);
    const { currentTenantId ,currentCompanyId,currentBranchId,currentLangId} = useCurrent();
    const params = useParams();

    const getSallesman = async () => {
        const _salesman = await getAllSalesman(currentTenantId ,currentCompanyId,currentBranchId)
        setSalesman(_salesman)
    } 
    const getSallesmanTypes = async () => {
        setLoading(true);
        try {
            const _salesmanTypes = await getAllSalesmanTypes(currentTenantId ,currentCompanyId,currentBranchId);
            setTimeout(() => {
                setSalesmanTypes(_salesmanTypes);
                setLoading(false);
            }, 500);
        } catch (error) {
            console.error('Error fetching salesman types:', error);
            setTimeout(() => {
                setLoading(false);
            }, 500);
        }
    } 

    useEffect(() => {
        getSallesman();
        getSallesmanTypes();
    },[]);

    return (
        <>
            <div className='flex justify-between items-center pb-3'>
                <div className=' flex gap-1 px-2 text-sm text-gray-400 '>
                    <LiaUsersSolid size={18} />
                    إدارةالمناديب </div>
                
            </div>
            <div id='Tabs' className='w-full flex mt-3 gap-3  border-b'>
                <div onClick={()=>setTabActive('Salesman')} className={`flex-1 flex flex-col gap-1 items-center justify-center rounded-t-lg text-slate-500  p-3 hover:bg-indigo-50 hover:font-bold cursor-default ${tabActive === 'Salesman' ? ' bg-indigo-100 text-indigo-800 font-bold' : 'bg-slate-50'}`}>
                    <FaUsersBetweenLines  size={28}/>
                    <hr />
                    <span className='text-sm '> المناديب </span>
                </div>
                <div onClick={()=>setTabActive('SalesmanType')} className={`flex-1 flex flex-col gap-1 items-center justify-center rounded-t-lg text-slate-500  p-3 hover:bg-indigo-50 hover:font-bold cursor-default ${tabActive === 'SalesmanType' ? 'bg-indigo-100 text-indigo-800 font-bold' : 'bg-slate-50'}`}>
                    <MdGroups2  size={28}/>
                    <hr />
                    <span className='text-sm  '>  انواع المناديب </span>
                </div>
            </div>
            <div className='text-slate-500 bg-slate-50 rounded-b-lg text-xs p-3 py-5 '>
                { tabActive === 'Salesman' && 
                <div className=''>
                    <div className='flex flex-row-reverse'>
                        <Link to={`/${params.tenant}/salesmans/add`}>
                        <MwButton inGroup={true} type='saveBtn'  >
                            <MdOutlineAddBox size={18}/>
                            أضف مندوب جديد
                            </MwButton>
                        </Link>
                    </div>
                    <ListSalesmans 
                        data={salesmans} 
                        setData = {setSalesman}
                        currentCompanyId = {currentCompanyId} 
                        currentBranchId = {currentBranchId}
                        currentLangId = {currentLangId}
                        refreshResult = {getSallesman}
                        />
                </div>
                     }
                { tabActive === 'SalesmanType' && <div className=''>
                    <div className='flex flex-row-reverse'>
                        <Link to={`/${params.tenant}/salesmans/types/add`}>
                        <MwButton inGroup={true} type='saveBtn'  >
                            <MdOutlineAddBox size={18}/>
                            أضف نوع جديد
                            </MwButton>
                        </Link>
                    </div>
                    <SalesmanTypesList 
                        data={salesmansTypes} 
                        setData={setSalesmanTypes}
                        currentCompanyId={currentCompanyId} 
                        currentBranchId={currentBranchId}
                        currentLangId={currentLangId}
                        refreshResult={getSallesmanTypes}
                        loading={loading}
                    />
                </div> }
            </div>
        
            
        </>
    )
}

export default Salesmans