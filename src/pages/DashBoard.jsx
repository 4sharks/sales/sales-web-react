import React, { useEffect, useState } from 'react'
import axios from 'axios';
import {  useTranslation } from 'react-i18next';
import LineChart from '../components/charts/LineChart';
import DaugeChart from '../components/charts/DaugeChart';
import PieChart from '../components/charts/PieChart';
import TimeLine from '../components/timeline/TimeLine';
import SearchBar from '../components/SearchBar';
import { useCurrent } from '../hooks';
import InvListWidget from '../components/invoice/InvListWidget';
import QuoteListWidget from '../components/quote/QuoteListWidget';
import {AiOutlineDashboard} from 'react-icons/ai'
import { MwButton } from '../components/ui';
import {MdSupportAgent} from 'react-icons/md'
import { Link, useParams } from 'react-router-dom';
import QuoteListDashboard from '../components/quote/QuoteListDashboard';
import i18next from 'i18next';
const DashBoard = () => {
    const [t] = useTranslation('global')
    const params = useParams();
    const {currentSettings,currentTenantId,currentCompanyId,currentBranchId} = useCurrent();
    const [salesCurrentYear,setSalesCurrentYear] = useState(0);
    const date = new Date();
    const currentMonth = date.getMonth() + 1;
    const currentYeay = date.getFullYear();
    const _token = localStorage.getItem('token');

    // const Target Monthly
    const SETTING_TARGET_MONTHLY = currentSettings?.generalSettings?.TARGET_MONTHLY
    const [targetMonthyPercent,setTargetMonthlyPercent] = useState([0,0]);
    const [targetMonthyLabel,setTargetMonthlyLabel] = useState([0,0]);
    const [targetMonthyLoading,setTargetMonthlyLoading] = useState(true);
    // const Target Yearly
    const SETTING_TARGET_YEARLY = currentSettings?.generalSettings?.TARGET_YEARLY
    const [targetYearlyPercent,setTargetYearlyPercent] = useState([0,0]);
    const [targetYearlyLabel,setTargetYearlyLabel] = useState([0,0]);
    const [targetYearlyLoading,setTargetYearlyLoading] = useState(true);
    // const Paid invoices 
    const [paidInvoicePercent,setPaidInvoicePercent] = useState([0,0]);
    const [paidInvoiceLabel,setPaidInvoiceLabel] = useState([0,0]);
    const [paidInvoiceLoading,setPaidInvoiceLoading] = useState(true);
    // const Sales Months groups
    const [salesMonths,setSalesMonths] = useState([]);
    const [salesMonthsLoading,setSalesMonthsLoading] = useState(true);
    // const Salesman
    const [salesmanTotals,setSalesmanTotals] = useState([]);
    const [salesmanLabels,setSalesmanLabels] = useState([]);
    const [salesmanLoading,setSalesmanLoading] = useState(false);
    // const Product
    const [productTotals,setProductTotals] = useState([]);
    const [productLabels,setProductLabels] = useState([]);
    const [productLoading,setProductLoading] = useState(false);

    const targetMonthlyHandler = async () => {
        const res = await axios.get(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/invoices-dashboard/month/${currentTenantId}/${currentCompanyId}/${currentBranchId}/${currentMonth}`);
        const sum = res.data;
        const percentSum = (parseFloat(sum) / parseFloat(SETTING_TARGET_MONTHLY) ) * 100 ;
        setTargetMonthlyLoading(false)
        setTargetMonthlyPercent([percentSum,(100 - percentSum)])
        setTargetMonthlyLabel([parseFloat(sum),parseFloat(SETTING_TARGET_MONTHLY - sum)])
    }
    const targetYearlyHandler = async () => {
        const res = await axios.get(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/invoices-dashboard/year/${currentTenantId}/${currentCompanyId}/${currentBranchId}/${currentYeay}`);
            const sum = res.data;
            setSalesCurrentYear(sum);
            //salesCurrentYear = sum;
            const percentSum = (parseFloat(sum) / parseFloat(SETTING_TARGET_YEARLY) ) * 100 ;
            setTargetYearlyLoading(false)
            setTargetYearlyPercent([percentSum,(100 - percentSum)])
            setTargetYearlyLabel([parseFloat(sum),parseFloat(SETTING_TARGET_YEARLY - sum)])
    }
    const paidInvoiceHandler = async () => {
        const res = await axios.get(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/invoices-dashboard/paid/${currentTenantId}/${currentCompanyId}/${currentBranchId}/${currentYeay}/true`);
        const sum = res.data;
        if(salesCurrentYear > 0){
            const percentSum = (parseFloat(sum) / parseFloat(salesCurrentYear)) * 100 ;
            // setPaidInvoiceLoading(false)
            setPaidInvoicePercent([percentSum,(100 - percentSum)])
            setPaidInvoiceLabel([parseFloat(sum),parseFloat(salesCurrentYear - sum)])
        }
        setPaidInvoiceLoading(false)
        console.log('paid',sum,salesCurrentYear);
    }
    const salesMonthsHandler = async () => {
        const res = await axios.get(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/invoices-dashboard/month-group/${currentTenantId}/${currentCompanyId}/${currentBranchId}/${currentYeay}`);
        const sum = res.data;
        setSalesMonths(sum)
        setSalesMonthsLoading(false)
    }

    const processLabelsAndData = (labels, totals) => {
        if (!labels || !Array.isArray(labels)) return { validLabels: ['لا توجد بيانات'], validTotals: [0] };
        
        const currentLanguage = i18next.language || 'ar';
        const validLabels = [];
        const validTotals = [];
        
        labels.forEach((item, index) => {
            let label = '';
            
            if (typeof item === 'string') {
                try {
                    const parsed = JSON.parse(item);
                    if (Array.isArray(parsed)) {
                        if (parsed.length > 0) {
                            const firstItem = parsed[0];
                            if (firstItem[currentLanguage]) label = firstItem[currentLanguage];
                            else if (firstItem.ar) label = firstItem.ar;
                            else if (firstItem.en) label = firstItem.en;
                            else label = Object.values(firstItem)[0] || '';
                        }
                    } else {
                        if (parsed[currentLanguage]) label = parsed[currentLanguage];
                        else if (parsed.ar) label = parsed.ar;
                        else if (parsed.en) label = parsed.en;
                        else label = Object.values(parsed)[0] || '';
                    }
                } catch (e) {
                    label = item === 'null' ? '' : item;
                }
            }
            else if (typeof item === 'object' && item !== null) {
                if (item[currentLanguage]) label = item[currentLanguage];
                else if (item.ar) label = item.ar;
                else if (item.en) label = item.en;
                else label = Object.values(item)[0] || '';
            }
            
             if (label && label.trim() !== '') {
                validLabels.push(label);
                if (totals && totals[index] !== undefined) {
                    validTotals.push(totals[index]);
                } else {
                    validTotals.push(0);
                }
            }
        });
        
         if (validLabels.length === 0) {
            return { validLabels: ['لا يوجد مبيعات'], validTotals: [0] };
        }
        
        return { validLabels, validTotals };
    };
    const salesmanhsHandler = async () => {
        setSalesmanLoading(true)
        try {
            const res = await axios.get(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/invoices-dashboard/salesman/${currentTenantId}/${currentCompanyId}/${currentBranchId}/${currentYeay}`);
            const data = res.data;
            
            if (data && data.total && data.total.length > 0 && data.salesman && data.salesman.length > 0) {
                // نمرر البيانات للمعالجة
                const result = processLabelsAndData(data.salesman, data.total);
                setSalesmanTotals(result.validTotals);
                setSalesmanLabels(result.validLabels);
            } else {
                setSalesmanTotals([0]);
                setSalesmanLabels(['لا يوجد مبيعات']);
            }
            setSalesmanLoading(false);
        } catch (e) {
            setSalesmanLoading(false);
            console.error(e);
            setSalesmanTotals([0]);
            setSalesmanLabels(['لا يوجد مبيعات']);
        }
    }
 const productsHandler = async () => {
    setProductLoading(true)
    try {
        const res = await axios.get(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/invoices-dashboard/topProducts/${currentTenantId}/${currentCompanyId}/${currentBranchId}/${currentYeay}`);
        const data = res.data;
        setProductLoading(false)
        
         if (data && data.total && data.total.length > 0 && data.productName && data.productName.length > 0) {
            setProductTotals(data.total)
            setProductLabels(data.productName)
        } else {
             setProductTotals([0])
            setProductLabels(['لا توجد منتجات'])
        }
    } catch (e) {
        setProductLoading(false)
        console.error(e)
         setProductTotals([0])
        setProductLabels(['لا توجد منتجات'])
    }
}

    useEffect(()=>{
        if (currentTenantId && currentCompanyId && currentBranchId) {
        targetMonthlyHandler();
        targetYearlyHandler();
        salesMonthsHandler();
        salesmanhsHandler();
        console.log('SETTING_TARGET_MONTHLY',currentMonth,currentYeay);
         }
    },[salesCurrentYear])

    useEffect(() => {
        if (salesCurrentYear > 0) {
            paidInvoiceHandler();
            productsHandler();
        }
    }, [salesCurrentYear]);

console.log("salesmanLabels",salesmanLabels)


    return (
        <>
            <div className='flex flex-col  '>
                <div className='flex-1 flex gap-1 justify-between items-center p-2 text-slate-500'>
                    <div className='flex gap-1 items-center justify-center '>
                        <AiOutlineDashboard size={18}/>
                        <span className='pt-2'>

                        {t('menu.Dashboard')}
                        </span>
                    </div>
                    <Link  target='_blank'  to={`${process.env.REACT_APP_WEB_SUPPORT_BASE_URL}/${params.tenant}/token/${_token}`}> 
                        <MwButton classNameCustom='w-full' inGroup={true} type='saveBtn'  ><MdSupportAgent size={20}/>    {t('DashBoard.support')} </MwButton>
                    </Link>
                </div>
                <SearchBar/>
                <div className='flex flex-col  gap-1 '>
                    <div className='flex flex-col md:flex-row justify-between gap-1 md:w-75  '>
                        <LineChart datasetData = {salesMonths} loading={salesMonthsLoading} />
                    </div>
                    <div className=' flex flex-col md:flex-row justify-between  gap-1 pt-5  '>
                        <div className='flex-1 grid grid-cols-1 md:grid-cols-3 justify-center md:justify-between gap-4'>
                            <DaugeChart
                                labels={ [t('DashBoard.actual'), t('DashBoard.target')] }
                                datasetLabel = {t('DashBoard.monthlyTarget')}
                                labelMetrics = {SETTING_TARGET_MONTHLY}
                                datasetData={targetMonthyPercent}
                                datasetDataLabel={targetMonthyLabel}
                                loading={targetMonthyLoading}
                                />
                            <DaugeChart 
                                labels={ [t('DashBoard.actual'), t('DashBoard.target')] }
                                datasetLabel = {t('DashBoard.yearlyTarget')}
                                labelMetrics = {SETTING_TARGET_YEARLY}
                                datasetData={targetYearlyPercent}
                                datasetDataLabel={targetYearlyLabel}
                                loading={targetYearlyLoading}
                                />
                            <DaugeChart 
                                labels={ [t('DashBoard.paid'), t('DashBoard.unpaid')] }
                                datasetLabel = {t('DashBoard.sales')}
                                labelMetrics = {salesCurrentYear}
                                datasetData={paidInvoicePercent}
                                datasetDataLabel={paidInvoiceLabel}
                                loading={paidInvoiceLoading}
                                />
                            
                        </div>
                    </div>
                    <div className='flex flex-col gap-2 pt-3 md:flex-row '>
                        <div className='flex-1'><InvListWidget/></div>
                        <div className='flex-1'><QuoteListDashboard/></div>
                    </div>
                    <div className=' flex flex-col md:flex-row justify-between items-start gap-1 '>
                        <PieChart
                                label ={t('DashBoard.topProducts')}
                                labels = {productLabels}
                                datasetsLabel = 'Products'
                                datasetsData = {productTotals}
                                borderColor = {['indigo','gray','black','purple','orange']}
                                backgroundColor = {['indigo','gray','black','purple','orange']}
                                loading = {productLoading}
                            />
                        
                        <PieChart
                                label={t('DashBoard.topSalesmen')}
                                labels = {salesmanLabels}
                                datasetsLabel = 'Salesman'
                                datasetsData = {salesmanTotals}
                                borderColor = {['indigo','gray','black','purple','orange']}
                                backgroundColor = {['indigo','gray','black','purple','orange']}
                                loading = {salesmanLoading}
                            /> 
                        <TimeLine/>
                    </div>
                    <div className='h-5'></div>
                </div>
            </div>
        </>
    )
}

export default DashBoard