import useTenant from "./useTenant";
import useLang from "./useLang";
import useUser from "./useUser";
import useInvoice from "./useInvoice";
import useFetch from "./useFetch";
import usePost from "./usePost";
import useDelete from "./useDelete";
import usePut from "./usePut";
import useGenInvoiceNo from "./useGenInvoiceNo";
import useGenQuoteNo from "./useGenQuoteNo";
import useStartup from "./useStartup";
import useCurrent from "./useCurrent";
import useQrInvoice from "./useQrInvoice";
import useLogs from "./useLogs";
import useUploadFile from "./useUploadFile";
import useZatca from "./useZatca";

export {
        useTenant,
        useLang,
        useUser,
        useInvoice,
        useFetch,
        usePost,
        useDelete,
        usePut,
        useGenInvoiceNo,
        useGenQuoteNo,
        useStartup,
        useCurrent,
        useQrInvoice,
        useLogs,
        useUploadFile,
        useZatca

    };