import axios from 'axios';
import { useState} from 'react'

const useDeleteWithBody = () => {
    const [loading,setLoading] = useState(false);
    const [error,setError]  = useState(null);
    const [data,setData] = useState();
    const token = localStorage.getItem('token');

    const deleteWithBody = async (url,body) => {
        setLoading(true);
        setError(null);
        console.log('list delete body' , body)
        try {
            const res = await axios.delete(url,body,{
              headers:{
                  'Authorization': `Bearer ${token}` 
              }
          });
            setData(res.data)
          // Handle successful deletion
        } catch (error) {
            setError(error);
          // Handle error
        } finally {
            setLoading(false);
        }
        };
        console.log('deleteItem' ,data)

    return {deleteWithBody,data,loading,error}
}

export default useDeleteWithBody