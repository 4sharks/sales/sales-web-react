import React from 'react'
import { useRouteError } from "react-router-dom";
import { useNavigate } from 'react-router-dom';

const ErrorPage = () => {
    const error = useRouteError();
    const navigate = useNavigate()
    console.error(error);
    return (
        <div id="error-page">
        <h1>Oops!</h1>
        <p>Sorry, an unexpected error has occurred.</p>
        <p>
            <i>{error.statusText || error.message}</i>
        </p>
        <a href="#" onClick={ ()=> navigate("/",{ replace:true }) } > Home</a>
        </div>
    )
}

export default ErrorPage;