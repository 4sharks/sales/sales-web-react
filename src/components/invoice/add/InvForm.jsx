import React, { useState,useEffect, useRef } from 'react'
import { useNavigate } from 'react-router-dom';
import { useSelector,useDispatch } from 'react-redux';
import {  useTranslation } from 'react-i18next';
import {AiOutlineSave} from 'react-icons/ai'
import {MdOutlineAddBox} from 'react-icons/md'
import toast, { Toaster } from 'react-hot-toast';

 import {updateSettings} from '../../../store/settingSlice'
import {useUser,usePost,useInvoice,useGenInvoiceNo,useCurrent,useLogs} from '../../../hooks';
import {MwToast,MwSpinner, MwButton, CardAmount, MwInputText} from '../../ui';
import CreateProduct from '../../Products/CreateProduct';
import CreateCustomer from '../../Customers/CreateCustomer';
import CreateSalesman from '../../Salesmans/CreateSalesman';
import {getAllSalesman} from '../../../services/salesmanService';
import {getAllCustomers} from '../../../services/customerService';
import MwSpinnerButton from '../../ui/MwSpinnerButton';
import SideModal from '../../ui/SideModal';
import InvFormFooter from './InvFormFooter';
import InvFormHeader from './InvFormHeader';
import InvFormBody from './InvFormBody';
import { amountFormat } from '../../../utils/invoceHelper';
import { getDateNow } from '../../../utils/global';


const InvForm = ({tenantId,tenantUsername}) => {
    const [t] = useTranslation('global')
    const dispatch = useDispatch()
    const [disabledDiscount,setDisabledDiscount] = useState(false);
    const {currentLangId,currentCompanyId,currentBranchId} = useCurrent();
    const companySelected  = useSelector((state)=>state.currentCompany)
    const branchSelected = useSelector((state)=>state.currentBranch)
    const {
            SETTING_INVOICE,
            initalProductItem,
            calcSumPrice,
            calcSumNetTotal,
            toFloat,
            calcVat,
            formatter
            } = useInvoice();

    const INV_CURRENCY = SETTING_INVOICE?.INV_CURRENCY
    const SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION = SETTING_INVOICE?.SHOW_INVOICE_PRODUCT_DESCRIPTION || 'false';
    const SETTING_INV_WITH_HEADER_FOOTER = SETTING_INVOICE?.INV_WITH_HEADER_FOOTER || 'false';
    const SETTING_INV_UOM_SHOW = SETTING_INVOICE?.INV_UOM_SHOW || 'false';
    const SETTING_INV_QTY_DAYS = SETTING_INVOICE?.INV_QTY_DAYS || 'false';
    const SETTING_INV_CURRENCY = INV_CURRENCY || 'SAR'
    const {resultLogsPost,loadingLogsPost,errorLogsPost,postDataHandler} = useLogs()
    
    const {newInvNo,loading:loadingGenInvoiceNo} = useGenInvoiceNo();
    const navigate = useNavigate();
    const {getUser} = useUser();
    const [submitBtnDisabled,setSubmitBtnDisabled] = useState(false);
    const { data:invDataPost, loading:invLoadingPost, error:invErrorPost,postData:invPost} = usePost(); 
    const  [formErrors,setFormErrors] = useState({});
    // products
    const [reloadProductList,setReloadProductList] = useState(false)
    const [showProductModal,setShowProductModal] = useState(false);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [priceListItems, setPriceListItems] = useState([]);
        
    
    // Customer
    const [customers,setCustomers] = useState([]); 
    const [showCreateCustomerModal,setShowCreateCustomerModal] = useState(false);
    const [isAddedCustomer,setIsAddedcustomer] = useState(false); 
    // Salesman
    const [salesmans,setSalesmans] = useState([]); 
    const [showCreateSalesmanModal,setShowCreateSalesmanModal] = useState(false);
    const [isAddedSalesman,setIsAddedSalesman] = useState(false); 
    // Toast
    const [showToast,setShowToast] = useState(false);
    const [msgToast,setMsgToast] = useState('');
    // Invoice input fields
    const [invNo,setInvNo] = useState('');
    const invRef = useRef(null);
    
    const [invDate, setInvDate] = useState(getDateNow().toLocaleDateString('en-CA') + ' ' + new Date().toLocaleTimeString('it-IT'));
    const calculateDueDate = (days) => {
        const date = new Date();
        date.setDate(date.getDate() + parseInt(days || 0));
        return date.toLocaleDateString('en-CA');
    };
    const [invDateDue, setInvDateDue] = useState(calculateDueDate(90));
    const invCurrency = useRef(null);
    const [customerSelected,setCustomerSelected] = useState();
    const [salesmanSelected, setSalesmanSelected] = useState([]);
    const [invNotes,setInvNotes] = useState('');
    const [invTerms,setInvTerms] = useState('');
    const [havePromoCode,setHavePromoCode] = useState(false);
    const [promoCode,setPromoCode] = useState('');
    const [invTotalAmount,setInvTotalAmount] = useState('');
    const [totalAmountWithDiscount,setTotalAmountWithDiscount] = useState('');
    const [discount,setDiscount] = useState('')
    const [vat,setVat] = useState('')
    const [netTotal,setNetTotal] = useState('')
    // const [productList,setProductList] = useState([initalProductItem]); 
    const [productList,setProductList] = useState([]); 
    const [tagList,setTagList] = useState([]);


            

    // Promo Code
    const promoCodeHandler =() => {

    }
    // Product Functions
    const showModalProductHandle = () => {
        setShowProductModal(true);
    }
    const onProductCreated = () => {
        setReloadProductList(true);
    }

    // Customer Functions
    const onCustomerCreated = () => {
        setIsAddedcustomer(true);
        
        // setMsgToast('Customer Has been added...')
        // setShowToast(true);
    }
    const onClickNewCustomer = () => {
        setShowCreateCustomerModal(true);
        setIsAddedcustomer(false);
    }
    


    // Salesman Functions
    const onSalesmanCreated = () => {
        setIsAddedSalesman(true);
        // setMsgToast('Salesman Has been added...')
        // setShowToast(true);
    }
    const onClickNewSalesman = () => {
        setShowCreateSalesmanModal(true);
        setIsAddedSalesman(false);
    }
    const calcSum = () => {
        return calcSumPrice(productList)
        
    }
    const invSumHandler = () => {
        if(productList){
            console.log('invSumHandler')
            const sum = calcSum();
            const netTotal = calcSumNetTotal(productList);
            const _discountItems = sum - netTotal;
            
            _discountItems > 0 ? setDisabledDiscount(true) : setDisabledDiscount(false)
            if(disabledDiscount && _discountItems >= 0){
                setDiscount(amountFormat(_discountItems) ) 
            }
            const _totalAmountWithDiscount = sum - discount;
            setInvTotalAmount(sum);
            setTotalAmountWithDiscount(_totalAmountWithDiscount);
            vatHandler(_totalAmountWithDiscount);
        }
    }

    const vatHandler = (total=totalAmountWithDiscount) => {
        const calcvat = calcVat(total)
        setVat(calcvat)
        // if(SETTING_INVOICE.PRICE_INCLUDE_VAT === 'true'){
        //     setNetTotal(toFloat(total))
        // }else{
            const nt = parseFloat(calcvat) + parseFloat(total);
            setNetTotal(nt)
        //}
    }

    const discountHandler = () => {
        
        invSumHandler();
    }

    const netTotalHandler = () => {
        const invTotal  = totalAmountWithDiscount   ;
        const invDiscount = toFloat(discount,false) ;  
        const invVat = toFloat(vat,false) ;  
        const net = toFloat(invTotal - invDiscount + invVat);
        setNetTotal(net);
    }

    const discountOnchange = (value) => {
        
        const sum = calcSum();
     
        let discountValue;
        if(value?.includes('%')){
            invSumHandler();
            discountValue =  (toFloat( toFloat(sum,false) * toFloat(value.replace('%', ''),false) /100 ));
            console.log(discountValue);
        }else{
            discountValue = value
        }
        console.log('netTotal',netTotal);
        setDiscount(discountValue  );
    }
    const validateForm = () => {
        setFormErrors({})
        let errors = {};
        console.log("errors", errors)
    
        let hasErrors = false
        if (!invDate) {
            errors.invDate = 'Date is required';
            hasErrors = true
        }
        if (!invDateDue) {
            errors.invDateDue = 'Date Due is required';
            hasErrors = true
        }
        if (!customerSelected || !customerSelected.value) {
            console.log("No customer selected - showing toast");
            errors.customer = 'Customer is required';
            hasErrors = true;
            
                 toast.error('يجب اختيار العميل');
             
            setFormErrors({ ...errors });
            return false; // Return early to prevent other toasts
        }
        if (!productList || productList.length === 0) {
            toast.error('يجب إضافة منتج واحد على الأقل');
            hasErrors = true;
        
            setFormErrors({ ...errors });
            return false; // Return early to prevent other toasts
        }
         else if (!productList[0]?.productId) {
         toast.error('يجب اختيار منتج')
            hasErrors = true;
            return false;
        }
        if (!netTotal) {
            toast.error('يجب إدخال القيمة الإجمالية');
            hasErrors = true;
            return false;
        }
        if (Object.keys(errors).length > 0) {
            setFormErrors({ ...errors });
            return false;
        }
        return true;
    }
    const onsubmitHandler = async(e) => {
        e.preventDefault()
         
        if(e.key === 'Enter') return ;
        // validate form 
        if(!validateForm())return;
        setSubmitBtnDisabled(true); 
        setIsSubmitting(true);
        try{
        let productListHandler = [];
        productList.map((product)=>{
            if (product && product.productName !=='Deleted' ){
                productListHandler.push(  {
                    product_id : product.productId,
                    product_name : product.productName,
                    product_desc : product.productDesc || '',
                    unit_id : 1,
                    qty : product.qty ,
                    qty_days: product.qtyDays || '1',
                    uom: product.uom || '',
                    uom_template_id: product.uomTemplateId || '',
                    // price: (parseFloat(product.price)/parseInt(product.qty)),
                    price: product.priceUnit,
                    total_price : product.price,
                    product_discount : product.productDiscount || 0,
                    product_net_total : product.productNetTotal,
                    product_vat : product.productVat,
                    product_net_total_with_vat: product.productNetTotalWithVat
                });
            }
        });
        const _data = {
            inv_no: invNo,
            inv_ref:invNo || '',
            inv_date:invDate,
            inv_date_due:invDateDue,
            customer_id:customerSelected?.value,
            customer_name:customerSelected?.label,
            // salesman_id:salesmanSelected.map(s => s.value).join(','),
            // salesman_name:salesmanSelected.map(s => s.label).join(','),
            salesman_id:salesmanSelected.map(s => s.value),
            salesman_name:salesmanSelected.map(s => ({
                en: s.fullname.find(f => f.lang === "en")?.text || "",
                ar: s.fullname.find(f => f.lang === "ar")?.text || ""
            })),
            total_amount: invTotalAmount,
            discount_amount: discount,
            // promo_code_id:
            // promo_code:
            vat_amount: vat,
            net_amount: netTotal,
            notes: JSON.stringify(invNotes),
            terms: JSON.stringify(invTerms),
            is_paid: false,
            type_id:1,
            status_id:1,
            tenant_id: tenantId,
            company_id: companySelected.value,
            branch_id: branchSelected.value,
            created_by: getUser?._id || 0,
            inv_tags:JSON.stringify(tagList),
            inv_details:productListHandler
        }
        console.log("_data", _data);
        await invPost(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/invoices`
        ,_data)

        } catch (error) {
              toast.error('حدث خطأ أثناء حفظ الفاتورة')
            setIsSubmitting(false);
        }
    };

    const getcustomerHandler = async () => {
        const res = await getAllCustomers(tenantId,companySelected.value,branchSelected.value)
        console.log('getAllCustomers',res)
        setCustomers(res);
    }

    const getSalesmanHandler = async () => {
        const res = await getAllSalesman(tenantId,companySelected.value,branchSelected.value);
        setSalesmans(res);
    }

    const setLogsHandler =  () => {
        const _data = {
            moduleName: "SALES",
            resourceName: "INVOICES",
            eventName:"ADD",
            logContentEn: `Add new invoice No: ${invDataPost.inv_no}`,
            logContentAr: `انشاء فاتورة جديدة برقم : ${invDataPost.inv_no}`,
        }
        
        postDataHandler(_data);
        
    }
    useEffect(()=>{
        if(resultLogsPost){
            if(SETTING_INVOICE?.INV_PAYMENT_AFTER_CREATE === 'true'){
                //setShowPaymentModal(true);
                setTimeout(() => {
                navigate(`/${tenantUsername}/invoices/show/${invDataPost?.id}/${currentCompanyId}/${currentBranchId}`);
            }, 2000);

            }else{
                setTimeout(() => {
                navigate(`/${tenantUsername}/invoices`);
            }, 2000);
            }
        }
    },[resultLogsPost]);

    useEffect(()=>{
        if(invDataPost){
            setLogsHandler();
        }
    },[invDataPost]);

     useEffect(() => {
             if (isSubmitting) {
                setIsSubmitting(false);
            }
        }, [
            productList, 
            customerSelected, 
            salesmanSelected, 
            invNotes, 
            invTerms,
            discount,
            netTotal,
            invDate,
            invDateDue,
            invRef,
          ]);
    

    useEffect(() => {   
        getcustomerHandler();    
        getSalesmanHandler();
    },[]);

    useEffect(() => {
        if(isAddedCustomer){
            getcustomerHandler();        
        }
    },[isAddedCustomer]);

    useEffect(() => {
        if(isAddedSalesman){
            getSalesmanHandler();       
        }
    },[isAddedSalesman]);

    useEffect(()=>{
        if(productList ){ 
            invSumHandler();

            dispatch(updateSettings({
                typeSetting : 'invoice', value :{
                    INV_ITEMS_ROW_NUMBER : productList.length 
                }
            }))
        }
    },[productList]);

    useEffect(()=>{
        netTotalHandler();
        vatHandler();
    },[invTotalAmount])

    useEffect(() => {
        discountHandler();
    },[discount])

    useEffect(() => {
        if(!loadingGenInvoiceNo){
            setInvNo(newInvNo)
        }
    },[loadingGenInvoiceNo]);
    console.log('invDataPost',invDataPost)

     useEffect(()=>{
            if(invDataPost?.branch_id){
                console.log('invDataPost',invDataPost)
            toast.success('تم حفظ الفاتورة بنجاح') 
            return;
            } if(invDataPost?.status===0){
                toast.error('حدث خطأ أثناء حفظ الفاتورة') 
                return
               }
        },[ invDataPost]);
        useEffect(()=>{
            if(invErrorPost){
             toast.error('حدث خطأ أثناء حفظ الفاتورة') 
            }
        },[ invErrorPost]);

     

    const onSubmitPay = () =>{
        //console.log('onSubmitPay');
        navigate(`/${tenantUsername}/invoices/show/${invDataPost?.id}/${currentCompanyId}/${currentBranchId}`);
    }
     return (
        <>

<Toaster
  position="top-center"
  containerStyle={{
    top: 60, 
    zIndex: 9999
  }}
  toastOptions={{
    duration: 1300,
    hideProgressBar: false,
    closeOnClick: true,
    draggable: true,
    pauseOnHover: true,
    style: {
      direction: "rtl",
      zIndex: 9999
    }
  }}
/>
            { showToast && <MwToast 
                msg={msgToast} 
                isShow={showToast}
                setIsShow = {setShowToast}
                /> 
            }


            <SideModal title="Create New Product" onShow={showProductModal} setOnShow={setShowProductModal} >
                <CreateProduct onProductCreated={onProductCreated} setReloadProductList={setReloadProductList}  />
            </SideModal>

            {/* <MwModal title="Create New Product" onShow={showProductModal} setOnShow={setShowProductModal} >
                <CreateProduct onProductCreated={onProductCreated} setReloadProductList={setReloadProductList}  />
            </MwModal> */}

            <SideModal title="Create New Customer" onShow={showCreateCustomerModal} setOnShow={setShowCreateCustomerModal} >
                <CreateCustomer onCustomerCreated={onCustomerCreated}  />
            </SideModal>

            <SideModal title="Create New Salesman" onShow={showCreateSalesmanModal} setOnShow={setShowCreateSalesmanModal} >
                <CreateSalesman onSalesmanCreated={onSalesmanCreated}  />
            </SideModal>
            
            {loadingGenInvoiceNo &&  <MwSpinner/> }
            {!loadingGenInvoiceNo && 
            <form  onSubmit={onsubmitHandler} >
                <div className='flex items-center justify-between mb-3 mt-1' >
                    <div className='pb-2 text-gray-400 text-sm flex items-center gap-1 '>
                        <MdOutlineAddBox size={18}/>
                        {t('pagesTitles.createInvoice')}
                    </div>
                    { invLoadingPost ? <MwSpinnerButton/> : 
                        <MwButton
                        actionType='submit'
                        // disabled={submitBtnDisabled}
                        disabled={isSubmitting}
                        inGroup = {true}
                        type = 'saveBtn'
                            > <AiOutlineSave size={16}/> {t('invoice.saveInvoice')} </MwButton> 
                    }
                </div>
                <div  className='flex flex-col h-full justify-between  bg-slate-50  rounded-lg'>
                    <InvFormHeader
                            invNo={invNo}
                            setInvNo={setInvNo}
                            invRef={invRef}
                            invDate={invDate}
                            setInvDate={setInvDate}
                            invDateDue={invDateDue}
                            setInvDateDue={setInvDateDue}
                             invCurrency = {invCurrency}
                            customers = {customers}
                            onClickNewCustomer = {onClickNewCustomer}
                            salesmans = {salesmans}
                            onClickNewSalesman = {onClickNewSalesman}
                            customerSelected = {customerSelected} 
                            setCustomerSelected = {setCustomerSelected}
                            salesmanSelected = {salesmanSelected}
                            setSalesmanSelected = {setSalesmanSelected}
                            formErrors = {formErrors}
                            SETTING_INVOICE = {SETTING_INVOICE}
                            priceListItems = {priceListItems}
                            setPriceListItems = {setPriceListItems}
                            productList={productList}
                            setProductList={setProductList}
                            SETTING_PRICE_INCLUDE_VAT={SETTING_INVOICE.PRICE_INCLUDE_VAT}
                             

                    />
                
                    <InvFormFooter
                         invNo={invNo}
                        setInvNo={setInvNo}
                        invNotes = {invNotes}
                        setInvNotes = {setInvNotes}
                        invTotalAmount = {invTotalAmount} 
                        totalAmountWithDiscount = {totalAmountWithDiscount}
                        invCurrency = {invCurrency}
                        promoCode = {promoCode}
                        setPromoCode = {setPromoCode}
                        discount = {discount}
                        vat = {vat}
                        netTotal = {netTotal}
                        havePromoCode = {havePromoCode}
                        setHavePromoCode = {setHavePromoCode}
                        promoCodeHandler = {promoCodeHandler}
                        discountOnchange = {discountOnchange}
                        SETTING_PRICE_INCLUDE_VAT = {SETTING_INVOICE.PRICE_INCLUDE_VAT}
                        SETTING_INV_CURRENCY = {SETTING_INV_CURRENCY}
                        tagList = {tagList} 
                        setTagList = {setTagList}
                        disabledDiscount ={disabledDiscount}
                        formatter ={formatter}
                    />


                    <InvFormBody 
                        SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION = {SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION}
                        SETTING_INV_QTY_DAYS = {SETTING_INV_QTY_DAYS}
                        SETTING_INV_UOM_SHOW = {SETTING_INV_UOM_SHOW}
                        initalProductItem = {initalProductItem}
                        reloadProductList = {reloadProductList}
                        showModalProductHandle = {showModalProductHandle}
                        productList = {productList}
                        setProductList ={setProductList}
                        formErrors = {formErrors}
                        SETTING_PRICE_INCLUDE_VAT = {SETTING_INVOICE.PRICE_INCLUDE_VAT}
                        setInvNotes = {setInvNotes}
                        setInvTerms = {setInvTerms}
                        priceListItems = {priceListItems}
                        /> 
                </div>

                 

<div className='flex md:flex-row flex-col gap-2  items-top p-3 justify-between text-sm'>
    <div className="flex-1  flex items-top justify-between">
        <CardAmount
            label= {t("invoices.totalBeforeDiscount")}
            amountValue={formatter.format(amountFormat(invTotalAmount || 0))}
            //footerValue={formatter.format(SETTING_INV_CURRENCY)}
            />
        <div id='discount-card' className=' flex-1 text-center '>
            <div className=' text-slate-400 text-xs'>  {t("invoices.discountOnTotalInvoice")}</div>
            <MwInputText
                value={discount}
                // disabled={disabledDiscount}
                disabled={true}
                // placeholder={t('invoice.discount')}
                classNameInput = 'font-bold py-5'
                size='sm'
                onChange={(e)=>discountOnchange(e.target.value)}
            />
            {/* <div className='text-slate-400 text-xs'>{ discount && SETTING_INV_CURRENCY.toUpperCase() }</div> */}
        </div>
        <CardAmount
            label= {t("invoices.totalAfterDiscount")}
            amountValue={ formatter.format(amountFormat(totalAmountWithDiscount || 0)) }
            // footerValue={SETTING_INV_CURRENCY.toUpperCase()}
            />
    </div>
    <div className="flex-1 flex items-center justify-between">
        <CardAmount
            label={t('invoice.vatAmount')}
            amountValue={formatter.format(amountFormat(vat || 0))}
            // footerValue={SETTING_INV_CURRENCY.toUpperCase()}
            />
        <CardAmount
            label={t('invoice.netTotal')}
            amountValue={formatter.format(amountFormat(netTotal || 0))}
            // footerValue={SETTING_INV_CURRENCY.toUpperCase()}
            />                        
    </div>
</div>
            </form> }
        </>
    )
}

export default InvForm