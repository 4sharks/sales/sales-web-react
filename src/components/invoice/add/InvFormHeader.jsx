import React, { useEffect } from 'react'
import {  useTranslation } from 'react-i18next';
import {getDateNow} from '../../../utils/global';

import {MwInputText,MwSelector, MwSelectorMulti} from '../../ui';
import { useFetch } from '../../../hooks';

const InvFormHeader = ({
    invNo,
    setInvNo,
    invRef,
    invDate,
    setInvDate,
    invDateDue,
    setInvDateDue,
    invCurrency,
    customers,
    onClickNewCustomer,
    salesmans,
    onClickNewSalesman,
    customerSelected,
    setCustomerSelected,
    salesmanSelected,
    setSalesmanSelected,
    formErrors,
    SETTING_INVOICE,
    priceListItems,
    setPriceListItems ,
    productList,
    setProductList,
    SETTING_PRICE_INCLUDE_VAT

    }) => {
        const [t] = useTranslation('global')
        const [paymentDays, setPaymentDays] = React.useState('');
        const {
            data: fetchedData,
            loading,
            error,
            refreshHandler
          } = useFetch( 
            customerSelected && customerSelected.listPriceId
              ? `${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/inventory/list-price-items/${customerSelected.listPriceId}`
              : null,
            false // تم تعطيل autoLoad لأننا سنستخدم refreshHandler عند التغيير
          );

        console.log("customerSelected", customerSelected)
       


        const date = new Date();
        const today = getDateNow().toLocaleDateString('en-CA'); // Get current date in the format 'YYYY-MM-DD'
        const [dueDate, setDueDate] = React.useState(today);
        const todayAndTime = getDateNow().toLocaleDateString('en-CA')+' '+date.toLocaleTimeString('it-IT'); // Get current date in the format 'YYYY-MM-DD'
        
        console.log('SETTING_INVOICE',SETTING_INVOICE.INV_SALESMAN_DEFAULT)
 
        const calculateDueDate = (days) => {
            const invoiceDate = new Date(invDate);
            invoiceDate.setDate(invoiceDate.getDate() + parseInt(days || 0));
            return invoiceDate.toLocaleDateString('en-CA');
        };
        
        const calculateDays = (dueDate) => {
            const invoiceDate = new Date(invDate);
            invoiceDate.setHours(0, 0, 0, 0);
            
            const due = new Date(dueDate);
            due.setHours(0, 0, 0, 0);
            
            const diffTime = due - invoiceDate;
            return Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        };

        useEffect(() => {
            // تعيين تاريخ استحقاق افتراضي (90 يوم بعد اليوم)
            const defaultDays = 90;
            setPaymentDays(defaultDays.toString());
            const defaultDueDate = calculateDueDate(defaultDays);
            setDueDate(defaultDueDate);
            
            // تحديث قيمة المرجع
            if (invDateDue.current) {
                invDateDue.current.value = defaultDueDate;
            }
        }, []); 

        console.log("customerSelected", customerSelected);
        console.log("salesmans", salesmans);

           useEffect(() => {
                   if (customerSelected && customerSelected.salesmanId) {
                       const matchingSalesman = salesmans.find(salesman => salesman._id === customerSelected.salesmanId);
                       if (matchingSalesman) {
                           setSalesmanSelected([{
                               label: matchingSalesman?.fullname && matchingSalesman.fullname[1] ? matchingSalesman.fullname[1].text : '',
                               value: matchingSalesman._id,
                               ...matchingSalesman
                           }]);
                       }
                   } else {
                       setSalesmanSelected([]);
                   }
               }, [customerSelected, salesmans, setSalesmanSelected]);
       
               useEffect(() => {
                 if (customerSelected && customerSelected.listPriceId) {
                   console.log("defaultcustomerSelected",customerSelected);
                   refreshHandler();
                 } else {
                    const defaultPriceList = productList.map(product => {
                     console.log("defaultPriceList out");
                      if (product.productId === 'section' || !product.productId || product.productId === '0') {
                        
                       return product;
                     }
               
                      const defaultPrice = SETTING_PRICE_INCLUDE_VAT === 'true' 
                       ? (parseFloat(product.originalPriceUnit) || 0)
                       : parseFloat(product.originalPriceUnit) || 0;
               
                     return {
                       ...product,
                       priceUnit: defaultPrice,
                       price: product.qty * defaultPrice * (product.qtyDays || 1),
                       productNetTotal: product.qty * defaultPrice * (product.qtyDays || 1),
                       productVat: (product.qty * defaultPrice * (product.qtyDays || 1)) * (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100,
                       productNetTotalWithVat: (product.qty * defaultPrice * (product.qtyDays || 1)) * (1 + (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100)
                     };
                   });
               
                   setPriceListItems([]);
                   setProductList(defaultPriceList);
                 }
               }, [customerSelected]);
                 
                 useEffect(() => {
                   if (fetchedData && fetchedData.data) {
                     setPriceListItems(fetchedData.data);
                     
                      if (productList && productList.length > 0 && customerSelected && customerSelected.listPriceId) {
                       const updatedProductList = [...productList];
                       
                       updatedProductList.forEach((product, idx) => {
                          if (product.productId === 'section' || !product.productId || product.productId === '0') {
                           return;
                         }
                         
                          const priceListItem = fetchedData.data.find(
                           item => item.product_id.trim() === product.productId
                         );
                         
                         if (priceListItem) {
                            const newPrice = SETTING_PRICE_INCLUDE_VAT === 'true' 
                             ? (parseFloat(priceListItem.product_sale_price) ) 
                             : parseFloat(priceListItem.product_sale_price);
                           
                            product.priceUnit = newPrice;
                           
                            product.price = product.qty * newPrice * (product.qtyDays || 1);
                           
                            if (product.productDiscount && product.productDiscount.includes('%')) {
                             product.productNetTotal = parseFloat(product.price) - (parseFloat(product.price) * parseFloat(product.productDiscount.replace('%', '')) / 100);
                           } else {
                             product.productNetTotal = product.price - (product.productDiscount || 0);
                           }
                           
                            const vatPercent = SETTING_INVOICE?.INV_VAT_PERCENT || 15;
                           product.productVat = (product.productNetTotal || 0) * vatPercent / 100;
                           product.productNetTotalWithVat = product.productVat + (product.productNetTotal || 0);
                         }
                       });
                       
                        setProductList([...updatedProductList]);
                     }
                   }
                 }, [fetchedData, customerSelected]);
               
        
    return (
            <div className="flex flex-col md:flex-row justify-between  items-center ">
                <div className="flex w-full md:w-fit " >

                <div className="flex-1 md:w-36">
                    <MwInputText 
                    label={t('invoice.invoiceNo')}
                    id='inv_no' 
                    value={invNo} 
                    disabled={true} 
                    onChange={(e)=>setInvNo(e.target.value)} />
                    </div>
            
                    <div className=" md:w-20">
                        <MwInputText 
                            label={t('invoice.invoiceRef')}
                            inputRef={invRef}
                            id='inv_ref' 
                            defaultValue=''
                            />
                    </div>
                   
                </div>
                    <div className="flex w-full md:w-fit " >
                    <div className="  md:w-36 ">
                    <MwInputText 
        label={t('invoice.invoiceDate')}
        inputType='datetime-local'
        id='inv_date'
        value={invDate}
        onChange={(e) => setInvDate(e.target.value)}
        invalid={!!formErrors?.invDate}
        disabled={true}
    />
                    </div>
                    <div className="  md:w-20">
                    <MwInputText 
    label={t('invoice.paymentWithin') || "السداد خلال"}
    id='payment_days'
    inputType='number'
    placeholder="عدد الأيام"
    value={paymentDays}
    onChange={(e) => {
        const days = e.target.value;
        setPaymentDays(days);
        
        const invoiceDate = new Date(invDate);
        const newDueDate = new Date(invoiceDate);
        newDueDate.setDate(invoiceDate.getDate() + parseInt(days || 0));
        const formattedDueDate = newDueDate.toLocaleDateString('en-CA');
        
        setDueDate(formattedDueDate);
        setInvDateDue(formattedDueDate);
    }}
/>
                    </div>
                    
                    <div className="  md:w-28">
                    <MwInputText 
    label={t('invoice.invoiceDateDue')}
    inputType='date'
    id='inv_date_due'
    value={dueDate}
    invalid={!!formErrors?.invDateDue}
    onChange={(e) => {
        const selectedDate = e.target.value;
        
        if (selectedDate) {
             setDueDate(selectedDate);
            setInvDateDue(selectedDate);
            
            const invoiceDate = new Date(invDate);
            invoiceDate.setHours(0, 0, 0, 0);
            
            const due = new Date(selectedDate);
            due.setHours(0, 0, 0, 0);
            
            const diffTime = due - invoiceDate;
            const days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            
            setPaymentDays(days.toString());
        }
    }}
/>

                    </div>
                
                </div>
                <div className="relative flex-1  flex w-full ">
                    <div className="flex-1 flex  flex-col ">
                        <MwSelector 
                            label={t('invoice.selectCustomer')}
                            initalValue={`${t('invoice.selectCustomer')}...`}
                            _data={customers} 
                            dataType='customer' 
                            onClickNew = {onClickNewCustomer}
                            selectedItem = {customerSelected}
                            setSelectedItem = {setCustomerSelected}
                            selectFirstValue = {SETTING_INVOICE.INV_CUSTOMER_DEFAULT}
                            invalid={!!formErrors?.customer}
                            ithSearch={false} 
                            
                            />
                    </div>
                    <div className="flex-1 flex flex-col ">
                    <MwSelectorMulti
                initalValue={`${t('invoice.selectSalesman')}...`}
                label={t('invoice.selectSalesman')}
                _data={salesmans} 
                dataType='salesman' 
                onClickNew={onClickNewSalesman}
                selectedItem={salesmanSelected}
                setSelectedItem={setSalesmanSelected}
                className="flex-1"
                displayProperty="name" 
                
            />
                    </div>
                </div>
            </div>
        
    )
}

export default InvFormHeader