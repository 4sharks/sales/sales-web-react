import React from 'react'
import { useTranslation } from 'react-i18next'
import parse from 'html-react-parser';
import Blocks from 'editorjs-blocks-react-renderer';

const InvoiceFooter = ({
    totalAmount,
    vatAmount,
    discountAmount,
    netAmount,
    invTerms,
    SETTINGTerms,
    SETTING_PRICE_INCLUDE_VAT,
    SETTING_INV_VAT_PERCENT,
    SETTING_SHOW_TERMS_ON_PRINT,
    SETTING_INV_PRINT_TERMS_SEPRATE_PAGE,
    isDelivery,
    currentLang,
    SETTING_INV_CURRENCY,
    formatter,
    invNotes
}) => {
    const [t] = useTranslation('global')
    
     const safeParseEditorData = (jsonString) => {
        try {
            let cleanString = jsonString;
            if (typeof cleanString === 'string') {
                 cleanString = cleanString
                    .replace(/\\+([\\\/"])/g, '$1')  
                    .replace(/\\([^\\\/"])/g, '$1')   
                    .replace(/\\\\/g, '\\')           
                    .replace(/\\"/g, '"')            
                    .replace(/\\n/g, ' ')         
                    .replace(/\/\//g, '')             
                    .replace(/^[\\\/\s]+/, '')        
                    .replace(/[\\\/\s]+$/, '')        
                    .replace(/\s+/g, ' ');            
 
                 if (cleanString.startsWith('"') && cleanString.endsWith('"')) {
                    cleanString = cleanString.slice(1, -1);
                }
            }

            const parsed = JSON.parse(cleanString);
            
            // Clean the parsed data
            if (parsed && typeof parsed === 'object' && Array.isArray(parsed.blocks)) {
                parsed.blocks = parsed.blocks.map(block => {
                    if (block.type === 'header' && block.data?.text) {
                        block.data.text = block.data.text
                            .replace(/\\+([\\\/"])/g, '$1')
                            .replace(/\\([^\\\/"])/g, '$1')
                            .replace(/\\n/g, ' ')
                            .replace(/\s+/g, ' ')
                            .trim();
                    } else if (block.type === 'list' && Array.isArray(block.data?.items)) {
                        block.data.items = block.data.items.map(item => ({
                            ...item,
                            content: item.content
                                ?.replace(/\\+([\\\/"])/g, '$1')
                                .replace(/\\([^\\\/"])/g, '$1')
                                .replace(/\\n/g, ' ')
                                .replace(/\s+/g, ' ')
                                .trim()
                        }));
                    } else if (block.type === 'paragraph' && block.data?.text) {
                        block.data.text = block.data.text
                            .replace(/\\+([\\\/"])/g, '$1')
                            .replace(/\\([^\\\/"])/g, '$1')
                            .replace(/\\n/g, ' ')
                            .replace(/\s+/g, ' ')
                            .trim();
                    }
                    return block;
                });

                // Filter out empty blocks
                parsed.blocks = parsed.blocks.filter(block => {
                    if (block.type === 'header' || block.type === 'paragraph') {
                        return block.data?.text?.trim().length > 0;
                    } else if (block.type === 'list') {
                        return block.data?.items?.some(item => item.content?.trim().length > 0);
                    }
                    return true;
                });
            }
            
            return parsed;
        } catch (e) {
            console.error('Failed to parse JSON:', e);
            return null;
        }
    };

     const renderEditorContent = (content) => {
        if (!content || typeof content !== 'string') return null;
        
         if (content.includes("{")) {
            const parsedData = safeParseEditorData(content);
            if (parsedData) {
                try {
                     const customRenderer = (data) => {
                        if (!data || !Array.isArray(data.blocks)) return null;

                         const renderedBlocks = [];
                        let i = 0;
                        
                        while (i < data.blocks.length) {
                            const block = data.blocks[i];
                            
                             if (block.data?.text?.toLowerCase()?.includes('special car') ||
                                block.data?.items?.some(item => item.content?.toLowerCase()?.includes('special car'))) {
                                i++;
                                continue;
                            }
                            
                            if (block.type === 'header') {
                                 const nextBlock = data.blocks[i + 1];
                                if (nextBlock && nextBlock.type === 'header' &&
                                    !nextBlock.data?.text?.toLowerCase()?.includes('special car')) {
                                     renderedBlocks.push(
                                        <div key={block.id} className="flex justify-between items-center my-2">
                                            <h1 className="text-[12px] font-bold">{block.data.text}</h1>
                                            <h1 className="text-[12px] font-bold">{nextBlock.data.text}</h1>
                                        </div>
                                    );
                                    i += 2;  
                                } else {
                                     renderedBlocks.push(
                                        <h1 key={block.id} className="text-[13px] font-bold my-2 text-center">{block.data.text}</h1>
                                    );
                                    i++;
                                }
                            } else if (block.type === 'list') {
                                 const nextBlock = data.blocks[i + 1];
                                if (nextBlock && nextBlock.type === 'list' &&
                                    !nextBlock.data?.items?.some(item => item.content?.toLowerCase()?.includes('special car'))) {
                                     const combinedItems = block.data.items.map((item, idx) => {
                                        const englishItem = nextBlock.data.items[idx];
                                        return (
                                            <div key={idx} className="flex justify-between items-start gap-4 mb-1 text-[13px]">
                                                <div className="flex-1 text-right flex items-start gap-2">
                                                    <span className="inline-block w-1.5 h-1.5 rounded-full border border-gray-500 mt-1.5"></span>
                                                    <span>{item.content}</span>
                                                </div>
                                                {englishItem && (
                                                    <div className="flex-1 text-left flex items-start gap-2 justify-end">
                                                        <span>{englishItem.content}</span>
                                                        <span className="inline-block w-1.5 h-1.5 rounded-full border border-gray-500 mt-1.5"></span>
                                                    </div>
                                                )}
                                            </div>
                                        );
                                    });
                                    renderedBlocks.push(
                                        <div key={block.id} className="my-2">
                                            {combinedItems}
                                        </div>
                                    );
                                    i += 2;  
                                } else {
                                     renderedBlocks.push(
                                        <div key={block.id} className="my-2">
                                            {block.data.items.map((item, idx) => (
                                                <div key={idx} className="mb-1 text-[13px] flex items-start gap-2">
                                                    <span className="inline-block w-1.5 h-1.5 rounded-full border border-gray-500 mt-1.5"></span>
                                                    <span>{item.content}</span>
                                                </div>
                                            ))}
                                        </div>
                                    );
                                    i++;
                                }
                            } else if (block.type === 'paragraph') {
                                // Find the next paragraph (if exists)
                                const nextBlock = data.blocks[i + 1];
                                if (nextBlock && nextBlock.type === 'paragraph' &&
                                    !nextBlock.data?.text?.toLowerCase()?.includes('special car')) {
                                    renderedBlocks.push(
                                        <div key={block.id} className="flex justify-between items-start gap-4 mb-1 text-[11px]">
                                            <div className="flex-1 text-right flex items-start gap-2">
                                                <span className="inline-block w-1.5 h-1.5 rounded-full border border-gray-500 mt-1.5"></span>
                                                <span>{block.data.text}</span>
                                            </div>
                                            <div className="flex-1 text-left flex items-start gap-2 justify-end">
                                                <span>{nextBlock.data.text}</span>
                                                <span className="inline-block w-1.5 h-1.5 rounded-full border border-gray-500 mt-1.5"></span>
                                            </div>
                                        </div>
                                    );
                                    i += 2;
                                } else {
                                    renderedBlocks.push(
                                        <div key={block.id} className="mb-1 text-[11px] flex items-start gap-2">
                                            <span className="inline-block w-1.5 h-1.5 rounded-full border border-gray-500 mt-1.5"></span>
                                            <span>{block.data.text}</span>
                                        </div>
                                    );
                                    i++;
                                }
                            } else {
                                i++;
                            }
                        }

                        return <div className="terms-content">{renderedBlocks}</div>;
                    };

                    return customRenderer(parsedData);
                } catch (e) {
                    console.error('Failed to render Blocks:', e);
                    return parse(content.replace(/["']/g, ''));
                }
            }
        }
        return parse(content.replace(/["']/g, ''));
    };

    return (
        <>
        { !isDelivery &&
        <div> 
            <div className=' border rounded text-xs'>
                {
                    discountAmount ? 
                    <div className='flex-col'>
                        <div className='flex justify-between px-3 py-2 border-b'>
                            <div className='flex flex-col'>
                                <span>الإجمالي قبل الخصم </span>
                                <span>TOTAL BEFORE DISCOUNT</span>
                            </div>
                            <div className='flex flex-col justify-center '>{ formatter.format(totalAmount)  } </div>
                        </div>
                        <div className='flex justify-between px-3 py-2 border-b'>
                            <div className='flex flex-col'>
                                <span> الخصم </span>
                                <span>DISCOUNT</span>
                            </div>
                            <div className='flex flex-col justify-center '>{formatter.format(discountAmount)}</div>
                        </div>
                        <div className='flex justify-between px-3 py-2 border-b'>
                            <div className='flex flex-col'>
                                <span>االاجمالي قبل الضريبة</span>
                                <span> TOTAL BEFORE VAT </span>
                            </div>
                            <div className=''>{formatter.format(totalAmount - discountAmount )} </div>
                        </div>
                    </div> : 
                    <div className='flex justify-between px-3 py-2 border-b'>
                        <div className='flex flex-col'>
                            <span>الإجمالي الفرعي </span>
                            <span>SUB TOTAL</span>
                        </div>
                        <div className='flex flex-col justify-center '>{formatter.format(totalAmount)}  </div>
                    </div>
                }
                <div className='flex justify-between px-3 py-2 border-b'>
                    <div className='flex flex-col'>
                            <span> ضريبة القيمة المضافة ({SETTING_INV_VAT_PERCENT}%)</span>
                            <span>VAT ({SETTING_INV_VAT_PERCENT}%)</span>
                        </div>
                    <div className='flex flex-col justify-center '>{formatter.format(vatAmount)} </div>
                </div>
                <div className='flex justify-between px-3 py-2 border-b'>
                    <div className='flex flex-col'>
                            <span>الإجمالي شامل الضريبة </span>
                            <span>TOTAL</span>
                        </div>
                    <div className='font-bold flex flex-col justify-center '>{formatter.format(netAmount)}  </div>
                </div>
            </div>
        </div>}
        
        {invNotes && invNotes.replace(/[\\\/]+/g, '').trim() && 
            <div className='p-2'>
                {renderEditorContent(invNotes.replace(/[\\\/]+/g, '').trim())}
            </div>
        }
        
        <div className=
        {` ${SETTING_INV_PRINT_TERMS_SEPRATE_PAGE === 'true' ? ' page text-xs leading-5 ' : '  text-[11px] leading-3' } `} >
           { SETTING_SHOW_TERMS_ON_PRINT === 'true' && invTerms && 
    (() => {
        const parsedTerms = safeParseEditorData(invTerms);
        const hasContent = parsedTerms && parsedTerms.blocks && 
                           parsedTerms.blocks.length > 0;
        
        return hasContent ? (
            <div className='p-2 invoice-terms'>
                <div className='flex justify-between mb-3'>
                    <h1 className="text-sm font-bold">الشروط والاحكام</h1>
                    <h1 className="text-sm font-bold">CONDITION AND TERMS</h1>
                </div>
                {renderEditorContent(invTerms)}
            </div>
        ) : null;
    })()
}
        </div>
        </>
    )
}

export default InvoiceFooter