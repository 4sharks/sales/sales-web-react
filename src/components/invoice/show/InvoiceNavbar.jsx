import React, { useState } from 'react'
import { AiOutlinePrinter } from 'react-icons/ai';
import { MdOutlinePaid } from 'react-icons/md';
import { AiOutlineEdit } from 'react-icons/ai';
import {  useTranslation } from 'react-i18next';
 
import { BasicSelector, MwButton, MwModal } from '../../ui';
import { useNavigate, useParams } from 'react-router-dom';
import NotHaveInPackage from '../../NotHaveInPackage';
import { FaFilePdf, FaPrint } from 'react-icons/fa';
import handleConvertToPDF from '../../../utils/handleConvertToPDF';
import i18next from 'i18next';
import InvoicePDFDownload from './InvoicePDFDownload';
 

const InvoiceNavbar = ({invoiceData,handlePrint,html2pdf,deleteHandler,isPaid,isReturned,fullReturned,countReturned,
    showpaymentModal,setShowPaymentModal,invId,invoiceTypeSelected,setInvoiceTypeSelected,
    companyName,
    companyAddress,
    commercialRegisteration,
    vatNo,
    companyPhone,
    companyEmail,
    companyWebsite,
    invProducts}) => {
    const [t] = useTranslation('global')
    const isRTL = i18next.dir() === 'rtl';

    const pdfInvoiceData = {
        companyName,
        commercialRegisteration,
        vatNo,
        companyAddress,
        // Add new company contact information
        companyPhone,
        companyEmail,
        companyWebsite,
        inv_date: invoiceData.inv_date,
        inv_no: invoiceData.inv_no,
        inv_date_due: invoiceData.inv_date_due,
        invProducts: invProducts,
        totalAmount: invoiceData.total_amount,
        vatAmount: invoiceData.vat_amount,
        netAmount: invoiceData.net_amount,
        // Add terms and conditions from invoiceData
        termsAndConditions: invoiceData.terms || null
    };
    const navigate = useNavigate();
    const invoiceTypeList = [
        {label: t("invoice.simplified_tax_invoice"), value:"simplified"},
        {label: t("invoice.standard_tax_invoice"), value:"standard"}
    ]
    const params = useParams();
    const [showSendMailModal,setShowSendMailModal] = useState(false);
    const onSelectSendInvoiceToCustomer = (value) => {
       // console.log(value)
        setShowSendMailModal(true);
    }
    const OnChangeInvoiceType = (value) => {
        setInvoiceTypeSelected(value)
    }
    return (
        <>
            <MwModal onShow={showSendMailModal} setOnShow={setShowSendMailModal}  >
                <NotHaveInPackage/>
            </MwModal>
            <div className='w-full flex justify-between gap-4  '>
            <div className='w-2/4 flex py-2 justify-between z-10 px-2 rounded-lg'>
                    <div className='flex gap-2 mt-5'>
                    <div className='flex' >
                    { 
                            !fullReturned &&
                                <MwButton 
                                    // disabled = {!isPaid}
                                    size = 'sm'
                                    inGroup = {true}
                                    type = 'secondary' 
                                    onClick={ () => { navigate(`/${params.tenant}/invoices-returns/${invId}`) } }>
                                        {t("invoice.create_return_invoice")}
                                </MwButton>
                        }
                        {
                            !isPaid &&  
                                <MwButton 
                                    disabled = {isPaid}
                                    size = 'sm'
                                    inGroup = {true}
                                    type = 'saveBtn' 
                                    onClick={ () => setShowPaymentModal(!showpaymentModal) }>
                                        <MdOutlinePaid size={16}/> {t('invoice.PayInvoice')}
                                </MwButton>
                        }
                    </div>
                      

                        <BasicSelector
                            className={`rounded-xl text-slate-500`}
                                label={invoiceTypeSelected.label}
                                defaultValue={invoiceTypeSelected}
                                onSelected={OnChangeInvoiceType}
                                listItem={invoiceTypeList}
                            />
                        <BasicSelector
                            className={`rounded-xl text-slate-500`}
                                label={`ارسال نسخة للعميل pdf`}
                                onSelected={onSelectSendInvoiceToCustomer}
                                listItem={[
                                    { label: t("invoice.email"), value: 'sendMail' },
                                { label: t("invoice.sms"), value: 'sendSms' }
                                ]}
                            />
                    </div>
                    <div className='flex items-center justify-center gap-1  text-xs'>
                      
             
                    </div>
                </div>

                <div className={`relative flex items-center  gap-1 z-10`}>

    <div className=" flex flex-col items-center">
    <div className='flex gap-2'>
    {isPaid ? <div className='text-green-300 px-3 py-1 text-xs  border border-green-300 rounded-xl'>{t('invoice.PAID')}</div> : <div className='text-red-300 px-3 text-xs py-1 border border-red-300 '>{t('invoice.UNPAID')}</div>}
                        {isReturned && countReturned > 0 && 
                            <div className='text-orange-300 px-3 py-1 text-xs border border-orange-300 rounded-xl'> 
                            
                            { fullReturned ? <span className='text-xs'> {t("invoice.fully_returned_invoice")}    </span> :  <span className='text-xs'>     {t("invoice.partially_returned_invoice")}</span>}
                            <span> [{countReturned}] </span>
                            </div> }
    </div>
    <div className="p-3 text-slate-500 flex ">
      <ul className="flex gap-1">
        
        {/* <li
          style={{ cursor: "pointer" }}
          onClick={html2pdf }
          className="px-2 py-1 flex items-center gap-1 bg-slate-100 hover:font-bold"
        >
          <FaFilePdf />
          <span className="text-sm text-slate-500">Save_as_PDF</span>
        </li> */}
         {/* <li className="px-2 py-1 flex items-center gap-1 bg-slate-100 hover:font-bold">
            <InvoicePDFDownload invoiceData={pdfInvoiceData} />
        </li>  */}
        <li
          style={{ cursor: "pointer" }}
          onClick={handlePrint}
          className="px-2 py-1 flex items-center gap-1 bg-slate-100 hover:font-bold"
        >
          <FaPrint />
          <span className="text-sm text-slate-500">{t("common.print")}</span>
        </li>
      </ul>
    </div>

   

    </div>
    
   
  </div>

                </div>
        </>
    )
}

export default InvoiceNavbar