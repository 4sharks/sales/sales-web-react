import React from 'react';

const InvoicePDFDownload = ({ invoiceData }) => {
    const handleDownloadPDF = async () => {
        try {
            // Calculate total products and discount
            let totalAmount = 0;
            let totalDiscount = 0;
            
            const formattedProducts = [];
            
            if (invoiceData.invProducts && invoiceData.invProducts.length > 0) {
                invoiceData.invProducts.forEach(product => {
                    // Make sure all numeric values are parsed as floats
                    const price = parseFloat(product.price || 0);
                    const qty = parseFloat(product.qty || 0);
                    const totalPrice = parseFloat(product.total_price || 0);
                    const discount = parseFloat(product.discount || 0);
                    
                    totalAmount += totalPrice;
                    totalDiscount += discount;
                    
                    // Format each product with proper number types
                    formattedProducts.push({
                        product_name: product.product_name || '',
                        qty: qty,
                        unit: product.unit || '',
                        price: price,
                        total_price: totalPrice,
                        discount: discount
                    });
                });
            }
            
            // Calculate VAT and total
            const vatPercent = parseFloat(invoiceData.vatPercent || 15);
            const totalAfterDiscount = totalAmount - totalDiscount;
            const vatAmount = invoiceData.vatAmount ? parseFloat(invoiceData.vatAmount) : (totalAfterDiscount * (vatPercent / 100));
            const netAmount = invoiceData.netAmount ? parseFloat(invoiceData.netAmount) : (totalAfterDiscount + vatAmount);

            // Format data to match what the backend expects
            const formattedData = {
                companyName: invoiceData.companyName,
                commercialRegisteration: invoiceData.commercialRegisteration,
                vatNo: invoiceData.vatNo,
                companyAddress: invoiceData.companyAddress,
                // Add the new footer data
                companyPhone: invoiceData.companyPhone || '',
                companyEmail: invoiceData.companyEmail || '',
                companyWebsite: invoiceData.companyWebsite || '',
                inv_date: invoiceData.inv_date,
                inv_no: invoiceData.inv_no,
                inv_date_due: invoiceData.inv_date_due || '',
                invProducts: formattedProducts,
                totalAmount: totalAmount,
                vatAmount: vatAmount,
                netAmount: netAmount,
                vatPercent: vatPercent,
                logo: invoiceData.logo || null,
                // Add terms and conditions data
                termsAndConditions: invoiceData.termsAndConditions || null
            };

            const response = await fetch('http://localhost:3001/generate-pdf', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formattedData)
            });

            if (!response.ok) {
                const errorText = await response.text();
                throw new Error(`PDF generation failed: ${errorText}`);
            }

            const blob = await response.blob();
            
            const url = window.URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.href = url;
            a.download = `invoice-${invoiceData.inv_no || 'unnamed'}.pdf`;
            
            document.body.appendChild(a);
            a.click();
            
            window.URL.revokeObjectURL(url);
            document.body.removeChild(a);

        } catch (error) {
            console.error('Error downloading PDF:', error);
            alert('Failed to download PDF: ' + error.message);
        }
    };

    return (
        <div onClick={handleDownloadPDF} className="flex items-center gap-1 cursor-pointer">
            <span>تحميل PDF</span>
        </div>
    );
};

export default InvoicePDFDownload;