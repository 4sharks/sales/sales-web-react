import React from 'react'
import {  useTranslation } from 'react-i18next';

const InvoiceBody = ({
    customerName,
    customer,
    salesmanName,
    invProducts,
    isDelivery,
    SETTING_INV_CURRENCY,
    SETTING_INV_VAT_PERCENT,
    formatter,
    SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION,
    SETTING_INV_QTY_DAYS,
    SETTING_INV_SHOW_ITEM_T_A_VAT,
    SETTING_INV_UOM_SHOW
}) => {
    console.log('invoice invProducts',invProducts);
    console.log('invoice customer',customer);

    const [t] = useTranslation('global')
    const invProductsList = invProducts.map((prod) => (
        <div key={prod.id} className={`flex justify-between border-b  ${ prod.is_returned && 'bg-orange-50'}`}>
            <div className='flex-1 p-2 border-e'>{prod.product_name} {prod.is_returned && <span className='font-bold text-orange-300'>[مرتجع]</span> } 
                <div className='text-xs text-slate-400 px-1'>
                        {prod?.product_desc} 
                </div>
                {SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION === 'true' && 
                    <div className='text-xs text-slate-400 px-1'>
                        {prod?.product_desc} 
                    </div>
                }
            </div>
            <div className='w-12 flex items-center justify-center text-center border-e p-2'>{prod.qty}</div>
            { SETTING_INV_UOM_SHOW === 'true' && <div className='w-12 flex items-center justify-center text-center border-e p-2'>{prod?.uom_relation?.short_name}</div> }
            { SETTING_INV_QTY_DAYS === 'true' && <div className='w-12 flex items-center justify-center text-center border-e p-2'>{prod.qty_days}</div> }
            {!isDelivery && <div className='w-20 flex items-center justify-center text-center  border-e p-2'>{formatter.format(prod.price).slice(0,-6)} </div>}
            {!isDelivery && <div className='w-24 flex items-center justify-center text-center border-e p-2'>{formatter.format(prod.total_price).slice(0,-6)} </div>}
            {!isDelivery && <div className='w-14 flex items-center justify-center text-center border-e p-2'>{formatter.format(prod.product_discount).slice(0,-6)} </div>}
            {!isDelivery && <div className='w-24 flex items-center justify-center text-center border-e p-2'>{formatter.format(prod.product_net_total).slice(0,-6)} </div>}
            {/* {!isDelivery && <div className='w-24 text-center border-e p-2'>{formatter.format(prod.product_vat).slice(0,-6)} </div>} */}
            {!isDelivery && SETTING_INV_SHOW_ITEM_T_A_VAT === "true" && <div className='w-20 flex items-center justify-center text-center border-e p-2'>{formatter.format(prod.product_vat)} </div>}
            {!isDelivery && SETTING_INV_SHOW_ITEM_T_A_VAT === "true" && <div className='w-24 flex items-center justify-center text-center border-e p-2'>{formatter.format(prod.product_net_total_with_vat).slice(0,-6)} </div>}
        </div>
    ));

    return (
        <>
            <div className='text-xs '>
                <div className='flex justify-between text-xs  pb-3  '>
                    {customer && customerName && 
                        <div className='border flex-1 flex flex-col text-center gap-1 p-2 '> 
                            <div className='flex justify-between '>
                                <span>اسم العميل</span>
                                <span>CUSTOMER NAME</span>
                            </div>
                            <span className='font-bold'>{customer?.fullname[0].text || customer?.fullname[1].text }</span>
                        </div>
                    }
                    {customer?.customerNo && 
                        <div className='border flex-1 flex flex-col text-center gap-1 p-2 '>
                            <div className='flex justify-between '>
                                <span>رقم العميل</span>    
                                <span >CUSTOMER NO</span>
                            </div>
                            <span className='font-bold'>{customer.customerNo}</span>
                        </div>
                    }
                    {customer?.customerVatNo && 
                        <div className='border flex-1 flex flex-col text-center gap-1 p-2'>
                            <div className='flex justify-between '>
                                <span className='text-xs'>الرقم الضريبي للعميل</span>
                                <span>CUSTOMER VAT NO</span>
                            </div>
                            <span className='font-bold'>{customer.customerVatNo}</span>
                        </div>}
                    {customer?.customerAddresses.length > 0 && 
                        <div className='border flex-1 flex flex-col text-center gap-1 p-2'>
                            <div className='flex justify-between '>
                                <span className='text-xs'> عنوان العميل</span>
                                <span>CUSTOMER Address </span>
                            </div>
                            <span className='font-bold '>{customer.customerAddresses[0].desc}</span>
                        </div>}
                    {/* {salesmanName && <div>{t('invoice.Salesman')}: {salesmanName}</div>} */}
                </div>
                <div  className='flex   justify-between items-center  bg-slate-100 text-xs font-bold border rounded-t'>
                    <div className='flex-1 text-center border-e flex flex-col py-2'>
                        <span>اسم المنتج - الخدمة</span>
                        <span>PRODUCT / SERVICE NAME</span>
                    </div>
                    <div className='w-12 text-center border-e flex flex-col py-2'>
                        <span className=' text-center'>الكمية</span>
                        <span>QTY</span>
                    </div>
                    {
                    SETTING_INV_UOM_SHOW === 'true' && 
                        <div className='w-12 text-center border-e flex flex-col  py-2'>
                            <span>الوحدة</span>
                            <span>UNIT</span>
                        </div>
                    }
                    {
                    SETTING_INV_QTY_DAYS === 'true' && 
                        <div className='w-12 text-center border-e flex flex-col  py-2'>
                            <span> الايام </span>
                            <span>DAYS</span>
                        </div>
                    }
                    {
                    !isDelivery && 
                        <div className='w-20 text-center border-e flex flex-col py-2 '>
                            <span>سعر</span>
                            <span>RATE </span>
                        </div>
                    }
                    {!isDelivery && <div className='w-24 text-center border-e flex flex-col py-2'>
                        <span>مجموع</span>
                        <span>TOTAL</span>
                        </div>}
                    {!isDelivery && <div className='w-14 text-center border-e flex flex-col py-2'>
                        <span>الخصم</span>
                        <span>DISCOUNT</span>
                        </div>}
                    {!isDelivery && <div className='w-24 text-center border-e   flex flex-col py-2'>
                        <span className='text-xs'>     إج. قبل الضريبة</span>
                        <span className='text-xs'>Excl. VAT</span>
                        </div>}
                    {/* {!isDelivery && <div className='w-24 text-center  flex flex-col'>
                        <span>الضريبة ({SETTING_INV_VAT_PERCENT}%)</span>
                        <span>VAT ({SETTING_INV_VAT_PERCENT}%)</span>
                        </div>} */}
                    {!isDelivery && SETTING_INV_SHOW_ITEM_T_A_VAT === "true" && <div className='w-20 text-center border-e   flex flex-col py-2'>
                        <span>  الضريبة</span>
                        <span> VAT</span>
                        </div>}
                    {!isDelivery && SETTING_INV_SHOW_ITEM_T_A_VAT === "true" && <div className='w-24 text-center border-e   flex flex-col py-2'>
                        <span>إج. بعد الضريبة</span>
                        <span>T.A VAT</span>
                        </div>}
                </div>
                <div className='border '>{invProductsList}</div>
            </div>
        </>
    )
}

export default InvoiceBody