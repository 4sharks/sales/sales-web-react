import React from 'react'

const MwButton = (props) => {
    const {
            actionType,
            onClick,
            children,
            type,
            size,
            classNameCustom,
            inGroup = false,
            disabled = false,
        } = props
        let classItem = `flex items-center justify-center gap-1  drop-shadow-md  text-sm rounded ${size ==='sm' ? ' h-6 px-2' : 'h-8 px-4' } ${classNameCustom} `;
        switch(type) {
            case 'secondary':
                classItem += ` pt-1 border border-slate-500 text-xs font-medium text-slate-500  ${ disabled || 'hover:text-slate-900 hover:border-slate-900'}   ${inGroup && 'rounded-none rounded-s-lg '}`
                break;
            case 'secondary-inline-end':
                classItem += ` border-t border-b border-e border-slate-500 text-xs font-medium text-slate-500  ${ disabled || 'hover:text-slate-900 hover:border-slate-900'}   ${inGroup && 'rounded-none  '}`
                break;
            case 'saveBtn':
                classItem +=  `border border-indigo-500 text-xs font-medium text-indigo-500  ${ disabled && 'text-slate-400 border-slate-400 hover:text-slate-400 hover:border-slate-400'}  ${inGroup && 'rounded-none rounded-e-lg '}`   
                break; 
            case 'deleteBtn':
                classItem +=  `border border-red-500 text-xs font-medium text-red-500 hover:text-red-900 hover:border-red-900  ${inGroup && 'rounded-none rounded-e-lg '}`   
                break; 
            case 'deleteBtn-inline':
                classItem +=  ` border-red-500 text-xs font-medium text-red-500 hover:text-red-900 hover:border-red-900  ${inGroup ? ' rounded-none  border-y border-slate-500' : 'border'}`   
                break; 
            case 'cancelBtn':
                classItem +=  `border border-slate-500 text-xs font-medium text-slate-500 hover:text-slate-900 hover:border-slate-900  ${inGroup && 'border-0 border-s border-t border-b rounded-none  rounded-s-lg '}`   
                break; 
            case 'bottomBtn':
                classItem +=  ` text-sm font-medium  rounded-none ${disabled ? 'bg-gray-400 text-slate-200' : 'bg-gray-700 text-slate-100 hover:bg-gray-900'}`   
                break; 
            case 'circular-left':
                classItem += ` bg-sky-500 text-white hover:bg-sky-700 ${
             disabled || 'hover:text-white'
                    } ${inGroup && 'rounded-s-full'}`;
                    break;
                  case 'circular-right':
                    classItem += ` bg-red-500 text-white hover:bg-red-700 ${
                      disabled || 'hover:text-white'
                    } ${inGroup && 'rounded-e-full'}`;
                    break; 
 case 'original-left':
    classItem += `hover:text-slate-900 hover:border-slate-900 border-y border-l border-slate-500 px-4 py-2 text-xs font-medium text-slate-500 bg-white ${disabled ? 'opacity-50' : ''} ${inGroup && 'rounded-l-full border-r-0'}`;
    break;
case 'original-right':
    classItem += `hover:text-slate-900 hover:border-slate-900 border-y border-r border-slate-500 px-4 py-2 text-xs font-medium text-slate-500 bg-white ${disabled ? 'opacity-50' : ''} ${inGroup && 'rounded-r-full border-l-0'}`;
    break;
    case 'deleteBtn':
        classItem += `bg-red-500 text-white hover:bg-red-700 px-4 py-2 border-y border-x border-red-500 ${inGroup ? 'border-r-0' : 'rounded-full'}`;
        break;
case 'middleBtn':
            classItem += ` border border-slate-500 text-xs font-medium text-slate-500 hover:text-slate-900 hover:border-slate-900 ${
              disabled && 'text-slate-400 border-slate-400 hover:text-slate-400 hover:border-slate-400'
            } ${inGroup && 'rounded-none'}`;
            break;    
    
    
        case 'cancelBtn':
            classItem += `bg-gray-500 text-white hover:bg-gray-700 px-4 py-2 border-y border-gray-500 ${inGroup ? 'border-r-0' : 'rounded-full'} ${disabled ? 'opacity-50 cursor-not-allowed' : ''}`;
            break;

            case 'circular-back':
            classItem += ` bg-green-500 text-white hover:bg-green-700 ${disabled || 'hover:text-white'} ${inGroup && 'rounded-full'}`;
            break;    
        
                    
            default :
                classItem += ' bg-sky-500 text-white hover:bg-sky-700'
        }
    return (
        <button
            type={actionType}
            onClick={onClick} 
            disabled = {disabled}
            className={classItem}>
            {children}
        </button>
    )
}

export default MwButton