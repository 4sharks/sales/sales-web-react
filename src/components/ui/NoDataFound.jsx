import React from 'react'
import { CiBatteryEmpty } from "react-icons/ci";

const NoDataFound = ({children,msg}) => {
    return (
        <div className='h-full flex flex-col items-center justify-center rounded-xl bg-slate-100 text-slate-400 font-bold text-center p-7 my-3 '>
            <div className='flex justify-between gap-1 items-center'>
                <CiBatteryEmpty size={36} />
                <span className='pt-1'>{msg}</span>
            </div>
            {
                children && 
                <div className='my-2'>
                    {children}
                </div>
            }
        </div>
    )
}

export default NoDataFound