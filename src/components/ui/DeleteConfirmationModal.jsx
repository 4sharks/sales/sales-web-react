import React from "react";
import { useTranslation } from 'react-i18next'

const DeleteConfirmationModal = ({ isOpen, onClose, onConfirm, loading }) => {
  const [t] = useTranslation('global')
  if (!isOpen) return null;

  return (
    <div  onClick={(e) => {
      e.stopPropagation(); 
    }}  className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div onClick={(e) => e.stopPropagation()} className="bg-gray-200 p-6 rounded-lg shadow-lg w-96">
      <h2 className="text-xl font-semibold mb-4 text-gray-900"> {t('deleted.delete_confirmation')}     </h2>
      <p className="text-gray-700 mb-6">{t('deleted.delete_confirmation_message')}     </p>
        <div  className="flex justify-end gap-4">
          <button
          onClick={(e) => {
            e.stopPropagation(); // منع الانتشار عند النقر على الخلفية
            onClose();
          }}
            className="px-4 py-2 bg-gray-500 text-white rounded-lg hover:bg-gray-600 transition duration-200"
          >
              {t('deleted.cancel')}
          </button>
          <button
             onClick={(e) => {
              e.stopPropagation(); 
              onConfirm();
            }}
            disabled={loading}  
            className={`px-4 py-2 ${
              loading ? "bg-red-300" : "bg-red-500"
            } text-white rounded-lg hover:bg-red-600 transition duration-200`}
          >
            {loading ? t("deleted.deleting")  :  t('deleted.confirm_delete')  }
          </button>
        </div>
      </div>
    </div>
  );
};

export default DeleteConfirmationModal;