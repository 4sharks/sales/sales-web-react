import React from 'react'

const MwCardWrapper = ({children}) => {
    return (
        <div className=' m-3 bg-slate-50 rounded-lg'>{children}</div>
    )
}

export default MwCardWrapper