import {useEffect, useState} from 'react'
import {MdOutlineAddBox} from 'react-icons/md'
import { ConfirmDelete, MwButton, MwInputText, MwSpinnerButton } from '../../components/ui';
import { useCurrent, useDelete, usePost } from '../../hooks';
import ListProductsItems from './ListProductsItems';
import NoDataFound from '../ui/NoDataFound';
import CardHeadeWithActions from '../ui/CardHeadeWithActions';
import {BiSearch} from 'react-icons/bi'
import {LangArrayToObjKeyValue} from '../../utils/global';
import { useTranslation } from 'react-i18next'
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md';


const ListProducts = ({idSelected,setIdSelected,setMode}) => {
    const [t,i18n ] = useTranslation('global')
  const isRTL = i18n.dir() === 'rtl';

    const [products,setProducts] = useState();
    const [loadingProducts,setLoadingProducts] = useState(false);
    const [term,setTerm] = useState();
    const [productsFilter,setProductsFilter] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(13);

 const handlePageChange = (pageNumber) => {
  setCurrentPage(pageNumber);
};

const indexOfLastItem = currentPage * itemsPerPage;
const indexOfFirstItem = indexOfLastItem - itemsPerPage;
const currentItems = itemsPerPage === 13 
  ? productsFilter?.slice(indexOfFirstItem, indexOfLastItem) 
  : productsFilter;
const totalPages = Math.ceil((productsFilter?.length || 0) / itemsPerPage);
    
    const { currentTenantId ,currentCompanyId,currentLangId} = useCurrent();
    const [showConfirmDelete,setShowConfirmDelete] = useState(false);
    const {data:postResult,loading:postLoading,error,postData} = usePost();
    const {data:deleteResult ,loading:deleteLoading,deleteItem} = useDelete()
    
    const getProducts = () =>{
        postData(`${process.env.REACT_APP_PRODUCTS_API_SERVER_BASE_URL}/search/All`,{
            tenantId: currentTenantId,
            companyId: currentCompanyId
        })
    }



    const onChangeSearch = (newValue) => {
       // setData(newValue);
    }
    const editHandeler = (id) =>{
        setIdSelected(id)
        setMode('Edit');
    }
    // const deleteHandeler = (id) =>{
    //     setIdSelected(id)
    //     setShowConfirmDelete(true);
    // }
    const deleteHandeler = (id) => {
               deleteItem(`${process.env.REACT_APP_PRODUCTS_API_SERVER_BASE_URL}/delete/${id}`)
           }
    // const confirmDeleteHandler = () =>{
    //     deleteItem(`${process.env.REACT_APP_PRODUCTS_API_SERVER_BASE_URL}/delete/${idSelected}`)
    // }

    useEffect(() => {
        getProducts();
    },[]);

    useEffect(()=>{
        if(postResult){
            setProducts(postResult.data)
            setProductsFilter(postResult.data)
            console.log('Products',postResult.data)
        }
    },[postResult]);


    useEffect(()=>{
        if(deleteResult && !deleteLoading){
            setShowConfirmDelete(false);
            getProducts();
        }
    },[deleteResult]);

    const onChangeHandler = () =>{
        setCurrentPage(1);
        if(term){
            const _list = products?.filter((product)=>{
                const productNameAr = product?.productName[0]?.text
                const productNameEn = product?.productName[1]?.text 
                if(productNameAr?.toLocaleLowerCase()?.indexOf(term.toLocaleLowerCase()) !== -1 || productNameEn?.toLocaleLowerCase()?.indexOf(term.toLocaleLowerCase()) !== -1){
                    return true;
                }
                        return false;
                        
                    });
                    setProductsFilter(_list)                            
        }else{
            setProductsFilter(postResult?.data)
        }
        setLoadingProducts(false);
    }

    useEffect(() => {
        if(!term){
            setLoadingProducts(false);
            return 
        }
        setLoadingProducts(true);
        const timeout = setTimeout(() => {
            if(term){
                onChangeHandler();
            }
        }, 1000);
        
        return () =>{
            clearTimeout(timeout);
        }
    },[term])

    return (
        <>
           
            <div className='text-slate-500 bg-slate-50 rounded-lg z-30 h-screen max-h-[calc(100vh-300px)]   text-xs p-3'>
                <CardHeadeWithActions title={t('product.productsList')}>
                </CardHeadeWithActions>
                    <div className=' flex-1  flex gap-2 justify-between '>
                        <div className={` relative flex-1  `}>
                            <BiSearch className='absolute top-2 start-3' size={16}/>
                            <MwInputText 
                                classNameInput={`bg-slate-50 rounded-xl `}
                                placeholder = {t('product.searchProduct')}
                                value = {term}
                                onChange = {(e) => setTerm(e.target.value)}
                                />
                        </div>
                        <div className='w-40'>
                            <MwButton inGroup={true} type='saveBtn' onClick={()=>setMode('Add')}  >
                                <MdOutlineAddBox size={18}/>
                                {t('product.createNewProduct')}
                            </MwButton>
                        </div>
                    </div>
                { (postLoading || loadingProducts) && <MwSpinnerButton  withLabel={false} isFullCenter={true} />}
                {products?.length > 0 ? (
  <div className=' h-[calc(100%-4rem)] overflow-y-auto custom-scrollbar mt-4'>
    {currentItems?.map((item, index) => (
      <ListProductsItems 
        key={item._id} 
        item={item} 
        index={indexOfFirstItem + index} 
        editHandeler={editHandeler} 
        deleteHandeler={deleteHandeler} 
      />
    ))}
    {/* Add pagination controls */}
    {totalPages > 1 && (
  <div className="flex justify-center gap-2 mt-4 pb-4">
    <button
      onClick={() => {
        setCurrentPage(1);
        setItemsPerPage(productsFilter?.length || itemsPerPage);
      }}
      className={`px-3 py-1 rounded ${
        itemsPerPage !== 13 ? 'bg-indigo-600 text-white' : 'bg-slate-200 hover:bg-slate-300'
      }`}
    >
      {t('common.all')}
    </button>
    
    {totalPages > 5 && currentPage > 3 && (
      <button
        onClick={() => handlePageChange(currentPage - 1)}
        className="px-3 py-1 rounded bg-slate-200 hover:bg-slate-300"
      >
            {i18n.language === 'ar' ? <MdKeyboardArrowRight size={20} /> : <MdKeyboardArrowLeft size={20} />}

      </button>
    )}
    
    {Array.from({ length: totalPages }, (_, i) => i + 1)
      .filter(number => {
        if (totalPages <= 5) return true;
        if (number === 1 || number === totalPages) return true;
        return Math.abs(currentPage - number) < 3;
      })
      .map((number) => (
        <button
          key={number}
          onClick={() => {
            handlePageChange(number);
            setItemsPerPage(13);
          }}
          className={`px-3 py-1 rounded ${
            currentPage === number && itemsPerPage === 13
              ? 'bg-indigo-600 text-white'
              : 'bg-slate-200 hover:bg-slate-300'
          }`}
        >
          {number}
        </button>
      ))}
    
    {totalPages > 5 && currentPage < totalPages - 2 && (
      <button
        onClick={() => handlePageChange(currentPage + 1)}
        className="px-3 py-1 rounded bg-slate-200 hover:bg-slate-300"
      >
           {i18n.language === 'ar' ? <MdKeyboardArrowLeft size={20} /> : <MdKeyboardArrowRight size={20} />}

      </button>
    )}
  </div>
)}
  </div>
) : (
  !postLoading && <NoDataFound msg={`لا يوجد منتجات`}/>
)}
            </div>
        </>
    )
}

export default ListProducts