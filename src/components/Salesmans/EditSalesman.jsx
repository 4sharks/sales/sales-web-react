import React, { useEffect, useState } from 'react'
import {MwButton, MwSpinner} from '../../components/ui'
import {getAllSalesmanTypes, updateSalesman} from '../../services/salesmanService'
import { useCurrent, useFetch } from '../../hooks'
import { Link, useNavigate, useParams } from 'react-router-dom'
import {AiOutlineSave} from 'react-icons/ai'
import CardHeadeWithActions from '../ui/CardHeadeWithActions'
import SalesmanForm from './SalesmanForm'
import {langObjType,objLangTextHandler,LangArrayToObjKeyValue} from '../../utils/global'
import toast, { Toaster } from 'react-hot-toast';
import { useTranslation } from 'react-i18next'

const EditSalesman = ({onHide,onSalesmanCreated = ()=>{} }) => {
    const navigate = useNavigate();
    const params = useParams();
    const [t] = useTranslation('global');
    
    const id = params.id;
    const [salesmansTypes,setSalesmanTypes] = useState([]); 
    const [salesmanTypeId, setSalesmanTypeId] = useState('');   
    const {data:dataSalesman,loading:loadingSalesman} = useFetch(`${process.env.REACT_APP_Auth_API_BASE_URL}/salesmans/show/${id}`);
    const {currentTenantId,currentCompany,currentBranchId,currentCompanyId,currentLangList,currentLangId} = useCurrent()
    const [errors,setErrors] = useState({});

    const getSallesmanTypes = async () => {
        const _salesman = await getAllSalesmanTypes(currentTenantId ,currentCompanyId,currentBranchId);
        setSalesmanTypes(_salesman)
    } 

    useEffect(() => {
            getSallesmanTypes();
            console.log("salesmansTypes",salesmansTypes)
       },[]);

    // FORM STATE
    const [salesmanNo,setSalesmanNo] = useState('')
    const [fullname,setFullname] = useState(langObjType())
    const [mobileNo,setMobileNo] = useState('')
    const [email,setEmail] = useState('')
    const [salesmanNotes,setSalesmanNote] = useState('')
    const [targetMonthly,setTargetMonthly] = useState();
    const [targetYearly,setTargetYearly] = useState();
    const [isActive,setIsActive] = useState(true);

    const saveHandler = async(e) => { 
        e.preventDefault();
        let errorObj = {};
        const data={
            fullname : objLangTextHandler(fullname),
            salesmanNo,
            mobileNo,
            email,
            salesmanTypeId,
            salesmanNotes,
            targetMonthly,
            targetYearly,
            isActive,
            tenantId:currentTenantId,
            companyId:currentCompany,
            branchId:currentBranchId
        }
        console.log('data',data)
        if(!fullname || Object.values(fullname).every(val => !val || val.trim() === '')) {
            errorObj.fullname = 'Fullname is required';
        }
        if(isNaN(mobileNo)){
            errorObj.mobileNo = 'Mobile number is not a number';
        }
        if(Object.keys(errorObj).length !== 0 ){
            setErrors(errorObj)
            toast.error(t('validation.fillRequiredFields') || 'يرجى ملء الحقول المطلوبة', {
                position: "top-center",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
                rtl: true,
            });
            return
        }
        const updated = await updateSalesman(id,data)
        if(updated){
            console.log('updated',updated)
            toast.success(t('salesman.updatedSuccessfully') || 'تم تعديل المندوب بنجاح');
            setTimeout(() => {
                onHide ? cancelHandle() : navigate(`/${params.tenant}/salesmans`);
                onSalesmanCreated();
            }, 2000);
        }
    };
    const cancelHandle = () => {
        onHide();
    }
    useEffect(() => {
        if(dataSalesman){
            console.log('dataSalesman', dataSalesman);
            setSalesmanNo(dataSalesman.data?.salesmanNo)
            setFullname(LangArrayToObjKeyValue(dataSalesman.data?.fullname))
            setMobileNo(dataSalesman.data?.mobileNo)
            setEmail(dataSalesman.data?.email)
            setTargetMonthly(dataSalesman.data?.targetMonthly)
            setTargetYearly(dataSalesman.data?.targetYearly)
            setSalesmanNote(dataSalesman.data?.salesmanNotes)
            setIsActive(dataSalesman.data?.isActive)
            setSalesmanTypeId(dataSalesman.data?.salesmanTypeId?._id)
        }
    },[dataSalesman]);
    return (
        <div className={`${onHide && 'p-5'} text-sm flex flex-col gap-5`}>
            <CardHeadeWithActions title={`تعديل مندوب `}>
                        { onHide && <MwButton inGroup={true}  type='cancelBtn' onClick={cancelHandle}>الغاء</MwButton> }
                            <Link to={`/${params.tenant}/salesmans`} >
                                <MwButton inGroup={true}  type='cancelBtn' >ادارة المناديب</MwButton> 
                            </Link>
                            <MwButton inGroup={true}  type='saveBtn' actionType={`button`} onClick={saveHandler}>
                            حفظ المندوب
                            <AiOutlineSave size={18}/>
                            </MwButton>
            </CardHeadeWithActions>

            {loadingSalesman ? (
                <div className="flex justify-center items-center h-full min-h-[200px]">
                    <MwSpinner />
                </div>
            ) : (
                <SalesmanForm
                   salesmansTypes={salesmansTypes}
                   setSalesmanTypes={setSalesmanTypes}
                   salesmanTypeId = {salesmanTypeId}
                   setSalesmanTypeId = {setSalesmanTypeId}
                    currentLangList = {currentLangList}
                    currentLangId = {currentLangId}
                    errors = {errors}
                    salesmanNo = {salesmanNo}
                    setSalesmanNo = {setSalesmanNo}
                    fullname = {fullname}
                    setFullname = {setFullname}
                    mobileNo   = {mobileNo}
                    setMobileNo = {setMobileNo}
                    email = {email}
                    setEmail = {setEmail}
                    salesmanNotes = {salesmanNotes}
                    setSalesmanNote = {setSalesmanNote}
                    targetMonthly = {targetMonthly}
                    setTargetMonthly = {setTargetMonthly}
                    targetYearly = {targetYearly}
                    setTargetYearly = {setTargetYearly}
                    isActive = {isActive}
                    setIsActive = {setIsActive}
                    />
            )}
        </div>
    )
}

export default EditSalesman