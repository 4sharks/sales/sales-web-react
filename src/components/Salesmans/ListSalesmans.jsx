import { useState, useEffect } from 'react';
import { NoDataFound, MwSpinner } from '../ui';
import { useDelete } from '../../hooks';
import { useNavigate, useParams } from 'react-router-dom';
import ListSalesmanItem from './ListSalesmanItem';
import DeleteConfirmationModal from '../ui/DeleteConfirmationModal';

const ListSalesmans = ({data, setData, currentCompanyId, currentBranchId, currentLangId, refreshResult}) => {
    const navigate = useNavigate();
    const params = useParams();
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [selectedItemId, setSelectedItemId] = useState(null);
    const [loading, setLoading] = useState(true);

    const {data: resultDelete, loading: deleteLoading, deleteItem} = useDelete();
    console.log("data from list",data);
    

    useEffect(() => {
        if (data) {
            setLoading(false);
        }
    }, [data]);

    const editHandeler = (id) => {
        navigate(`/${params.tenant}/salesmans/edit/${id}`);
    }

    const deleteHandeler = (id) => {
        setSelectedItemId(id);
        setShowDeleteModal(true);
    }
    
    const confirmDeleteHandler = async () => {
        if (selectedItemId) {
            setLoading(true);
            await deleteItem(`${process.env.REACT_APP_Auth_API_BASE_URL}/salesmans/delete/${selectedItemId}`);
            const filteredData = data.filter(el => el._id !== selectedItemId);
            setData(filteredData);
            setShowDeleteModal(false);
            setLoading(false);
        }
    }

    useEffect(() => {
        if (resultDelete) {
            setShowDeleteModal(false);
        }
    }, [resultDelete]);

    return (
        <>
            <DeleteConfirmationModal
                isOpen={showDeleteModal}
                onClose={() => setShowDeleteModal(false)}
                onConfirm={confirmDeleteHandler}
                loading={deleteLoading}
            />

            <div className='text-slate-500 bg-slate-50 rounded-lg text-xs p-3'>
                <div className='flex items-center'>
                    <div className='flex-1 font-bold text-slate-400'>قائمة المناديب</div>
                </div>
                <div className="min-h-[200px]">
                    {loading || deleteLoading ? (
                        <div className="flex justify-center items-center h-full min-h-[200px]">
                            <MwSpinner />
                        </div>
                    ) : data?.length > 0 ? (
                        <div className='pt-3'>
                            {data.map((item, index) => (
                                <ListSalesmanItem
                                    key={item._id}
                                    index={index}
                                    item={item}
                                    currentLangId={currentLangId}
                                    currentCompanyId={currentCompanyId}
                                    currentBranchId={currentBranchId}
                                    editHandeler={editHandeler}
                                    deleteHandeler={deleteHandeler}
                                />
                            ))}
                        </div>
                    ) : (
                        <div className="flex items-center justify-center h-full">
                            <NoDataFound msg={`لا يوجد مناديب`} />
                        </div>
                    )}
                </div>
            </div>
        </>
    )
}

export default ListSalesmans