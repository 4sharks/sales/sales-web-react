import { useState, useEffect } from 'react';
import { MwInputText, MwSelector, MwSwitch, MwTextArea } from '../../../components/ui';
import FormLangBar from '../../FormLangBar';
import { useTranslation } from 'react-i18next';

const SalesmanTypeForm = ({
    currentLangList,
    currentLangId,
    errors,
    typeName,
    setTypeName,
    typeDesc,
    setTypeDesc,
    typeCommission,
    setTypeCommission,
    typeCommissionValue,
    setTypeCommissionValue,
    isActive,
    setIsActive,
    isEditMode = false // إضافة وسيط جديد لمعرفة إذا كانت الصفحة في وضع التعديل
}) => {
    // استخدام متغيرات منفصلة للغة لكل قسم
    const [typeNameLang, setTypeNameLang] = useState(currentLangId);
    const [typeDescLang, setTypeDescLang] = useState(currentLangId);
    const [t] = useTranslation('global');

    // خيارات نوع العمولة
    const commissionTypes = [
        { value: "Percent", label: t('salesman.percentCommission') || "نسبة مئوية" },
        { value: "Fixed", label: t('salesman.fixedCommission') || "قيمة ثابتة" }
    ];

    // تكوين البيانات بالشكل المطلوب لـ MwSelector
    const commissionTypeData = commissionTypes.map(type => ({
        _id: type.value,
        label: type.label,
        lang: currentLangId
    }));

    // تحديد العنصر المحدد حاليًا
    const selectedCommissionType = commissionTypeData.find(item => item._id === typeCommission) || {
        _id: "Percent",
        label: t('salesman.percentCommission') || "نسبة مئوية",
        lang: currentLangId
    };

    // التحقق من صحة قيمة العمولة عند الإدخال
    const handleCommissionValueChange = (e) => {
        const value = e.target.value;
        
        // التحقق من أن القيمة عددية
        if (value === '' || !isNaN(value)) {
            // إذا كان نوع العمولة نسبة مئوية، تأكد من أن القيمة لا تتجاوز 100
            if (typeCommission === 'Percent') {
                if (value === '' || parseFloat(value) <= 100) {
                    setTypeCommissionValue(value);
                }
            } else {
                setTypeCommissionValue(value);
            }
        }
    };

    return (
        <form>
            <div className='p-3 bg-slate-50 rounded-lg mb-3 text-sm flex flex-col gap-1'>
                {/* حقل اسم النوع */}
                <div className='py-2'>
                    <FormLangBar currentLangList={currentLangList} formLang={typeNameLang} setFormLang={setTypeNameLang} />
                    {currentLangList.map((lang) => (
                        <div key={`typeName-${lang.langCode}`} className=''>
                            <div className={`${typeNameLang === lang?.langCode ? 'block' : 'hidden'}`}>
                                <MwInputText
                                    label={`${t('salesman.typeName') || 'نوع المندوب'} (${lang?.langName})`}
                                    id={`typeName${lang?.langCode}`}
                                    value={typeName[lang?.langCode] || ''}
                                    invalid={!!errors[`typeName.${lang?.langCode}`]}
                                    invalidMsg={errors[`typeName.${lang?.langCode}`]}
                                    onChange={(e) => {
                                        setTypeName({ ...typeName, [lang?.langCode]: e.target.value });
                                    }}
                                />
                            </div>
                        </div>
                    ))}
                </div>

                {/* حقل وصف النوع */}
                <div className='py-2'>
                    <FormLangBar currentLangList={currentLangList} formLang={typeDescLang} setFormLang={setTypeDescLang} />
                    {currentLangList.map((lang) => (
                        <div key={`typeDesc-${lang.langCode}`} className=''>
                            <div className={`${typeDescLang === lang?.langCode ? 'block' : 'hidden'}`}>
                                <MwTextArea
                                    label={`${t('salesman.typeDesc') || 'وصف النوع'} (${lang?.langName})`}
                                    id={`typeDesc${lang?.langCode}`}
                                    value={typeDesc[lang?.langCode] || ''}
                                    rows={2}
                                    invalid={!!errors[`typeDesc.${lang?.langCode}`]}
                                    invalidMsg={errors[`typeDesc.${lang?.langCode}`]}
                                    onChange={(e) => {
                                        setTypeDesc({ ...typeDesc, [lang?.langCode]: e.target.value });
                                    }}
                                />
                            </div>
                        </div>
                    ))}
                </div>

                {/* حقل نوع العمولة */}
                <div className='py-2'>
                    <MwSelector
                        label={t('salesman.commissionType') || 'نوع العمولة'}
                        _data={commissionTypeData}
                        dataType="labelValue"
                        selectedItem={selectedCommissionType}
                        setSelectedItem={(item) => setTypeCommission(item._id)}
                        invalid={!!errors.typeCommission}
                        invalidMsg={errors.typeCommission}
                        withAddNew={false}
                    />
                </div>

                {/* حقل قيمة العمولة */}
                <div className='py-2'>
                    <MwInputText
                        label={
                            typeCommission === 'Percent'
                                ? t('salesman.commissionPercent') || 'نسبة العمولة %'
                                : t('salesman.commissionValue') || 'قيمة العمولة'
                        }
                        id='typeCommissionValue'
                        value={typeCommissionValue}
                        onChange={handleCommissionValueChange}
                        invalid={!!errors.typeCommissionValue}
                        invalidMsg={errors.typeCommissionValue}
                        type="number"
                        min={0}
                        max={typeCommission === 'Percent' ? 100 : undefined}
                        step="any"
                        placeholder={typeCommission === 'Percent' ? 'أدخل النسبة (0-100)' : 'أدخل القيمة'}
                        required={true}
                    />
                </div>

                {/* زر التفعيل */}
                <div className='flex justify-between items-center py-4 mb-2'>
                    <label className='text-xs text-slate-400'>{t('salesman.activeInactive') || 'نشط / غير نشط'}</label>
                    <MwSwitch custKey='isActive' isActive={isActive} setIsActive={setIsActive} onChangeSwitch={() => setIsActive(!isActive)} />
                </div>
            </div>
        </form>
    )
}

export default SalesmanTypeForm