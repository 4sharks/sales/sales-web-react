import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useCurrent, usePost } from "../../../hooks";
import CardHeadeWithActions from "../../ui/CardHeadeWithActions";
import { MwButton } from "../../ui";
import { AiOutlineSave } from "react-icons/ai";
import SalesmanTypeForm from './SalesmanTypeForm';
import { langObjType } from "../../../utils/global";
import { useTranslation } from "react-i18next";
import toast, { Toaster } from 'react-hot-toast';

const CreateSalesmanType = ({ onHide, onSalesmanCreated = () => {} }) => {
    const navigate = useNavigate();
    const params = useParams();
    const { currentTenantId, currentCompany, currentBranchId, currentLangList, currentLangId } = useCurrent();
    const { data: resultPost, loading: loadingPost, postData } = usePost();
    const [t] = useTranslation('global');
    const [errors, setErrors] = useState({});
    
    // FORM STATE
    const [typeName, setTypeName] = useState(langObjType());
    const [typeDesc, setTypeDesc] = useState(langObjType());
    const [typeCommission, setTypeCommission] = useState('Percent');
    const [typeCommissionValue, setTypeCommissionValue] = useState('');
    const [isActive, setIsActive] = useState(true);

    const saveHandler = async(e) => { 
        e.preventDefault();
        let errorObj = {};
        
         if (!typeName['ar'] || typeName['ar'].trim() === '') {
            errorObj[`typeName.ar`] = `${t('validation.required') || 'هذا الحقل مطلوب'}`;
        }
        
        if (!typeCommission) {
            errorObj.typeCommission = t('validation.required') || 'هذا الحقل مطلوب';
        }
        
        if (!typeCommissionValue || typeCommissionValue.trim() === '') {
            errorObj.typeCommissionValue = t('validation.required') || 'هذا الحقل مطلوب';
        }
        
        if (Object.keys(errorObj).length !== 0) {
            setErrors(errorObj);
            return;
        }
        
         const typeNameData = Object.keys(typeName).map(langCode => ({
            text: typeName[langCode] || "",
            lang: langCode
        })).filter(item => item.text.trim() !== "");
        
        const typeDescData = Object.keys(typeDesc).map(langCode => ({
            text: typeDesc[langCode] || "",
            lang: langCode
        })).filter(item => item.text.trim() !== "");
        
        const data = {
            typeName: typeNameData,
            typeDesc: typeDescData,
            typeCommission,
            typeCommissionValue: Number(typeCommissionValue),
            isActive,
            tenantId: currentTenantId,
            companyId: currentCompany,
            branchId: currentBranchId
        };
        
        console.log("Sending data:", data);
        postData(`${process.env.REACT_APP_Auth_API_BASE_URL}/salesmans/types`, data);
    };

    const cancelHandle = () => {
        onHide && onHide();
    };

    useEffect(() => {
        if (resultPost) {
            toast.success('تم إضافة نوع المندوب بنجاح');
            setTimeout(() => {
                onHide ? cancelHandle() : navigate(`/${params.tenant}/salesmans`, { state: { activeTab: 'SalesmanType' } });
                onSalesmanCreated();
            }, 2000);
        }
    }, [resultPost]);

    return (
        <div className={`${onHide && 'p-5'} text-sm flex flex-col gap-5`}>
          <Toaster
  position="top-center"
  containerStyle={{
    top: 60, 
  }}
  toastOptions={{
    duration: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    draggable: true,
    pauseOnHover: true,
    style: {
      direction: "rtl"
    }
  }}
/>
            <CardHeadeWithActions title={`${t('salesman.addNewType') || 'أضف نوع جديد'}`}>
                {onHide && <MwButton inGroup={true} type='cancelBtn' onClick={cancelHandle}>{t('salesman.cancel') || 'الغاء'}</MwButton>}
                <Link to={`/${params.tenant}/salesmans`} state={{ activeTab: 'SalesmanType' }}>
                    <MwButton inGroup={true} type='cancelBtn'>{t('salesman.manageTypes') || 'ادارة الانواع'}</MwButton> 
                </Link>
                <MwButton inGroup={true} type='saveBtn' actionType={`button`} onClick={saveHandler} disabled={loadingPost}>
                    {loadingPost ? (t('salesman.saving') || 'جاري الحفظ...') : (t('salesman.save') || 'حفظ')}
                    <AiOutlineSave size={18}/>
                </MwButton>
            </CardHeadeWithActions>

            <SalesmanTypeForm
                currentLangList={currentLangList}
                currentLangId={currentLangId}
                errors={errors}
                typeName={typeName}
                setTypeName={setTypeName}
                typeDesc={typeDesc}
                setTypeDesc={setTypeDesc}
                typeCommission={typeCommission}
                setTypeCommission={setTypeCommission}
                typeCommissionValue={typeCommissionValue}
                setTypeCommissionValue={setTypeCommissionValue}
                isActive={isActive}
                setIsActive={setIsActive}
                isEditMode={false}
            />
        </div>
    )
}

export default CreateSalesmanType