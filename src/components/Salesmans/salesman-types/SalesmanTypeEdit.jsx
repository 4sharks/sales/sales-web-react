import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useCurrent } from "../../../hooks";
import useFetch from "../../../hooks/useFetch";
import usePut from "../../../hooks/usePut";
import CardHeadeWithActions from "../../ui/CardHeadeWithActions";
import { MwButton, MwSpinner } from "../../ui";
import { AiOutlineSave } from "react-icons/ai";
import SalesmanTypeForm from './SalesmanTypeForm';
import { langObjType } from "../../../utils/global";
import { useTranslation } from "react-i18next";
import toast, { Toaster } from 'react-hot-toast';


const EditSalesmanType = ({ onHide, onSalesmanUpdated = () => {} }) => {
    const navigate = useNavigate();
    const params = useParams();
    const { currentTenantId, currentCompany, currentBranchId, currentLangList, currentLangId } = useCurrent();
    const { data: resultPut, loading: loadingPut, putData } = usePut();
    const [t] = useTranslation('global');
    const [errors, setErrors] = useState({});
    
    // Get salesman type ID from URL parameters
    const salesmanTypeId = params.id;
    
    // Fetch salesman type data
    const { data: salesmanTypeData, loading: loadingData, error: fetchError } = useFetch(
        `${process.env.REACT_APP_Auth_API_BASE_URL}/salesmans/types/show/${salesmanTypeId}`
    );
    
    // FORM STATE
    const [typeName, setTypeName] = useState(langObjType());
    const [typeDesc, setTypeDesc] = useState(langObjType());
    const [typeCommission, setTypeCommission] = useState('Percent');
    const [typeCommissionValue, setTypeCommissionValue] = useState('');
    const [isActive, setIsActive] = useState(true);
    const [dataLoaded, setDataLoaded] = useState(false);

    // Populate form data when fetched
    useEffect(() => {
        if (salesmanTypeData && !dataLoaded) {
            console.log("salesmanTypeData", salesmanTypeData);
            const data = salesmanTypeData.data; // Access the data property from the response
            
            // Process type name data
            const nameObj = langObjType();
            if (data.typeName && Array.isArray(data.typeName)) {
                data.typeName.forEach(item => {
                    if (item && item.lang && item.text) {
                        nameObj[item.lang] = item.text;
                    }
                });
            }
            setTypeName(nameObj);
            
            // Process type description data
            const descObj = langObjType();
            if (data.typeDesc && Array.isArray(data.typeDesc)) {
                data.typeDesc.forEach(item => {
                    if (item && item.lang && item.text) {
                        descObj[item.lang] = item.text;
                    }
                });
            }
            setTypeDesc(descObj);
            
            // Set other fields
            if (data.typeCommission) {
                setTypeCommission(data.typeCommission);
            }
            
            // Handle the null or undefined typeCommissionValue appropriately
            const commissionValue = data.typeCommissionValue;
            if (commissionValue !== null && commissionValue !== undefined) {
                setTypeCommissionValue(commissionValue.toString());
            }
            
            // Set active status
            setIsActive(data.isActive !== undefined ? data.isActive : true);
            
            // Mark data as loaded
            setDataLoaded(true);
        }
    }, [salesmanTypeData, dataLoaded]);

    const saveHandler = async(e) => { 
        e.preventDefault();
        let errorObj = {};
        
        // فحص الحقول المطلوبة - العربية فقط
        if (!typeName['ar'] || typeName['ar'].trim() === '') {
            errorObj[`typeName.ar`] = `${t('validation.required') || 'هذا الحقل مطلوب'}`;
        }
        
        if (!typeCommission) {
            errorObj.typeCommission = t('validation.required') || 'هذا الحقل مطلوب';
        }
        
        if (!typeCommissionValue && typeCommissionValue !== '0') {
            errorObj.typeCommissionValue = t('validation.required') || 'هذا الحقل مطلوب';
        }
        
        if (Object.keys(errorObj).length !== 0) {
            setErrors(errorObj);
            toast.error(t('validation.fillRequiredFields') || 'يرجى ملء الحقول المطلوبة', )

            return;
        }
        
         const typeNameData = Object.keys(typeName).map(langCode => ({
            text: typeName[langCode] || "",
            lang: langCode
        })).filter(item => item.text.trim() !== "");
        
        const typeDescData = Object.keys(typeDesc).map(langCode => ({
            text: typeDesc[langCode] || "",
            lang: langCode
        })).filter(item => item.text.trim() !== "");
        
        const data = {
            typeName: typeNameData,
            typeDesc: typeDescData,
            typeCommission,
            typeCommissionValue: typeCommissionValue ? Number(typeCommissionValue) : 0,
            isActive,
            tenantId: currentTenantId,
            companyId: currentCompany,
            branchId: currentBranchId
        };
        
        console.log("Sending update data:", data);
        putData(`${process.env.REACT_APP_Auth_API_BASE_URL}/salesmans/types/update/${salesmanTypeId}`, data);
    };

    const cancelHandle = () => {
        onHide && onHide();
    };

    useEffect(() => {
        if (resultPut) {
            toast.success('تم تعديل نوع المندوب بنجاح');
            setTimeout(() => {
                onHide ? cancelHandle() : navigate(`/${params.tenant}/salesmans`, { state: { activeTab: 'SalesmanType' } });
                onSalesmanUpdated();
            }, 2000);
        }
    }, [resultPut]);

    return (
        <div className={`${onHide && 'p-5'} text-sm flex flex-col gap-5`}>
          <Toaster
  position="top-center"
  containerStyle={{
    top: 60, 
  }}
  toastOptions={{
    duration: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    draggable: true,
    pauseOnHover: true,
    style: {
      direction: "rtl"
    }
  }}
/>
            <CardHeadeWithActions title={`${t('salesman.editType') || 'تعديل النوع'}`}>
                {onHide && <MwButton inGroup={true} type='cancelBtn' onClick={cancelHandle}>{t('salesman.cancel') || 'الغاء'}</MwButton>}
                <Link to={`/${params.tenant}/salesmans`} state={{ activeTab: 'SalesmanType' }}>
                    <MwButton inGroup={true} type='cancelBtn'>{t('salesman.manageTypes') || 'ادارة الانواع'}</MwButton> 
                </Link>
                <MwButton inGroup={true} type='saveBtn' actionType={`button`} onClick={saveHandler} disabled={loadingPut || loadingData}>
                    {loadingPut ? (t('salesman.saving') || 'جاري الحفظ...') : (t('salesman.save') || 'حفظ')}
                    <AiOutlineSave size={18}/>
                </MwButton>
            </CardHeadeWithActions>

            {loadingData ? (
                <div className="flex justify-center items-center min-h-[200px]">
                    <MwSpinner />
                </div>
            ) : fetchError ? (
                <div className="text-center p-4 text-red-500">{t('salesman.fetchError') || 'حدث خطأ أثناء تحميل البيانات'}</div>
            ) : (
                <SalesmanTypeForm
                    currentLangList={currentLangList}
                    currentLangId={currentLangId}
                    errors={errors}
                    typeName={typeName}
                    setTypeName={setTypeName}
                    typeDesc={typeDesc}
                    setTypeDesc={setTypeDesc}
                    typeCommission={typeCommission}
                    setTypeCommission={setTypeCommission}
                    typeCommissionValue={typeCommissionValue}
                    setTypeCommissionValue={setTypeCommissionValue}
                    isActive={isActive}
                    setIsActive={setIsActive}
                    isEditMode={true}
                />
            )}
        </div>
    )
}

export default EditSalesmanType