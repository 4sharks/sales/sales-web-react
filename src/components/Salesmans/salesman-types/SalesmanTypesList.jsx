import { useState, useEffect } from 'react';
import { NoDataFound, MwSpinner } from '../../ui'
import DeleteConfirmationModal from '../../ui/DeleteConfirmationModal';
import { useDelete } from '../../../hooks';
import { useNavigate, useParams } from 'react-router-dom';
import SalesmanTypesListItem from './SalesmanTypesListItem';

const SalesmanTypesList = ({data, setData, currentCompanyId, currentBranchId, currentLangId, refreshResult, loading}) => {
    const navigate = useNavigate();
    const params = useParams();
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [selectedItemId, setSelectedItemId] = useState(null);
    console.log("data from list",data);
    

    const {data: resultDelete, loading: deleteLoading, deleteItem} = useDelete();

    const onChangeSearch = (newValue) => {
        setData(newValue);
    }

    const editHandeler = (id) => {
        navigate(`/${params.tenant}/salesmans/types/edit/${id}`);
    }

    const deleteHandeler = (id) => {
        setSelectedItemId(id);
        setShowDeleteModal(true);
    }
    
    const confirmDeleteHandler = async () => {
        if (selectedItemId) {
            await deleteItem(`${process.env.REACT_APP_Auth_API_BASE_URL}/salesmans/types/delete/${selectedItemId}`);
            const filteredData = data.filter(el => el._id !== selectedItemId);
            setData(filteredData);
            setShowDeleteModal(false);
        }
    }

    useEffect(() => {
        if (resultDelete) {
            setShowDeleteModal(false);
        }
    }, [resultDelete]);

    return (
        <>
            <DeleteConfirmationModal
                isOpen={showDeleteModal}
                onClose={() => setShowDeleteModal(false)}
                onConfirm={confirmDeleteHandler}
                loading={deleteLoading}
            />

            <div className='text-slate-500 bg-slate-50 rounded-lg text-xs p-3'>
                <div className='flex items-center'>
                    <div className='flex-1 font-bold text-slate-400'>قائمة انواع المناديب</div>
                </div>
                <div className="min-h-[200px]">
                    {loading ? (
                        <div className="flex justify-center items-center h-full">
                            <MwSpinner />
                        </div>
                    ) : data?.length > 0 ? (
                        <div className='pt-3'>
                            {data.map((item, index) => (
                                <SalesmanTypesListItem
                                    key={item._id}
                                    index={index}
                                    item={item}
                                    currentLangId={currentLangId}
                                    currentCompanyId={currentCompanyId}
                                    currentBranchId={currentBranchId}
                                    editHandeler={editHandeler}
                                    deleteHandeler={deleteHandeler}
                                />
                            ))}
                        </div>
                    ) : (
                        <div className="flex items-center justify-center h-full">
                            <NoDataFound msg={`لا يوجد انواع للمناديب`} />
                        </div>
                    )}
                </div>
            </div>
        </>
    )
}

export default SalesmanTypesList