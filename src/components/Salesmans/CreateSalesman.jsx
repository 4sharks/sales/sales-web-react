import React, { useEffect, useState } from 'react'
import {MwButton} from '../../components/ui'
import {addSalesman, getAllSalesmanTypes} from '../../services/salesmanService'
import { useCurrent } from '../../hooks'
import { Link, useNavigate, useParams } from 'react-router-dom'
import {AiOutlineSave} from 'react-icons/ai'
import CardHeadeWithActions from '../ui/CardHeadeWithActions'
import SalesmanForm from './SalesmanForm'
import {langObjType,objLangTextHandler} from '../../utils/global'
import { useTranslation } from 'react-i18next'
import toast, { Toaster } from 'react-hot-toast';


const CreateSalesman = ({onHide,onSalesmanCreated = ()=>{} }) => {
    const navigate = useNavigate();
    const [t] = useTranslation('global');
    const [salesmansTypes,setSalesmanTypes] = useState([]); 
    const params = useParams();
    const {currentTenantId,currentCompany,currentCompanyId,currentBranchId,currentLangList,currentLangId} = useCurrent()
      const getSallesmanTypes = async () => {
            const _salesman = await getAllSalesmanTypes(currentTenantId ,currentCompanyId,currentBranchId);
            setSalesmanTypes(_salesman)
        } 
    const [errors,setErrors] = useState({});
    // FORM STATE
    const [fullname,setFullname] = useState(langObjType())
    const [salesmanNo,setSalesmanNo] = useState('')
    const [mobileNo,setMobileNo] = useState('')
    const [email,setEmail] = useState('')
    const [salesmanNotes,setSalesmanNote] = useState('')
    const [targetMonthly,setTargetMonthly] = useState();
    const [targetYearly,setTargetYearly] = useState();
    const [isActive,setIsActive] = useState(true);
    const [salesmanTypeId, setSalesmanTypeId] = useState('');
    useEffect(() => {
        getSallesmanTypes();
        console.log("salesmansTypes",salesmansTypes)
   },[]);
    const saveHandler = async(e) => { 
        e.preventDefault();
        let errorObj = {};
        const data={
            fullname : objLangTextHandler(fullname),
            salesmanNo,
            mobileNo,
            email,
            salesmanNotes,
            targetMonthly,
            targetYearly,
            isActive,
            salesmanTypeId,
            tenantId:currentTenantId,
            companyId:currentCompany,
            branchId:currentBranchId
        }

        // التحقق من الحقول المطلوبة
        if(!fullname['ar'] || fullname['ar'].trim() === ''){
            errorObj['fullname.ar'] = t('validation.required') || 'هذا الحقل مطلوب';
        }
        if(!salesmanTypeId){
            errorObj.salesmanTypeId = t('validation.required') || 'هذا الحقل مطلوب';
        }
        if(isNaN(mobileNo)){
            errorObj.mobileNo = t('validation.mobileNumberInvalid') || 'رقم الجوال غير صحيح';
        }
        if(Object.keys(errorObj)?.length !== 0 ){
            setErrors(errorObj)
            toast.error(t('validation.fillRequiredFields') || 'يرجى ملء الحقول المطلوبة' 
                );
            return
        }

        console.log("datadata", data);
        
        const added = await addSalesman(data)
        if(added){
            console.log('added',added)
            toast.success(t('salesman.addedSuccessfully') || 'تم إضافة المندوب بنجاح' );
            setTimeout(() => {
                onHide ? cancelHandle() : navigate(`/${params.tenant}/salesmans`);
                onSalesmanCreated();
            }, 2000);
        }
    };
    const cancelHandle = () => {
        onHide();
    }
    return (
        <div className={`${onHide && 'p-5'} text-sm flex flex-col gap-5`}>
       <Toaster
  position="top-center"
  containerStyle={{
    top: 60, 
  }}
  toastOptions={{
    duration: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    draggable: true,
    pauseOnHover: true,
    style: {
      direction: "rtl"
    }
  }}
/>
            <CardHeadeWithActions title={t('salesman.addNewSalesman')}>
                        { onHide && <MwButton inGroup={true}  type='cancelBtn' onClick={cancelHandle}>{t('salesman.cancel')}</MwButton> }
                            <Link to={`/${params.tenant}/salesmans`} >
                                <MwButton inGroup={true}  type='cancelBtn' > {t('salesman.manageSalesmen')}  </MwButton> 
                            </Link>
                            <MwButton inGroup={true}  type='saveBtn' actionType={`button`} onClick={saveHandler}>
                            {t('salesman.saveSalesman')}
                            <AiOutlineSave size={18}/>
                            </MwButton>
            </CardHeadeWithActions>

            <SalesmanForm
                currentLangList = {currentLangList}
                currentLangId = {currentLangId}
                errors = {errors}
                salesmansTypes={salesmansTypes}
                setSalesmanTypes={setSalesmanTypes}
                salesmanNo = {salesmanNo}
                setSalesmanNo = {setSalesmanNo}
                fullname = {fullname}
                setFullname = {setFullname}
                mobileNo   = {mobileNo}
                setMobileNo = {setMobileNo}
                email = {email}
                setEmail = {setEmail}
                salesmanNotes = {salesmanNotes}
                setSalesmanNote = {setSalesmanNote}
                targetMonthly = {targetMonthly}
                setTargetMonthly = {setTargetMonthly}
                targetYearly = {targetYearly}
                setTargetYearly = {setTargetYearly}
                isActive = {isActive}
                setIsActive = {setIsActive}
                salesmanTypeId = {salesmanTypeId}
                setSalesmanTypeId = {setSalesmanTypeId}
                />
        </div>
    )
}

export default CreateSalesman