import { useState } from 'react';
import {MwInputText, MwSelector, MwSwitch, MwTextArea, MwSpinner} from '../../components/ui'
import FormLangBar from '../FormLangBar';
import { useTranslation } from 'react-i18next';
import toast, { Toaster } from 'react-hot-toast';


const SalesmanForm = ({
    salesmansTypes,
    currentLangList,
    currentLangId,
    errors,
    salesmanNo,
    setSalesmanNo,
    fullname,
    setFullname,
    mobileNo,
    setMobileNo,
    email,
    setEmail,
    salesmanNotes,
    setSalesmanNote,
    targetMonthly,
    setTargetMonthly,
    targetYearly,
    setTargetYearly,
    isActive,
    setIsActive,
    salesmanTypeId,
    setSalesmanTypeId,
    loading = false
}) => {

    const [formLang, setFormLang] = useState(currentLangId);
    const [formSubmitted, setFormSubmitted] = useState(false);
    const [t] = useTranslation('global');

    if (loading) {
        return (
            <div className="flex justify-center items-center h-full min-h-[200px]">
                <MwSpinner />
            </div>
        );
    }

    return (
        <form onSubmit={(e) => {
            e.preventDefault();
            setFormSubmitted(true);
        }}>
   <Toaster
  position="top-center"
  containerStyle={{
    top: 60, 
  }}
  toastOptions={{
    duration: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    draggable: true,
    pauseOnHover: true,
    style: {
      direction: "rtl"
    }
  }}
/>
        <div className='p-3 bg-slate-50 rounded-lg mb-3 text-sm flex flex-col gap-1'>
            <div className='py-2'>
                <MwInputText 
                    label={t('salesman.salesmanNumber')}
                    id='salesmanNo' 
                    value={salesmanNo} 
                    onChange={(e)=>setSalesmanNo(e.target.value)} />
            </div>
            <div className='py-2'>
            <FormLangBar currentLangList = {currentLangList} formLang = {formLang} setFormLang = {setFormLang} />
            { currentLangList.map((lang) => (
            <div key={lang.langCode} className=''>
                <div className={` ${ formLang === lang?.langCode ? 'block' : 'hidden' } `}>
                    <MwInputText 
                        label={` ${t('salesman.salesmanName')}   (${lang?.langName})`}
                        id={`salesmanName${lang?.langCode}`} 
                        value={fullname[lang?.langCode]}
                        invalid={formSubmitted && (!!errors[`fullname.${lang?.langCode}`] || (lang.langCode === 'ar' && (!fullname[lang.langCode] || fullname[lang.langCode].trim() === '')))}
                        invalidMsg={errors[`fullname.${lang?.langCode}`] || (lang.langCode === 'ar' && (!fullname[lang.langCode] || fullname[lang.langCode].trim() === '') ? t('validation.required') || 'هذا الحقل مطلوب' : '')}
                        onChange={(e)=>{
                            setFullname({...fullname, [lang?.langCode]:e.target.value});
                        }}
                        />
                </div>
            </div>
            )) }
            </div>
                <div className='py-2'>
                    <MwInputText 
                        label={t('salesman.mobileNumber')}
                        id='mobileNo' 
                        value={mobileNo} 
                        invalid={formSubmitted && !!errors.mobileNo}
                        invalidMsg={errors.mobileNo}
                        onChange={(e)=>setMobileNo(e.target.value)} />
                </div>
                <div className='py-2'>
                <MwSelector
                    initalValue={t('salesman.selectTypes')}
                    label={t('salesman.salesmanTypes')}
                    _data={salesmansTypes} 
                    dataType='salesmansTypes'
                    withSearch={false} 
                    selectedItem={salesmansTypes?.find(type => {
                        if (!type || !salesmanTypeId) return false;
                        return type._id === salesmanTypeId;
                    })}
                    setSelectedItem={(value) => setSalesmanTypeId(value?._id)}
                    className="flex-1"
                    invalid={formSubmitted && !salesmanTypeId}
                    invalidMsg={!salesmanTypeId ? t('validation.required') || 'هذا الحقل مطلوب' : ''}
                />
            </div>
                <div className='py-2'>
                    <MwInputText 
                        label={t('salesman.email')}
                        id='email' 
                        value={email} 
                        onChange={(e)=>setEmail(e.target.value)} />
                </div>
                <div className='py-2'>
                    <MwInputText 
                        label={t('salesman.targetMonthly')}
                        id='targetMonthly' 
                        value={targetMonthly} 
                        onChange={(e)=>setTargetMonthly(e.target.value)} />
                </div>
                <div className='py-2'>
                    <MwInputText 
                        label={t('salesman.targetYearly')}
                        id='targetYearly' 
                        value={targetYearly} 
                        onChange={(e)=>setTargetYearly(e.target.value)} />
                </div>
                <div className='py-2'>
                    <MwTextArea 
                        label={t('salesman.notes')}
                        id='salesmanNotes' 
                        value={salesmanNotes} 
                        rows={2}
                        onChange={(e)=>setSalesmanNote(e.target.value)} />
                </div>
                <div className='flex justify-between items-center py-4   mb-2'>
                    <label className='text-xs text-slate-400'>  {t('salesman.activeInactive')}   </label>
                    <MwSwitch custKey='isActive' isActive={isActive} setIsActive={setIsActive} onChangeSwitch={() => setIsActive(!isActive)} />
                </div>
        </div>
    </form>
    )
}

export default SalesmanForm