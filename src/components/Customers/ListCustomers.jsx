import {useState,useEffect} from 'react'
// import { GrView } from 'react-icons/gr';
// import { FiEdit } from 'react-icons/fi';
// import { RiDeleteBinLine } from 'react-icons/ri';
import SearchCustomer from '../SearchCustomer';
import ListCustomersItem from './ListCustomersItem';
import NoDataFound from '../ui/NoDataFound';
import { ConfirmDelete, MwButton, MwSpinner } from '../ui';
import {MdOutlineAddBox} from 'react-icons/md'
import CardHeadeWithActions from '../ui/CardHeadeWithActions';
import { useCurrent, useDelete, useFetch } from '../../hooks';

const ListCustomers = ({setMode,idItemSelected,setIdItemSelected}) => {
    const [showConfirmDelete,setShowConfirmDelete] = useState(false);
    const { currentTenantId ,currentCompanyId,currentBranchId} = useCurrent();
    const [customersList,setCustomersList] = useState(); 
    const {data:deleteResult,deleteItem} = useDelete()
    const {data:dataCustomers,loading,refreshHandler} = useFetch(`${process.env.REACT_APP_Auth_API_BASE_URL}/customers/${currentTenantId}/${currentCompanyId}/${currentBranchId}`);
    const onChangeSearch = (newValue) => {
        if(newValue.length === 0){
            refreshHandler()
        }
        setCustomersList(newValue);
        console.log('=>newValue',newValue);
    }

    const editHandeler = (id) =>{
        setIdItemSelected(id)
        setMode('Edit')
    }
    const deleteHandeler = (id) =>{
        setIdItemSelected(id)
        setShowConfirmDelete(true);
    }

    const confirmDelete = () =>{
        deleteItem(`${process.env.REACT_APP_Auth_API_BASE_URL}/customers/delete/${idItemSelected}`)
    }
    useEffect(() =>{
        if(dataCustomers){
            setCustomersList(dataCustomers.data)
        }
    },[dataCustomers])
    useEffect(()=>{
        if(deleteResult){
            console.log('deleteResult',deleteResult)
            refreshHandler()
            setShowConfirmDelete(false);
        }
    },[deleteResult]);

    return (
        <>
            <ConfirmDelete onDeleteHandler={confirmDelete} onShow={showConfirmDelete} setOnShow={setShowConfirmDelete} msg={`هل انت متاكد من حذف العميل`} />
            <div className=''>
                <CardHeadeWithActions title={`قائمة العملاء`}>
                    <div className='flex-1  flex gap-3 justify-between '>
                        <SearchCustomer className={`bg-white`} isOpen={false} onChangeSearch={onChangeSearch} />
                        <MwButton inGroup={true} type='saveBtn' onClick={()=>setMode('Add')}  >
                            <MdOutlineAddBox size={18}/>
                            أضف عميل جديد
                        </MwButton>
                    </div>
                </CardHeadeWithActions>
                {loading && <MwSpinner/>}
                {
                    customersList && customersList?.length > 0 ? 
                        <div className='pt-3'>{
                            customersList.map((item,index) =>(<ListCustomersItem key={item._id} index={index} item={item} deleteHandeler = {deleteHandeler} editHandeler = {editHandeler} /> ))
                            }</div> :
                        <NoDataFound msg={`لا يوجد عملاء `}/>
                }
            </div>
        </>
    )
}

export default ListCustomers