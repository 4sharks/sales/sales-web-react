import {  useTranslation } from 'react-i18next';

import {MwInputText,MwSelector, MwSelectorMulti} from '../../ui';
import { useCallback, useEffect, useState } from 'react';
import { useFetch } from '../../../hooks';

const InvFormHeader = ({
    invNo,
    setInvNo,
    invRef,
    invTitle,
    setInvTitle,
    invTitleAr,
    setInvTitleAr,
    invDate,
    invDateDue,
    invCurrency,
    customers,
    onClickNewCustomer,
    salesmans,
    onClickNewSalesman,
    customerSelected,
    setCustomerSelected,
    salesmanSelected,
    setSalesmanSelected,
    formErrors,
    initalCustomerName,
    initalSalesmanName,
    initalSalesmanId,
    setPriceListItems,
    productList,SETTING_INVOICE,SETTING_PRICE_INCLUDE_VAT,setProductList
    }) => {
      console.log("customerSelected",customerSelected);
      console.log("productList out",productList);
      
        const [t] = useTranslation('global')
        const [titleLang, setTitleLang] = useState('ar');
        const [arValue, setArValue] = useState(invTitleAr ?? '');
        const [enValue, setEnValue] = useState(invTitle ?? '');
 const getPriceListUrl = useCallback(() => {
  if (!customerSelected) return null;

  const listPriceId = customers.find(customer => customer._id === customerSelected.id)?.listPriceId || 
                      customerSelected.listPriceId;
  
  return listPriceId 
      ? `${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/inventory/list-price-items/${listPriceId}`
      : null;
}, [customerSelected, customers]);

 const {
  data: fetchedData,
  loading,
  error,
  refreshHandler
} = useFetch(getPriceListUrl(), false);

 useEffect(() => {
  if (productList && productList.length > 0) {
       const updatedProductList = productList.map(product => {
           if (product.productId === 'section' || !product.productId || product.productId === '0') {
              return product;
          }
          
           const existingPrice = product.priceUnit || 0;
          
          return {
              ...product,
              // حفظ السعر الأصلي
              originalPriceUnit: product.originalPriceUnit || existingPrice,
              priceUnit: existingPrice,
              price: product.qty * existingPrice * (product.qtyDays || 1),
              productNetTotal: product.productNetTotal || (product.qty * existingPrice * (product.qtyDays || 1)),
              productVat: product.productVat || ((product.qty * existingPrice * (product.qtyDays || 1)) * (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100),
              productNetTotalWithVat: product.productNetTotalWithVat || ((product.qty * existingPrice * (product.qtyDays || 1)) * (1 + (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100))
          };
      });
  
      setProductList(updatedProductList);
  }
}, []);

 useEffect(() => {
  if (!customerSelected) {
      return;
  }

  const url = getPriceListUrl();
  if (url) {
      refreshHandler();
  }
}, [customerSelected, getPriceListUrl]);

       useEffect(() => {
        if (customerSelected && customerSelected.listPriceId) {
            console.log("defaultcustomerSelected", customerSelected);
            refreshHandler();
        } else if (customerSelected) {
            // إذا تم اختيار عميل بدون قائمة أسعار، نعود للأسعار الافتراضية
            console.log("productListosama", productList);
            const defaultPriceList = productList.map(product => {
                if (product.productId === 'section' || !product.productId || product.productId === '0') {
                    return product;
                }

                // استخدام originalPriceUnit كسعر افتراضي إذا كان موجودًا، وإلا نستخدم السعر الأساسي للمنتج
                const defaultPrice = product.originalPriceUnit || parseFloat(product.priceUnit) || 0;

                return {
                    ...product,
                    priceUnit: defaultPrice,
                    price: product.qty * defaultPrice * (product.qtyDays || 1),
                    productNetTotal: product.qty * defaultPrice * (product.qtyDays || 1),
                    productVat: (product.qty * defaultPrice * (product.qtyDays || 1)) * (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100,
                    productNetTotalWithVat: (product.qty * defaultPrice * (product.qtyDays || 1)) * (1 + (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100)
                };
            });
            console.log("defaultPriceList", defaultPriceList);

            setPriceListItems([]);
            setProductList(defaultPriceList);
        }
      }, [customerSelected]);
      useEffect(() => {
        if (fetchedData && fetchedData.data) {
            setPriceListItems(fetchedData.data);
            
            if (productList && productList.length > 0 && customerSelected) {
                const updatedProductList = productList.map(product => {
                    if (product.productId === 'section' || !product.productId || product.productId === '0') {
                        return product;
                    }
                    
                    // البحث عن سعر المنتج في قائمة الأسعار
                    const priceListItem = fetchedData.data.find(
                        item => item.product_id.trim() === product.productId
                    );
                    
                    if (priceListItem) {
                        // إذا وجد سعر في قائمة الأسعار، نستخدمه
                        const newPrice = SETTING_PRICE_INCLUDE_VAT === 'true' 
                            ? parseFloat(priceListItem.product_sale_price) 
                            : parseFloat(priceListItem.product_sale_price);
                        
                        // حساب كل القيم المطلوبة بناءً على السعر الجديد
                        const totalPrice = product.qty * newPrice * (product.qtyDays || 1);
                        let netTotal = totalPrice;
                        
                        // حساب الخصم إذا وجد
                        if (product.productDiscount) {
                            if (product.productDiscount.includes('%')) {
                                netTotal = totalPrice - (totalPrice * parseFloat(product.productDiscount.replace('%', '')) / 100);
                            } else {
                                netTotal = totalPrice - parseFloat(product.productDiscount || 0);
                            }
                        }
                        
                        const vatPercent = SETTING_INVOICE?.INV_VAT_PERCENT || 15;
                        const productVat = netTotal * vatPercent / 100;
                        
                        return {
                            ...product,
                            priceUnit: newPrice,
                            price: totalPrice,
                            productNetTotal: netTotal,
                            productVat: productVat,
                            productNetTotalWithVat: netTotal + productVat
                        };
                    } else {
                        // إذا لم يوجد سعر في قائمة الأسعار، نحتفظ بالسعر الحالي
                        return product;
                    }
                });
                
                setProductList(updatedProductList);
            }
        }
      }, [fetchedData, SETTING_PRICE_INCLUDE_VAT, SETTING_INVOICE]);
              
      
      
      useEffect(() => {
                  setArValue(invTitleAr ?? '');
                  setEnValue(invTitle ?? '');
              }, [invTitleAr, invTitle]);
              
              const handleArChange = (e) => {
                const newValue = e.target.value;
                setArValue(newValue);
                setInvTitleAr(newValue);
            };
      
      const handleEnChange = (e) => {
          const newValue = e.target.value;
          setEnValue(newValue);
          setInvTitle(newValue);
      };
        const date = new Date();
         
        // add use effect to handle the change of the customerSelected and salesman
        useEffect(() => {
          console.log("initalSalesmanName:", initalSalesmanName);
          console.log("initalSalesmanId:", initalSalesmanId);
          console.log("salesmans:", salesmans);
          console.log("customerSelected:", customerSelected);
          
          // حالة 1: إذا كان لدينا أسماء البائعين بصيغة JSON ولم يتم تحديد أي بائع بعد
          if (initalSalesmanName && (!salesmanSelected || salesmanSelected.length === 0)) {
            try {
              const formattedSalesmen = formatSalesmanName(initalSalesmanName, initalSalesmanId);
              if (formattedSalesmen.length > 0) {
                console.log("Found salesmen using formatSalesmanName:", formattedSalesmen);
                setSalesmanSelected(formattedSalesmen);
                return;
              }
            } catch (e) {
              console.error('Error formatting salesman data:', e);
            }
          }
          
           if (salesmans && salesmans.length > 0) {
            const selectedSalesmen = [];
            
             if (Array.isArray(initalSalesmanId) && initalSalesmanId.length > 0) {
              for (const salesmanId of initalSalesmanId) {
                const matchingSalesman = salesmans.find(salesman => salesman._id === salesmanId);
                if (matchingSalesman) {
                  selectedSalesmen.push({
                    label: matchingSalesman?.fullname && matchingSalesman.fullname[1] ? matchingSalesman.fullname[1].text : '',
                    value: matchingSalesman._id,
                    fullname: matchingSalesman.fullname || [
                      { lang: 'ar', text: '' },
                      { lang: 'en', text: '' }
                    ],
                    ...matchingSalesman
                  });
                }
              }
              
              if (selectedSalesmen.length > 0) {
                console.log("Found matching salesmen by initalSalesmanId array:", selectedSalesmen);
                setSalesmanSelected(selectedSalesmen);
                return;
              }
            }
            
            // البحث عن بائع واحد بناءً على معرف نصي
            if (typeof initalSalesmanId === 'string' && initalSalesmanId) {
              const matchingSalesman = salesmans.find(salesman => salesman._id === initalSalesmanId);
              if (matchingSalesman) {
                selectedSalesmen.push({
                  label: matchingSalesman?.fullname && matchingSalesman.fullname[1] ? matchingSalesman.fullname[1].text : '',
                  value: matchingSalesman._id,
                  fullname: matchingSalesman.fullname || [
                    { lang: 'ar', text: '' },
                    { lang: 'en', text: '' }
                  ],
                  ...matchingSalesman
                });
                
                console.log("Found matching salesman by initalSalesmanId string:", selectedSalesmen);
                setSalesmanSelected(selectedSalesmen);
                return;
              }
            }
            
            // حالة 3: البحث عن البائع بناءً على العميل المحدد
            if (customerSelected && customers) {
              const customerId = customerSelected.id || (customerSelected._id ? customerSelected._id : null);
              if (customerId) {
                const fullCustomer = customers.find(c => c._id === customerId);
                if (fullCustomer && fullCustomer.salesmanId) {
                  const matchingSalesman = salesmans.find(salesman => salesman._id === fullCustomer.salesmanId);
                  if (matchingSalesman) {
                    selectedSalesmen.push({
                      label: matchingSalesman?.fullname && matchingSalesman.fullname[1] ? matchingSalesman.fullname[1].text : '',
                      value: matchingSalesman._id,
                      fullname: matchingSalesman.fullname || [
                        { lang: 'ar', text: '' },
                        { lang: 'en', text: '' }
                      ],
                      ...matchingSalesman
                    });
                    
                    console.log("Found matching salesman by customerSelected lookup:", selectedSalesmen);
                    setSalesmanSelected(selectedSalesmen);
                    return;
                  }
                }
              }
            }
            
            // لم نجد أي بائع متطابق
            console.log("No matching salesmen found");
            setSalesmanSelected([]);
          }
        }, [initalSalesmanName, initalSalesmanId, salesmans, customerSelected, customers]);
        
        // دالة تنسيق أسماء البائعين من البيانات المستلمة بصيغة JSON
        const formatSalesmanName = (salesmanData, salesmanId) => {
          if (typeof salesmanData === 'string') {
            try {
              const parsed = JSON.parse(salesmanData);
              const salesmanIds = typeof salesmanId === 'string' ? 
                JSON.parse(salesmanId).map(id => id.replace(/[\[\]"\\\s]/g, '')) : 
                (Array.isArray(salesmanId) ? salesmanId : [salesmanId]);
        
              // نتأكد أن لدينا نفس عدد الأسماء والـ IDs
              return Array.isArray(parsed) ? parsed.map((item, index) => ({
                value: salesmanIds[index] || '', // كل اسم يأخذ الـ ID المقابل له في نفس الترتيب
                label: item.ar || item.en,
                fullname: [
                  { lang: 'ar', text: item.ar || '' },
                  { lang: 'en', text: item.en || '' }
                ]
              })) : [];
            } catch (e) {
              console.error('Error formatting salesman data:', e);
              return [];
            }
          }
          return [];
        };  

        const cleanTextValue = (text) => {
          if (!text) return '';
           return text.replace(/\s+/g, ' ').trim();
        };
        console.log('customerSelected out useEffect', customerSelected);
    
       
    return (
            <div className="flex flex-col md:flex-row justify-between my-4 items-center ">
                <div className="flex w-full md:w-fit " >
                    <div className="flex-1 md:w-24   ">
                        <MwInputText 
                            label={t('DashBoard.quoteNumber')}
                            id='inv_no' 
                            value={invNo} 
                            disabled={true} 
                            onChange={(e)=>setInvNo(e.target.value)} />
                    </div>
                    <div className="flex-1 md:w-20">
                        <MwInputText 
                            label={t('invoice.invoiceRef')}
                            inputRef={invRef}
                            id='inv_ref' 
                            defaultValue={invRef.current?.value || ''}
                            />
                    </div>
                </div>
                    <div className="flex w-full md:w-fit " >
                    <div className="flex-1 md:w-38 ">
                        <MwInputText 
                            label={t('invoice.invoiceDate')}
                            // inputType='date'
                            inputRef={invDate} 
                            id='inv_date'
                            defaultValue={invDate.current}
                            invalid = {!!formErrors?.invDate}
                            disabled = {true}
                            // invalidMsg={formErrors?.invDate}
                            />
                    </div>
                    <div className="flex-1 md:w-20">
                        <MwInputText 
                            label={t('invoice.invoiceDateDue')}
                            inputType='date'
                            inputRef={invDateDue} 
                            id='inv_date_due'
                            defaultValue={invDateDue.current}
                            invalid = {!!formErrors?.invDateDue}
                            />
                    </div>
                </div>
                <div className="relative flex-1  flex w-full ">
                <div className="flex-1 mx-1">
    <div className="flex w- justify-between items-center mb-1">
      <span className="text-gray-400 text-xs">
        {titleLang === 'ar' ? t('invoice.invTitleAr') : t('invoice.invTitle')}
      </span>
      <button 
        onClick={() => setTitleLang(titleLang === 'ar' ? 'en' : 'ar')}
        type="button"
        className="px-1 py-0.5 text-xs bg-blue-500 text-white rounded hover:bg-blue-600"
      >
        {titleLang === 'ar' ? 'EN' : 'AR'}
      </button>
    </div>
    <div className={titleLang === 'ar' ? '' : 'hidden'}>
      <MwInputText 
        // inputRef={invTitleAr}
        id="inv_title_ar"
        value={cleanTextValue(arValue)}
        onChange={(e) => {
          const cleanValue = cleanTextValue(e.target.value);
          handleArChange({ target: { value: cleanValue } }); 
        }}
        dir="rtl"
         style={{ textAlign: 'right' }}  
  className="text-right"
      />
    </div>
    <div className={titleLang === 'ar' ? 'hidden' : ''}>
      <MwInputText 
        // inputRef={invTitle}
        id="inv_title"
        value={cleanTextValue(enValue)}
        onChange={(e) => {
          const cleanValue = cleanTextValue(e.target.value);
          handleEnChange({ target: { value: cleanValue } });
        }}
        dir="ltr"
        style={{ textAlign: 'left' }}
        className="text-left"
      />
    </div>
  </div>
                    <div className="  flex-1 flex  flex-col ">
                        <MwSelector 
                            label='العميل'
                            initalValue={initalCustomerName}
                            _data={customers} 
                            dataType='customer' 
                            onClickNew = {onClickNewCustomer}
                            selectedItem={customerSelected}
                            setSelectedItem = {setCustomerSelected}
                            defaultValue={initalCustomerName}
                            />
                    </div>
                    <div className="flex-1 flex flex-col ">
                    <MwSelectorMulti
      label='المندوب'
      defaultValue={salesmanSelected}  
      _data={salesmans}
      dataType='salesman'
      onClickNew={onClickNewSalesman}
      selectedItem={salesmanSelected}
      setSelectedItem={setSalesmanSelected}
      isMulti={true}  
    />
                    </div>
                </div>
              
            </div>
        
    )
}

export default InvFormHeader