import React from 'react'
import {  useTranslation } from 'react-i18next';

const InvoiceBody = ({
    customerName,
    customer,
    salesmanName,
    invProducts,
    isDelivery,
    SETTING_INV_CURRENCY,
    formatter,
    SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION,
    SETTING_INV_QTY_DAYS,
    SETTING_INV_SHOW_ITEM_T_A_VAT,
    SETTING_INV_UOM_SHOW
    
}) => {
   
 
    

// Validate formatter existence
const formatCurrency = (value) => {
    if (!formatter || typeof formatter.format !== 'function') {
        return value || 0; // Return raw value if formatter is invalid
    }
    
    // Ensure value is a valid number
    const numValue = parseFloat(value);
    if (isNaN(numValue)) {
        return 0;
    }
    
    try {
        return formatter.format(numValue);
    } catch (error) {
        console.error('Error formatting value:', error);
        return numValue.toString();
    }
};

// Safely calculate discount percentage
const calculateDiscountPercentage = (discount, totalPrice) => {
    if (!discount || discount <= 0 || !totalPrice || totalPrice === 0) {
        return null;
    }
    
    try {
        const percentage = Math.round((discount / totalPrice) * 100);
        return isFinite(percentage) ? "%" + percentage : null;
    } catch (error) {
        console.error('Error calculating discount percentage:', error);
        return null;
    }
};

// Ensure invProducts is an array
const safeInvProducts = Array.isArray(invProducts) ? invProducts : [];

const invProductsList = safeInvProducts.map((prod) => {
        // Skip if prod is not an object
        if (!prod || typeof prod !== 'object') {
            return null;
        }
        
        // Generate a unique key safely
        const itemKey = prod.id || `item-${Math.random().toString(36).substr(2, 9)}`;
        
        if (prod.product_id === "section") {
            return (
                <div key={itemKey} className={`flex justify-between border-b`}>
                    <div className='flex-1 p-2 border-e font-bold bg-slate-100'>{prod?.product_name || 'Unnamed Section'}</div>
                </div>
            );
        }
        
        return (
            <div key={itemKey} className={`flex justify-between border-b border-s border-e ${prod.is_returned ? 'bg-orange-50' : ''}`}>
                <div className='flex-1 p-2 border-e'>
                    {prod.product_name || 'Unnamed Product'} 
                    {prod.is_returned && <span className='font-bold text-orange-300'>[مرتجع]</span>}
                    {
                        SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION === 'true' && prod?.product_desc && 
                        <div className='text-xs text-slate-400 px-1'>
                            {prod.product_desc}
                        </div>
                    }
                </div>
                
                <div className='w-12 flex items-center justify-center text-center border-e p-2'>
                    {typeof prod?.qty === 'number' ? prod.qty : 0}
                </div>
                
                {SETTING_INV_UOM_SHOW === 'true' && 
                    <div className='w-12 flex items-center justify-center text-center border-e p-2'>
                        {prod?.uom_relation?.short_name || ''}
                    </div>
                }
                
                {SETTING_INV_QTY_DAYS === 'true' && 
                    <div className='w-12 flex items-center justify-center text-center border-e p-2'>
                        {typeof prod.qty_days === 'number' ? prod.qty_days : 0}
                    </div>
                }
                
                {!isDelivery && 
                    <div className='w-20 flex items-center justify-center text-center border-e p-2'>
                        {formatCurrency(prod.price)}
                    </div>
                }
                
                {!isDelivery && 
                    <div className='w-24 flex items-center justify-center text-center border-e p-2'>
                        {formatCurrency(prod.total_price)}
                    </div>
                }
                
                {!isDelivery && 
                    <div className='w-14 flex items-center justify-center text-center border-e p-2'>
                        {calculateDiscountPercentage(prod.product_discount, prod.total_price)}
                    </div>
                }
                
                {!isDelivery && 
                    <div className='w-24 flex items-center justify-center border-e text-center p-2'>
                        {formatCurrency(prod.product_net_total)}
                    </div>
                }
                
                {!isDelivery && SETTING_INV_SHOW_ITEM_T_A_VAT === "true" && 
                    <div className='w-20 flex items-center justify-center text-center border-e p-2'>
                        {formatCurrency(prod.product_vat)}
                    </div>
                }
                
                {!isDelivery && SETTING_INV_SHOW_ITEM_T_A_VAT === "true" && 
                    <div className='w-24 flex items-center justify-center text-center border-e p-2'>
                        {formatCurrency(prod.product_net_total_with_vat)}
                    </div>
                }
            </div>
        );
    }).filter(Boolean);

 
    return (
        <div className='static'>
            <div className='text-xs py-3' >
 
 {(customer?.customerVatNo || customerName || customer?.customerAddresses[0]?.desc) && (
  <div className='flex flex-wrap justify-between text-xs border rounded mt-3 mb-2'>
    {customerName && (
      <div className='border-e flex-1 p-2 min-w-[200px]'>
        <div className='flex flex-col text-center'>
          <span className='text-gray-500'> اسم العميل </span>
          <span className='text-gray-500'> CUSTOMER NAME</span>
          <span className='font-bold mt-1'>{customerName}</span>
        </div>
      </div>
    )}
    
    {customer?.customerVatNo && (
      <div className='border-e flex-1 p-2 min-w-[200px]'>
        <div className='flex flex-col text-center'>
          <span className='text-gray-500'> الرقم الضريبي للعميل</span>
          <span className='text-gray-500'> CUSTOMER VAT NO.</span>
          <span className='font-bold mt-1'>{customer.customerVatNo}</span>
        </div>
      </div>
    )}
    
    {customer?.customerAddresses && customer.customerAddresses[0]?.desc && (
      <div className='flex-1 p-2 min-w-[200px]'>
        <div className='flex flex-col text-center'>
          <span className='text-gray-500'> عنوان العميل</span>
          <span className='text-gray-500'>CUSTOMER ADDRESS </span>
          <span className='font-bold mt-1'>{customer.customerAddresses[0].desc}</span>
        </div>
      </div>
    )}
  </div>
)}
           
                {/* <div className='flex justify-between text-xs px-1 pb-3 pt-1'>
                    {customerName && <div>{t('invoice.Customer')}: {customerName}</div>}
                    {salesmanName && <div>{t('invoice.Salesman')}: {salesmanName}</div>}
                </div> */}
                <div  className='flex mt-5  justify-between items-center  bg-slate-100 text-xs font-bold border rounded-t'>
                    <div className='flex-1 text-center border-e flex flex-col py-2'>
                        <span>اسم المنتج - الخدمة</span>
                        <span>PRODUCT / SERVICE NAME</span>
                    </div>
                    <div className='w-12 text-center border-e flex flex-col py-2'>
                        <span className=' text-center'>الكمية</span>
                        <span>QTY</span>
                    </div>
                    {
                    SETTING_INV_UOM_SHOW === 'true' && 
                        <div className='w-12 text-center border-e flex flex-col  py-2'>
                            <span>الوحدة</span>
                            <span>UNIT</span>
                        </div>
                    }
                    {
                    SETTING_INV_QTY_DAYS === 'true' && 
                        <div className='w-12 text-center border-e flex flex-col  py-2'>
                            <span> الايام </span>
                            <span>DAYS</span>
                        </div>
                    }
                    {
                    !isDelivery && 
                        <div className='w-20 text-center border-e flex flex-col py-2 '>
                            <span>سعر</span>
                            <span>RATE </span>
                        </div>
                    }
                    {!isDelivery && <div className='w-24 text-center border-e flex flex-col py-2'>
                        <span>مجموع</span>
                        <span>TOTAL</span>
                        </div>}
                    {!isDelivery && <div className='w-14 text-center  text-xs border-e flex flex-col py-2'>
                        <span className='text-xs'>الخصم</span>
                        <span className='text-xs'>DISCOUNT</span>
                        </div>}
                    {!isDelivery && <div className='w-24 text-center border-e text-xs flex flex-col py-2'>
                        <span className='text-xs'>     إج. قبل الضريبة</span>
                        <span className='text-xs'>Excl. VAT</span>
                        </div>}
                    {/* {!isDelivery && <div className='w-24 text-center  flex flex-col'>
                        <span>الضريبة ({SETTING_INV_VAT_PERCENT}%)</span>
                        <span>VAT ({SETTING_INV_VAT_PERCENT}%)</span>
                        </div>} */}
                    {!isDelivery && SETTING_INV_SHOW_ITEM_T_A_VAT === "true" && <div className='w-20 text-center border-e flex flex-col py-2'>
                        <span>  الضريبة</span>
                        <span>VAT</span>
                        </div>}
                    {!isDelivery && SETTING_INV_SHOW_ITEM_T_A_VAT === "true" && <div className='w-24 text-center border-e  flex flex-col py-2'>
                        <span>إج. بعد الضريبة</span>
                        <span>T.A VAT</span>
                        </div>}
                </div>
                <div className=' '>{invProductsList}</div>
            </div>
            
        </div>
    )
}

export default InvoiceBody