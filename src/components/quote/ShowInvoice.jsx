import React, { useEffect, useState, useRef } from 'react'
import { useNavigate,useParams } from 'react-router-dom'
import { useSelector } from 'react-redux';
import axios from 'axios';
import { AiOutlinePrinter,AiOutlineEdit } from 'react-icons/ai';
import toast, { Toaster } from 'react-hot-toast';
import { MdOutlinePaid } from 'react-icons/md';
import { FaFilePdf, FaPhoneSquareAlt, FaPrint } from "react-icons/fa";
import { FiMapPin } from "react-icons/fi";
import { MdOutlineMailOutline } from "react-icons/md";
import { SiSitecore } from "react-icons/si";

import {getCompanyById} from '../../utils/tenantManager.js'
import {arrayNameByLang,printableDiv} from '../../utils/global.js'
import { useFetch,useLogs,useInvoice, useCurrent, useDelete } from '../../hooks';
import { ConfirmDelete,MwButton, MwSpinner } from '../ui';
import InvoiceHeader from './show/InvoiceHeader.jsx';
import InvoiceBody from './show/InvoiceBody.jsx';
import InvoiceFooter from './show/InvoiceFooter.jsx';
import {  useTranslation } from 'react-i18next';
import '../../handle-head-list.css';
import DeleteConfirmationModal from '../OsNewWeight/OsDeleteConfirmationModal .jsx';

const ShowInvoice = ({isPrint=false,isDelivery=false}) => {
    const divPrintRef = useRef();
    const [print,setPrint] = useState(false);
    const [t] = useTranslation('global')
    const params = useParams();
    const navigate = useNavigate();
    const {formatter} = useInvoice();
    const [deleteModalOpen, setDeleteModalOpen] = useState(false);
    const [isLoading, setIsLoading] = useState(true);


 
    const {currentTenantId,currentCompanyId,currentBranchId,currentLangId} = useCurrent();
    const lang = useSelector(state => state.lang);
    const invoiceSettings = useSelector(state => state.settings).quoteSettings;
    const company = getCompanyById(params.companyId);
    const companyName =  arrayNameByLang(company?.name,lang.value)?.text || arrayNameByLang(company?.name,'ar')?.text;
    const companyDesc =  arrayNameByLang(company?.desc,lang.value)?.text;
    const companyAddress =  arrayNameByLang(company?.mainAddress,lang.value)?.text;
    const companyPhone =  company?.mainPhone;
    const companyEmail =  company?.mainEmail;
    const companyWebsite =  company?.websiteUrl;
    const vatNo = company.vatNumber;
    const commercialRegisteration = company?.commercialRegisteration;
    const SETTINGTerms = invoiceSettings.INV_TERMS_PRINT
    const SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION = invoiceSettings?.SHOW_INVOICE_PRODUCT_DESCRIPTION || 'false';
    const SETTING_INV_WITH_HEADER_FOOTER = invoiceSettings?.INV_WITH_HEADER_FOOTER || 'false';
    const SETTING_INV_QTY_DAYS = invoiceSettings?.INV_QTY_DAYS || 'false';
    const SETTING_SHOW_TERMS_ON_PRINT = invoiceSettings.SHOW_TERMS_ON_PRINT
    const SETTING_INV_SHOW_ITEM_T_A_VAT = invoiceSettings.INV_SHOW_ITEM_T_A_VAT
    const SETTING_INV_FONT_COLOR = invoiceSettings.INV_FONT_COLOR
    const SETTING_INV_UOM_SHOW = invoiceSettings.INV_UOM_SHOW
    const INV_CURRENCY = invoiceSettings?.INV_CURRENCY
    const SETTING_INV_CURRENCY = INV_CURRENCY  || 'SAR'
    const {resultLogsPost,loadingLogsPost,errorLogsPost,postDataHandler} = useLogs()

    const retryFetch = async (url, maxRetries = 3, delay = 1000) => {
        for (let i = 0; i < maxRetries; i++) {
          try {
            const response = await axios.get(url);
            return response.data;
          } catch (error) {
            if (error.response?.status === 429 && i < maxRetries - 1) {
              await new Promise(resolve => setTimeout(resolve, delay * (i + 1)));
              continue;
            }
            throw error;
          }
        }
      };

    const {data:invData,loading:invLoading,error} = useFetch(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/quotes/show/${params.id}`,true, 
        { 
          fetchFunction: retryFetch,
          retryConfig: {
            maxRetries: 3,
            delay: 1000
          }
        })
    const {data:customerData,fetchData:fetchCustomer,} = useFetch(`${process.env.REACT_APP_Auth_API_BASE_URL}/customers/show/${currentTenantId}/${currentCompanyId}/${currentBranchId}/${invData?.customer_id}`,false);
    console.log("params.id" , params.id);

    const { deleteItem,data: deleteResponse,loading: isDeleting } = useDelete();

    

    useEffect(() => {
        if(invData){
            invData?.customer_id && fetchCustomer();
        }
    },[invData])

    useEffect(() => {
         setIsLoading(true);
        
         if (!invLoading && invData) {
            const timer = setTimeout(() => {
                setIsLoading(false);
            }, 350);
            
             return () => clearTimeout(timer);
        }
    }, [invLoading, invData]);

    

 
    const setLogsHandler =  (itemEn='delete',itemAr='حذف') => {
        const _data = {
            moduleName: "SALES",
            resourceName: "QUOTES",
            eventName:"DELETE",
            logContentEn: `${itemEn} quote No: ${invData.inv_no}`,
            logContentAr: `${itemAr} عرض سعر برقم : ${invData.inv_no}`,
        }
        postDataHandler(_data);
    }

  

    const deleteHandeler = () => {
        const url = `${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/quotes/delete/${params.id}`;
        toast.loading('جاري الحذف...', { id: 'deleteToast' });
        
        deleteItem(url).then(() => {
            // Show success toast
            toast.success('تم حذف عرض السعر بنجاح', { id: 'deleteToast' });
            
            // Set a timeout to navigate after 2 seconds
            setTimeout(() => {
                setLogsHandler();
                navigate(`/${params.tenant}/quotes`);
            }, 2000);
        }).catch((error) => {
            // Show error toast if deletion fails
            toast.error('حدث خطأ أثناء الحذف', { id: 'deleteToast' });
            console.log(`Error deleting: ${error}`);
        });
    };

      const handleDeleteClick = () => {
         setDeleteModalOpen(true);
    };


    const handlePrint = () => {
        setLogsHandler('print','طباعة');
        setPrint(true)
        printableDiv(divPrintRef,lang?.value)
    };
    // if(!customerData?.data){
    //     return <MwSpinner/>
    // }
    
    return (
        <>

<Toaster
  position="top-center"
  containerStyle={{
    top: 60, 
  }}
  toastOptions={{
    duration: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    draggable: true,
    pauseOnHover: true,
    style: {
      direction: "rtl"
    }
  }}
/>
            { !isLoading   ? 
            <div>
                    <DeleteConfirmationModal
    isOpen={deleteModalOpen}
    onClose={() => setDeleteModalOpen(false)}
    onConfirm={() => {
        deleteHandeler();
        setDeleteModalOpen(false);
    }}
    loading={isDeleting}
/>
                <div className='flex w-full py-2 relative   z-50 items-center  justify-between px-2 rounded-lg '>

              
                    {/* <MwButton 
                        size = 'sm'
                        inGroup = {true}
                        type = '    ' 
                        onClick={handlePrint}>
                            <AiOutlinePrinter size={16}/> طباعة
                    </MwButton> */}
                    <div className="flex">
                    <MwButton 
                            size = 'sm'
                            inGroup = {true}
                            type = 'secondary' 
                            onClick={()=>{
                                navigate(`/${params.tenant}/quotes/edit/${params.id}`)
                            }}>
                                <AiOutlineEdit size={16}/> {t("quotes.edit")} 
                    </MwButton>
                    <MwButton 
                            size = 'sm'
                            inGroup = {true}
                            type = 'deleteBtn-inline' 
                            onClick={  handleDeleteClick }>
                                <AiOutlinePrinter size={16}/> {t("quotes.delete")} 
                    </MwButton>
                    <MwButton 
                            size = 'sm'
                            inGroup = {true}
                            type = 'saveBtn' 
                            onClick={ () =>{
                                navigate(`/${params.tenant}/invoices/create/${params.id}`)
                            } }>
                                <MdOutlinePaid size={16}/>{t("quotes.createInvoiceFromQuote")}
                    </MwButton>
                    </div>

                    <div className="mx-15 text-slate-500 flex ">
                    <ul className="flex gap-1">
  {/* <li
    className="cursor-pointer px-2 py-1 flex items-center gap-1 bg-slate-100 hover:font-bold"
    // onClick={html2pdf}  
  >
    <FaFilePdf />
    <span className="text-sm text-slate-500">Save_as_PDF</span>
  </li> */}
  <li
    className="cursor-pointer px-2 py-1 flex items-center gap-1 bg-slate-100 hover:font-bold"
    onClick={handlePrint}
  >
    <FaPrint />
    <span className="text-sm text-slate-500">{t("common.print")}</span>
  </li>
</ul>

    </div>
                </div>
                <div className='bg-slate-50 p-3 mt-2 rounded-lg'>
                    { 
                    // customerData?.data && 
                    <div id='printableContent'  ref={divPrintRef} className='p-5 mt-2   text-sm '>

<div className='header w-full '>
                            <div dir='rtl' className=' text-center pb-2 font-bold text-lg'> <span style={{color:SETTING_INV_FONT_COLOR}}> عرض سعر  - Quotation Proposal</span> </div>
                            <div className=' pb-5  flex justify-between    gap-16'>
                                <div className=' max-w-64 content-center   gap-1 '>
                                    {invoiceSettings?.SHOW_LOGO_ON_PRINT === 'true' && <div><img src={company.logo} className='w-full' /></div> }
                                    
                                </div>
                                <div className='w-96 mx-7  content-center  ' dir='rtl' >
                                        <div className='border p-2 font-bold py-2 text-sm text-center' >{companyName}</div>
                                        <div className='flex flex-col  text-xs text-gray-500'>
                                            <div className='border p-2 flex items-center justify-between gap-3'>
                                                <span>   سجل تجاري:</span>
                                                <span className='font-bold'>{commercialRegisteration}</span>
                                                <span>:.C.R </span>
                                            </div>
                                            <div className='border p-2 flex items-center justify-between gap-3'>
                                                <span>  الرقم الضريبي:</span>
                                                <span className='font-bold'>{vatNo}</span>
                                                <span>:VAT NO</span>
                                            </div>
                                            { companyAddress && <div className='border p-2 flex items-center justify-between gap-3'>
                                                <span>   العنوان:</span>
                                                <span className='font-bold  text-center'>{companyAddress}</span>
                                                <span>:Address</span>
                                            </div>}
                                        </div>
                                </div>
                            </div>
                            <hr className='py-2' style={{  borderColor:SETTING_INV_FONT_COLOR || '#000000'}} />
                        </div>
                        <table className='w-full  '>
                        <thead><tr><td>
                            <div class="header-space">&nbsp;</div>
                        </td></tr></thead>
                        <tbody><tr><td>
                            <div className="  ">
                            <InvoiceHeader
                                logo = {company.logo} 
                                companyName={companyName}
                                companyPhone = {companyPhone}
                                companyEmail = {companyEmail}
                                companyDesc={companyDesc}
                                vatNo = {vatNo}
                                commercialRegisteration = {commercialRegisteration}
                                inv_no = {invData.inv_no}
                                inv_title = {invData.title}
                                inv_title_ar = {invData.title_ar}
                                inv_date = {invData.inv_date}
                                inv_date_due = {invData.inv_date_due}
                                companyAddress = {companyAddress}
                                invoiceSettings = {invoiceSettings}
                            />
                            <InvoiceBody
                            customerName = {invData.customer_name}
                            customer = {customerData?.data}
                            salesmanName = {invData.salesman_name}
                            invProducts = {invData.quote_details}
                            isDelivery = {isDelivery}
                            formatter = {formatter}
                            SETTING_INV_CURRENCY = {SETTING_INV_CURRENCY}
                            SETTING_INV_QTY_DAYS = {SETTING_INV_QTY_DAYS}
                            SETTING_INV_UOM_SHOW = {SETTING_INV_UOM_SHOW}
                            SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION = {SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION}
                            SETTING_INV_SHOW_ITEM_T_A_VAT = {SETTING_INV_SHOW_ITEM_T_A_VAT}
                            />
                            <InvoiceFooter
                                totalAmount = {invData.total_amount}
                                vatAmount = {invData.vat_amount}
                                discountAmount = {invData.discount_amount}
                                netAmount = {invData.net_amount}
                                SETTINGTerms = {SETTINGTerms}
                                SETTING_PRICE_INCLUDE_VAT = {invoiceSettings.SETTING_PRICE_INCLUDE_VAT}
                                SETTING_INV_VAT_PERCENT = {invoiceSettings.INV_VAT_PERCENT}
                                SETTING_SHOW_TERMS_ON_PRINT = {SETTING_SHOW_TERMS_ON_PRINT}
                                SETTING_INV_CURRENCY = {SETTING_INV_CURRENCY}
                                isDelivery={isDelivery}
                                currentLang = {lang}
                                formatter = {formatter}
                                invNotes = {invData.notes}
                                invTerms = {invData.terms}
                            />
                        </div>
                        </td></tr></tbody>
                        <tfoot><tr><td>
                            <div className="footer-space">&nbsp;</div>
                        </td></tr></tfoot>
                        </table>
                        
                        <div className={`footer w-full   pt-3  text-xs`} >
                        <hr className='py-2'  style={{  borderColor:SETTING_INV_FONT_COLOR || '#000000'}} />
                        <div className='w-full   px-5 pt-2 flex gap-5 justify-between'>
                        {companyAddress && (<div className='text-center  flex gap-1  items-center' ><span style={{color:SETTING_INV_FONT_COLOR}}><FiMapPin  /></span> {companyAddress}</div>  ) }
                        {companyPhone && (  <div className='flex gap-1 items-center'><span style={{color:SETTING_INV_FONT_COLOR}}> <FaPhoneSquareAlt /> </span> <span className='pt-1'>{companyPhone}</span>  </div>
                             )}
                             <div>
                             {companyWebsite && (  <div className='flex gap-1'><span style={{color:SETTING_INV_FONT_COLOR}}> <SiSitecore /> </span> {companyWebsite}</div> )}
                             {companyEmail && (   <div className='flex gap-1'><span style={{color:SETTING_INV_FONT_COLOR}}> <MdOutlineMailOutline /> </span>{companyEmail}</div> )}
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    }
                </div>
            </div> : 
           <MwSpinner/> 
            }
        </>
    )
}

export default ShowInvoice