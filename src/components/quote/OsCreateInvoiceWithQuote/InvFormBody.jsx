import React,{useState,useEffect,useRef} from 'react'
import EditorJS from '@editorjs/editorjs';
import Header from '@editorjs/header'; 
import List from '@editorjs/list';

import InvFormListItem from '../../invoice/InvFormListItem'
import { MwButton } from '../../ui'
import  MwTabView from '../../ui/tab/MwTabView'
import  MwTabViewHead from '../../ui/tab/MwTabViewHead'
import  MwTabViewBody  from '../../ui/tab/MwTabViewBody'
// import {updateSettings} from '../../../store/settingSlice'
// import { useDispatch } from 'react-redux';


const InvFormBody = ({
    SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION,
    SETTING_INV_QTY_DAYS,
    SETTING_INV_UOM_SHOW,
    initalProductItem,
    reloadProductList,
    showModalProductHandle,
    productList,
    setProductList,
    formErrors,
    invNotes,
    invTerms,
    setInvNotes,
    setInvTerms,
    priceListItems 
}) => {
    const [tabActive,setTabActive] = useState("items");
    const [isInitialized, setIsInitialized] = useState(false);
    const [notesEditorReady, setNotesEditorReady] = useState(false);
    const [termsEditorReady, setTermsEditorReady] = useState(false);

    const ejInstanceNotes = useRef();
    const ejInstanceTerms = useRef();

    const initEditorNotes = () => {
        try {
            if (document.getElementById('notes-editorjs')) {
                const editor = new EditorJS({
                    holder: 'notes-editorjs',
                    data: invNotes && JSON.parse(invNotes),
                    onReady: () => {
                        ejInstanceNotes.current = editor;
                        setNotesEditorReady(true);
                    },
                    onChange: async () => {
                        let content = await editor.saver.save();
                        setInvNotes(content);
                    },
                    autofocus: true,
                    tools: {
                        header: {
                            class: Header,
                            inlineToolbar: ['link'],
                        },
                        list: {
                            class: List,
                            inlineToolbar: true,
                        },
                    },
                });
            }
        } catch (error) {
            console.error('Error initializing notes editor:', error);
        }
    }

    const initEditorTerms = () => {
        try {
            if (document.getElementById('terms-editorjs')) {
                const editor = new EditorJS({
                    holder: 'terms-editorjs',
                    data: invTerms && JSON.parse(invTerms),
                    onReady: () => {
                        ejInstanceTerms.current = editor;
                        setTermsEditorReady(true);
                    },
                    onChange: async () => {
                        let content = await editor.saver.save();
                        setInvTerms(content);
                    },
                    autofocus: true,
                    tools: {
                        header: Header,
                        list: List
                    },
                });
            }
        } catch (error) {
            console.error('Error initializing terms editor:', error);
        }
    }

    useEffect(() => {
        let timer;
        
        const cleanupEditors = () => {
            if (ejInstanceNotes.current) {
                ejInstanceNotes.current.destroy();
                ejInstanceNotes.current = null;
                setNotesEditorReady(false);
            }
            if (ejInstanceTerms.current) {
                ejInstanceTerms.current.destroy();
                ejInstanceTerms.current = null;
                setTermsEditorReady(false);
            }
        };

        if (tabActive === "notes") {
            cleanupEditors();
            timer = setTimeout(() => {
                initEditorNotes();
            }, 100);
        } else if (tabActive === "terms") {
            cleanupEditors();
            timer = setTimeout(() => {
                initEditorTerms();
            }, 100);
        }

        return () => {
            if (timer) clearTimeout(timer);
        };
    }, [tabActive]);

    useEffect(() => {
        if (!isInitialized && productList?.length === 0) {
            setProductList([...initalProductItem]);
            setIsInitialized(true);
        }
    }, [isInitialized, productList, initalProductItem]);

    const addItemHandler = (e)=>{
        e.preventDefault()
        setProductList([...productList,{
            index: productList?.length,
            productId: '', 
            productName: '',
            productDesc: '',
            qty:1,
            qtyDays:1,
            price:'0',
            priceUnit: 0,
            productNetTotal: 0,
            productVat: 0,
            productNetTotalWithVat: 0,
            productDiscount: 0
        }]);
    }
    const handleMoveRow = (dragIndex, hoverIndex) => {
        const draggedItem = {...productList[dragIndex]};
        const updatedList = [...productList];
        
         const originalDraggedState = {
            productId: draggedItem.productId,
            productName: draggedItem.productName,
            price: draggedItem.price,
            priceUnit: draggedItem.priceUnit,
            qty: draggedItem.qty,
            qtyDays: draggedItem.qtyDays,
         };
    
         updatedList.splice(dragIndex, 1);
        
         updatedList.splice(hoverIndex, 0, {
            ...draggedItem,
            ...originalDraggedState,
            index: hoverIndex
        });
    
         const finalList = updatedList.map((item, idx) => ({
            ...item,
            index: idx
        }));
    
        setProductList(finalList);
    };
    return (
        <>
        <MwTabView>
            <MwTabViewHead  setTabActive={setTabActive} tabActive={tabActive} tabTitleList={[
                { label:"الاصناف", value:"items"},
                { label:"الملاحظات", value:"notes"},
                { label:"الشروط والاحكام", value:"terms"}
                ]} />
            
            <MwTabViewBody>
                {tabActive === 'items' && (
                    <div id='inv-body' className="max-h-[calc(100vh-275px)] min-h-[calc(100vh-400px)] overflow-y-auto">
                        <div>
                            {productList.map((e,index)=>{
                                return <InvFormListItem
                                    key = {index}
                                    index = {index}
                                    moveRow={handleMoveRow}
                                    reloadProductList = {reloadProductList}
                                    showModalProductHandle = {showModalProductHandle}
                                    productList = {productList}
                                    setProductList = {setProductList}
                                    formErrors = {index ===0 && formErrors}
                                    SETTING_INV_QTY_DAYS = {SETTING_INV_QTY_DAYS}
                                    SETTING_INV_UOM_SHOW = {SETTING_INV_UOM_SHOW}
                                    SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION = {SETTING_SHOW_INVOICE_PRODUCT_DESCRIPTION}
                                    priceListItems = {priceListItems}
                                    />
                            })}
                        </div>
                        <div className='flex justify-end gap-1 m-3'>
                            <MwButton size='sm' actionType="button" type="secondary" onClick={addItemHandler}> اضف بند </MwButton>
                        </div>
                    </div>
                )}
                
                {tabActive === 'notes' && (
                    <div id="notes-editorjs" className="h-full p-4 bg-white rounded-lg min-h-[300px]" />
                )}
                
                {tabActive === 'terms' && (
                    <div id="terms-editorjs" className="h-full p-4 bg-white rounded-lg min-h-[300px]" />
                )}
            </MwTabViewBody>
        </MwTabView>
        </>
    )
}

export default InvFormBody