import React from 'react'
import {  useTranslation } from 'react-i18next';
import {MwInputText, MwTextArea} from '../../ui/';
import CardAmount from '../../ui/CardAmount';
import PromoCodeBox from '../PromoCodeBox';
import TagList from '../../ui/TagList';

const InvFormFooter = ({
    invNotes,
    setInvNotes,
    invTotalAmount,
    totalAmountWithDiscount,
    invCurrency,
    promoCode,
    setPromoCode,
    discount,
    vat,
    netTotal,
    havePromoCode,
    setHavePromoCode,
    promoCodeHandler,
    discountOnchange,
    SETTING_PRICE_INCLUDE_VAT,
    SETTING_INV_CURRENCY,
    tagList,
    setTagList
}) => {
    const [t] = useTranslation('global')
    return (
        <div id='footer-inv' >
                    {/* <MwTextArea 
                        placeholder={t('invoice.enterNotes')}
                        value={invNotes}
                        onChange={(e)=>setInvNotes(e.target.value)}
                        /> */}
                    <div  className='flex md:flex-row flex-col justify-between my-2 '>

                        <TagList
                            tagList = {tagList} 
                            setTagList = {setTagList}
                        />
                        <PromoCodeBox
                            havePromoCode = {havePromoCode}
                            setHavePromoCode = {setHavePromoCode}
                            promoCode = {promoCode}
                            setPromoCode = {setPromoCode}
                            promoCodeHandler = {promoCodeHandler}
                            />
                    </div>
                     

                   
                    
                </div>
    )
}

export default InvFormFooter