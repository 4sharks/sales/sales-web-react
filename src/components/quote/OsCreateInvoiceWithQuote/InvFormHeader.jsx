import {  useTranslation } from 'react-i18next';

import {MwInputText,MwSelector, MwSelectorMulti} from '../../ui';
import { useCallback, useEffect, useState } from 'react';
import { useFetch } from '../../../hooks';

const InvFormHeader = ({
  invNo,
  setInvNo,
  dueDate, setDueDate,
  paymentDays, setPaymentDays,
  RefNo,
  invRef,
  invTitle,
  invDate,
  invDateDue,
  invCurrency,
  customers,
  onClickNewCustomer,
  salesmans,
  onClickNewSalesman,
  customerSelected,
  setCustomerSelected,
  salesmanSelected,
  setSalesmanSelected,
  formErrors,
  initalCustomerName,
  initalSalesmanName,
  initalSalesmanId,
  setInvDateDue,
  setPriceListItems,
  SETTING_PRICE_INCLUDE_VAT,setProductList,SETTING_INVOICE,productList
}) => {
  const [t] = useTranslation('global');
console.log("customers",customers);
    const getPriceListUrl = useCallback(() => {
           if (!customerSelected) return null;
   
            const listPriceId = customers.find(customer => customer._id === customerSelected.id)?.listPriceId || 
                               customerSelected.listPriceId;
           
           return listPriceId 
               ? `${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/inventory/list-price-items/${listPriceId}`
               : null;
       }, [customerSelected, customers]);
       const {
         data: fetchedData,
         loading,
         error,
         refreshHandler
     } = useFetch(getPriceListUrl(), false);
 
     useEffect(() => {
      if (productList && productList.length > 0) {
           const updatedProductList = productList.map(product => {
               if (product.productId === 'section' || !product.productId || product.productId === '0') {
                  return product;
              }
              
               const existingPrice = product.priceUnit || 0;
              
              return {
                  ...product,
                  // الاحتفاظ بالسعر الأصلي
                  originalPriceUnit: product.originalPriceUnit || existingPrice,
                  priceUnit: existingPrice,
                  price: product.qty * existingPrice * (product.qtyDays || 1),
                  productNetTotal: product.productNetTotal || (product.qty * existingPrice * (product.qtyDays || 1)),
                  productVat: product.productVat || ((product.qty * existingPrice * (product.qtyDays || 1)) * (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100),
                  productNetTotalWithVat: product.productNetTotalWithVat || ((product.qty * existingPrice * (product.qtyDays || 1)) * (1 + (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100))
              };
          });
      
          setProductList(updatedProductList);
      }
    }, []);
    
     useEffect(() => {
      if (!customerSelected) {
          return;
      }
    
      const url = getPriceListUrl();
      if (url) {
          refreshHandler();
      }
    }, [customerSelected, getPriceListUrl]);
    
           useEffect(() => {
            if (customerSelected && customerSelected.listPriceId) {
                console.log("defaultcustomerSelected", customerSelected);
                refreshHandler();
            } else if (customerSelected) {
                 console.log("productListosama", productList);
                const defaultPriceList = productList.map(product => {
                    if (product.productId === 'section' || !product.productId || product.productId === '0') {
                        return product;
                    }
    
                    // استخدام originalPriceUnit إذا كان موجود سعر افتراضي ، وإلا نستخدم السعر الأساسي للمنتج
                    const defaultPrice = product.originalPriceUnit || parseFloat(product.priceUnit) || 0;
    
                    return {
                        ...product,
                        priceUnit: defaultPrice,
                        price: product.qty * defaultPrice * (product.qtyDays || 1),
                        productNetTotal: product.qty * defaultPrice * (product.qtyDays || 1),
                        productVat: (product.qty * defaultPrice * (product.qtyDays || 1)) * (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100,
                        productNetTotalWithVat: (product.qty * defaultPrice * (product.qtyDays || 1)) * (1 + (SETTING_INVOICE.INV_VAT_PERCENT || 15) / 100)
                    };
                });
                console.log("defaultPriceList", defaultPriceList);
    
                setPriceListItems([]);
                setProductList(defaultPriceList);
            }
          }, [customerSelected]);
          useEffect(() => {
            if (fetchedData && fetchedData.data) {
                setPriceListItems(fetchedData.data);
                
                if (productList && productList.length > 0 && customerSelected) {
                    const updatedProductList = productList.map(product => {
                        if (product.productId === 'section' || !product.productId || product.productId === '0') {
                            return product;
                        }
                        
                         const priceListItem = fetchedData.data.find(
                            item => item.product_id.trim() === product.productId
                        );
                        
                        if (priceListItem) {
                             const newPrice = SETTING_PRICE_INCLUDE_VAT === 'true' 
                                ? parseFloat(priceListItem.product_sale_price) 
                                : parseFloat(priceListItem.product_sale_price);
                            
                             const totalPrice = product.qty * newPrice * (product.qtyDays || 1);
                            let netTotal = totalPrice;
                            
                             if (product.productDiscount) {
                                if (product.productDiscount.includes('%')) {
                                    netTotal = totalPrice - (totalPrice * parseFloat(product.productDiscount.replace('%', '')) / 100);
                                } else {
                                    netTotal = totalPrice - parseFloat(product.productDiscount || 0);
                                }
                            }
                            
                            const vatPercent = SETTING_INVOICE?.INV_VAT_PERCENT || 15;
                            const productVat = netTotal * vatPercent / 100;
                            
                            return {
                                ...product,
                                priceUnit: newPrice,
                                price: totalPrice,
                                productNetTotal: netTotal,
                                productVat: productVat,
                                productNetTotalWithVat: netTotal + productVat
                            };
                        } else {
                             return product;
                        }
                    });
                    
                    setProductList(updatedProductList);
                }
            }
          }, [fetchedData, SETTING_PRICE_INCLUDE_VAT, SETTING_INVOICE]);
  // دوال التعامل مع "السداد خلال" وتاريخ الاستحقاق
  const calculateDueDate = (days) => {
    const invoiceDateObj = new Date(invDate);
    invoiceDateObj.setDate(invoiceDateObj.getDate() + parseInt(days || 0));
    return invoiceDateObj.toLocaleDateString('en-CA');
  };

  const handlePaymentDaysChange = (e) => {
    const days = e.target.value;
    setPaymentDays(days);
    const newDueDate = calculateDueDate(days);
    setDueDate(newDueDate);
    setInvDateDue(newDueDate);
  };

  const handleDueDateChange = (e) => {
    const selectedDate = e.target.value;
    if (selectedDate) {
      setDueDate(selectedDate);
      setInvDateDue(selectedDate);
      const invoiceDateObj = new Date(invDate);
      invoiceDateObj.setHours(0, 0, 0, 0);
      const dueObj = new Date(selectedDate);
      dueObj.setHours(0, 0, 0, 0);
      const diffTime = dueObj - invoiceDateObj;
      const days = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
      setPaymentDays(days.toString());
    }
  };

  useEffect(() => {
    const defaultDays = 90;
    setPaymentDays(defaultDays.toString());
    const defaultDueDate = calculateDueDate(defaultDays);
    setDueDate(defaultDueDate);
    setInvDateDue(defaultDueDate);
  }, [invDate, setInvDateDue]);

  useEffect(() => {
    console.log("initalSalesmanIds:", initalSalesmanId);
    console.log("salesmans:", salesmans);
    console.log("customerSelected:", customerSelected);
    
     if (!salesmans || salesmans.length === 0) {
      console.log("No salesmans data available");
      return;
    }
    
    const selectedSalesmen = [];
    
     if (Array.isArray(initalSalesmanId) && initalSalesmanId.length > 0) {
      for (const salesmanId of initalSalesmanId) {
        const matchingSalesman = salesmans.find(salesman => salesman._id === salesmanId);
        if (matchingSalesman) {
          selectedSalesmen.push({
            label: matchingSalesman?.fullname && matchingSalesman.fullname[1] ? matchingSalesman.fullname[1].text : '',
            value: matchingSalesman._id,
            ...matchingSalesman
          });
        }
      }
      
      if (selectedSalesmen.length > 0) {
        console.log("Found matching salesmen by initalSalesmanId:", selectedSalesmen);
        setSalesmanSelected(selectedSalesmen);
        return;
      }
    }
    
     if (typeof initalSalesmanId === 'string' && initalSalesmanId) {
      const matchingSalesman = salesmans.find(salesman => salesman._id === initalSalesmanId);
      if (matchingSalesman) {
        selectedSalesmen.push({
          label: matchingSalesman?.fullname && matchingSalesman.fullname[1] ? matchingSalesman.fullname[1].text : '',
          value: matchingSalesman._id,
          ...matchingSalesman
        });
        
        console.log("Found matching salesman by initalSalesmanId string:", selectedSalesmen);
        setSalesmanSelected(selectedSalesmen);
        return;
      }
    }
    
     if (customerSelected) {
       const customerId = customerSelected.id || (customerSelected._id ? customerSelected._id : null);
      if (customerId) {
         const fullCustomer = customers.find(c => c._id === customerId);
        if (fullCustomer && fullCustomer.salesmanId) {
          const matchingSalesman = salesmans.find(salesman => salesman._id === fullCustomer.salesmanId);
          if (matchingSalesman) {
            selectedSalesmen.push({
              label: matchingSalesman?.fullname && matchingSalesman.fullname[1] ? matchingSalesman.fullname[1].text : '',
              value: matchingSalesman._id,
              ...matchingSalesman
            });
            
            console.log("Found matching salesman by customerSelected.id lookup:", selectedSalesmen);
            setSalesmanSelected(selectedSalesmen);
            return;
          }
        }
      }
    }
    
     console.log("No matching salesmen found");
    setSalesmanSelected([]);
  }, [initalSalesmanId, salesmans, customerSelected, customers]);

 

  return (
    <div className="flex flex-col md:flex-row justify-between items-center mb-4">
      <div className="flex w-full md:w-fit">
        <div className="flex-1 md:w-24">
          <MwInputText 
            label={t('invoice.invoiceNo')}
            id="inv_no"
            value={invNo}
            disabled={true}
            onChange={(e)=>setInvNo(e.target.value)}
          />
        </div>
        <div className="flex-1 md:w-20">
          <MwInputText 
            label={t('invoice.invoiceRef')}
            value={RefNo}
            disabled={true}
          />
        </div>
      </div>
      <div className="flex w-full md:w-fit">
        <div className="  md:w-36">
          <MwInputText 
            label={t('invoice.invoiceDate')}
            id="inv_date"
            value={invDate}
            invalid={!!formErrors?.invDate}
            disabled={true}
          />
        </div>
        <div className="  md:w-20">
          <MwInputText 
            label={t('invoice.paymentWithin') || "السداد خلال"}
            id="payment_days"
            inputType="number"
            placeholder="عدد الأيام"
            value={paymentDays}
            onChange={handlePaymentDaysChange}
          />
        </div>
        <div className="  md:w-28">
          <MwInputText 
            label={t('invoice.invoiceDateDue')}
            inputType="date"
            id="inv_date_due"
            value={dueDate}
            invalid={!!formErrors?.invDateDue}
            onChange={handleDueDateChange}
          />
        </div>
      </div>
                <div className="relative flex-1  flex w-full ">
                    <div className="  flex-1 flex  flex-col ">
                        <MwSelector 
                            label='العميل'
                            initalValue={initalCustomerName}
                            _data={customers} 
                            dataType='customer' 
                            onClickNew = {onClickNewCustomer}
                            selectedItem={customerSelected}
                            setSelectedItem = {setCustomerSelected}
                            defaultValue={initalCustomerName}
                            />
                    </div>
                    <div className="flex-1 flex flex-col ">
                    <MwSelectorMulti
      label='المندوب'
      defaultValue={salesmanSelected}  
      _data={salesmans}
      dataType='salesman'
      onClickNew={onClickNewSalesman}
      selectedItem={salesmanSelected}
      setSelectedItem={setSalesmanSelected}
      isMulti={true}  
    />
                    </div>
                </div>
              
            </div>
        
    )
}

export default InvFormHeader