import { useState,useRef,useEffect } from "react";
import InvoiceHeader from "./show/InvoiceHeader"
import InvoiceBody from "./show/InvoiceBody"
import { useNavigate, useParams } from "react-router-dom";
import { useCurrent, useFetch, useInvoice } from "../../hooks";
import { useSelector } from "react-redux";
import {  useTranslation } from 'react-i18next';


import {getCompanyById} from '../../utils/tenantManager.js'
import {arrayNameByLang,printableDiv} from '../../utils/global.js'
const ShowPrintComponent = ({isPrint=false,isDelivery=false}) => {
    const divPrintRef = useRef();

    const [t] = useTranslation('global')
    const params = useParams();
    const navigate = useNavigate();
    const {formatter} = useInvoice();

    const [showConfirmModal,setShowConfirmModal] = useState(false);
    const {currentTenantId,currentCompanyId,currentBranchId,currentLangId} = useCurrent();

    const lang = useSelector(state => state.lang);
    const invoiceSettings = useSelector(state => state.settings).quoteSettings;
    const company = getCompanyById(params.companyId);
    const companyName =  arrayNameByLang(company?.name,lang.value)?.text || arrayNameByLang(company?.name,'ar')?.text;
    const companyDesc =  arrayNameByLang(company?.desc,lang.value)?.text;
    const companyAddress =  arrayNameByLang(company?.mainAddress,lang.value)?.text;
    const companyPhone =  company?.mainPhone;
    const companyEmail =  company?.mainEmail;
    const vatNo = company.vatNumber;
    const commercialRegisteration = company?.commercialRegisteration;
    const invTerms = invoiceSettings.INV_TERMS_PRINT
    const SETTING_SHOW_TERMS_ON_PRINT = invoiceSettings.SHOW_TERMS_ON_PRINT
    const INV_CURRENCY = invoiceSettings?.INV_CURRENCY
    const SETTING_INV_CURRENCY = INV_CURRENCY  || 'SAR'

    const {data:invData,loading:invLoading,error} = useFetch(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/quotes/show/${params.id}`)

    const {data:customerData,fetchData:fetchCustomer,} = useFetch(`${process.env.REACT_APP_Auth_API_BASE_URL}/customers/show/${currentTenantId}/${currentCompanyId}/${currentBranchId}/${invData?.customer_id}`,false);


    useEffect(() => {
        if(invData){
            invData?.customer_id && fetchCustomer();
        }
    },[invData])

    const printAction = () => {
        const PAGE_HEIGHT = 1045
  const printElement = document.getElementById("print-component")
  if (printElement) {
      printElement.classList.add("temp-class-for-height")
      const height = printElement.clientHeight
      const numberOfPage = Math.ceil(height / PAGE_HEIGHT)
      const heightWithSingleHeader = numberOfPage*PAGE_HEIGHT
      let requiredHeight = heightWithSingleHeader
      if (numberOfPage > 1) {
          const headerHeight = printElement.getElementsByTagName("thead")?.[0]?.clientHeight
          const footerHeight = printElement.getElementsByTagName("tfoot")?.[0]?.clientHeight
          requiredHeight -= (numberOfPage - 1) * (headerHeight + footerHeight) 
      }
      printElement.style.height = `${requiredHeight}px`
      printElement.classList.remove("temp-class-for-height")
  }
  window.print()
  if (printElement) {
      printElement.style.height = `auto`
  }
    }
    return (<>
        <button className={"print-preview-button"} onClick={printAction}>{"Print Preview"}</button>
        {customerData?.data &&  <table className="print-component">
            <thead>
                <tr>
                <th className="header-print">
                <InvoiceHeader
                    logo = {company.logo} 
                    companyName={companyName}
                    companyPhone = {companyPhone}
                    companyEmail = {companyEmail}
                    companyDesc={companyDesc}
                    vatNo = {vatNo}
                    commercialRegisteration = {commercialRegisteration}
                    inv_no = {invData.inv_no}
                    inv_title = {invData.title}
                    inv_date = {invData.inv_date}
                    inv_date_due = {invData.inv_date_due}
                    companyAddress = {companyAddress}
                    invoiceSettings = {invoiceSettings}
                />
                </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td className="">
                <InvoiceBody
                            customerName = {invData.customer_name}
                            customer = {customerData?.data}
                            salesmanName = {invData.salesman_name}
                            invProducts = {invData.quote_details}
                            isDelivery = {isDelivery}
                            formatter = {formatter}
                            SETTING_INV_CURRENCY = {SETTING_INV_CURRENCY}
                            
                        />
                </td>
                </tr>
            </tbody>
            <tfoot className="footer-print">
                <tr>
                <td>
                {"Page footer"}
                </td>
                </tr>
            </tfoot>
        </table>}
        </>
    )
}

export default ShowPrintComponent