import React, { useEffect, useState, useRef } from 'react'
import { useNavigate,useParams } from 'react-router-dom'
import { useSelector } from 'react-redux';
import axios from 'axios';
import { AiOutlinePrinter,AiOutlineEdit } from 'react-icons/ai';
import { MdOutlinePaid } from 'react-icons/md';

import {getCompanyById} from '../../utils/tenantManager.js'
import {arrayNameByLang,printableDiv} from '../../utils/global.js'
import { useFetch,useLogs,useInvoice, useCurrent } from '../../hooks';
import { ConfirmDelete,MwButton, MwSpinner } from '../ui';
import InvoiceHeader from './show/InvoiceHeader.jsx';
import InvoiceBody from './show/InvoiceBody.jsx';
import InvoiceFooter from './show/InvoiceFooter.jsx';
import {  useTranslation } from 'react-i18next';
import '../../handle-head-list.css';
import { Page, Text, View, Document, StyleSheet, PDFViewer, Image,Font } from '@react-pdf/renderer';

const ShowInvoicePdf = ({isPrint=false,isDelivery=false}) => {
    const divPrintRef = useRef();

    // const fontPath = path.resolve('/fonts/Rubik-VariableFont_wght.ttf'); // Set you own path here

    Font.register({
    family: 'Rubik',
    src: '/assets/fonts/Rubik-VariableFont_wght.ttf',
    });
    const [t] = useTranslation('global')
    const params = useParams();
    const navigate = useNavigate();
    const {formatter} = useInvoice();

    const [showConfirmModal,setShowConfirmModal] = useState(false);
    const {currentTenantId,currentCompanyId,currentBranchId,currentLangId} = useCurrent();

    const lang = useSelector(state => state.lang);
    const invoiceSettings = useSelector(state => state.settings).quoteSettings;
    const company = getCompanyById(params.companyId);
    const companyName =  arrayNameByLang(company?.name,lang.value)?.text || arrayNameByLang(company?.name,'ar')?.text;
    const companyDesc =  arrayNameByLang(company?.desc,lang.value)?.text;
    const companyAddress =  arrayNameByLang(company?.mainAddress,lang.value)?.text;
    const companyPhone =  company?.mainPhone;
    const companyEmail =  company?.mainEmail;
    const vatNo = company.vatNumber;
    const commercialRegisteration = company?.commercialRegisteration;
    const invTerms = invoiceSettings.INV_TERMS_PRINT
    const SETTING_SHOW_TERMS_ON_PRINT = invoiceSettings.SHOW_TERMS_ON_PRINT
    const INV_CURRENCY = invoiceSettings?.INV_CURRENCY
    const SETTING_INV_CURRENCY = INV_CURRENCY  || 'SAR'
    const {resultLogsPost,loadingLogsPost,errorLogsPost,postDataHandler} = useLogs()

    const {data:invData,loading:invLoading,error} = useFetch(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/quotes/show/${params.id}`)
    const {data:customerData,fetchData:fetchCustomer,} = useFetch(`${process.env.REACT_APP_Auth_API_BASE_URL}/customers/show/${currentTenantId}/${currentCompanyId}/${currentBranchId}/${invData?.customer_id}`,false);
    // Create styles
    const styles = StyleSheet.create({
        page: {
          flexDirection: 'columns',
          backgroundColor: '#E4E4E4',
          fontFamily: 'Rubik',
          fontSize: '12px',
          padding: '10px',
        },
        row: {
            flexDirection: 'row',
            gap:'20px'
        },
        columns: {
            flexDirection: 'columns',
            gap:'10px',
        },
        columnsSm: {
            flexDirection: 'columns',
            gap:'1px',
            padding: '20px' 
        },
        section: {
          margin: 10,
          padding: 10,
          flexGrow: 1
        }
      });

    useEffect(() => {
        if(invData){
            invData?.customer_id && fetchCustomer();
        }
    },[invData])
    
    const setLogsHandler =  (itemEn='delete',itemAr='حذف') => {
        const _data = {
            moduleName: "SALES",
            resourceName: "QUOTES",
            eventName:"DELETE",
            logContentEn: `${itemEn} quote No: ${invData.inv_no}`,
            logContentAr: `${itemAr} عرض سعر برقم : ${invData.inv_no}`,
        }
        postDataHandler(_data);
    }

    const deleteConfirm = async() => {
        const urlDeleteReq = `${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/quotes/delete/${params.id}`;
        const res = await axios.delete(urlDeleteReq);
        if(res.data){
            setLogsHandler();
            navigate(`/${params.tenant}/quotes`);
        }
        console.log(`Error deleting`);
    }

    const deleteHandler = () => {
        setShowConfirmModal(true);
    }

    const handlePrint = () => {
        setLogsHandler('print','طباعة');
        printableDiv(divPrintRef,lang?.value)
    };
    
    return (
        <>
            { !invLoading  ? 
            <div className='h-full'>
                <ConfirmDelete
                    title={`هل انت متاكد من حذف عرض السعر`}
                    msg={`هل انت متاكد من حذف عرض السعر`}
                    onShow={showConfirmModal}
                    setOnShow={setShowConfirmModal}
                    onDeleteHandler={deleteConfirm}
                />
                <div className='flex py-2 '>
                    <MwButton 
                        size = 'sm'
                        inGroup = {true}
                        type = 'secondary' 
                        onClick={handlePrint}>
                            <AiOutlinePrinter size={16}/> طباعة
                    </MwButton>
                    <MwButton 
                            size = 'sm'
                            inGroup = {true}
                            type = 'secondary-inline-end' 
                            onClick={()=>{
                                navigate(`/${params.tenant}/quotes/edit/${params.id}`)
                            }}>
                                <AiOutlineEdit size={16}/> تعديل 
                        </MwButton>
                    <MwButton 
                            size = 'sm'
                            inGroup = {true}
                            type = 'deleteBtn-inline' 
                            onClick={  deleteHandler }>
                                <AiOutlinePrinter size={16}/> حذف 
                    </MwButton>
                    <MwButton 
                            size = 'sm'
                            inGroup = {true}
                            type = 'saveBtn' 
                            onClick={ () =>{} }>
                                <MdOutlinePaid size={16}/> انشاء فاتورة لعرض السعر
                        </MwButton>
                </div>
                <div className='bg-slate-50 p-3 mt-2 rounded-lg h-full border-2'>
                    { 
                    customerData?.data && 
                    <PDFViewer width={'100%'} height={'100%'}>
                        <Document language='en' >
        <Page size="A4" style={styles.page}>
            <View fixed >
            <Text>header #1</Text>
            </View>
            <View style={styles.row} >
                <View style={styles.section}>
                    {invoiceSettings?.SHOW_LOGO_ON_PRINT === 'true' && <div><Image src={company.logo} style={{width:'220px'}} /></div> }
                </View>
                <View style={styles.columns}>
                    <Text style={{fontWeight:'bold', textAlign:'right' }} >{companyName}</Text>
                    <View style={{flexDirection:'row-reverse',justifyContent:'space-between',gap:'10px'}} >
                        <Text style={{textAlign:'right'}}>   سجل تجاري</Text>
                        <Text style={{fontWeight:'bold'}}>{commercialRegisteration}</Text>
                        <Text style={{textAlign:'left'}}>C.R </Text>
                    </View>
                    <View style={{flexDirection:'row-reverse',justifyContent:'space-between',gap:'10px'}}>
                        <Text>  الرقم الضريبي</Text>
                        <Text style={{fontWeight:'bold'}}>{vatNo}</Text>
                        <Text>VAT NO</Text>
                    </View>
                    { companyAddress && 
                    <View  style={{flexDirection:'row-reverse',justifyContent:'space-between',gap:'10px'}}>
                        <Text>   العنوان</Text>
                        <Text style={{fontWeight:'bold'}}>{companyAddress}</Text>
                        <Text>Address</Text>
                    </View>
                    }
                </View>
            </View>
            { invData.title && 
            <View  style={{flexDirection:'row-reverse',justifyContent:'space-between',gap:'10px'}}>
                <View style={styles.columnsSm}>
                    <Text>اسم المشروع </Text>
                    <Text> PROJECT NAME</Text>
                </View>
                <Text style={{fontWeight:'bold' , flexGrow:1}}>{invData.title}</Text>
            </View>
            }
    </Page>
    </Document>
    </PDFViewer>}
                </div>
            </div> : 
            <MwSpinner/> 
            }
        </>
    )
}

export default ShowInvoicePdf