import React, { useRef ,useEffect} from 'react'
import {  useTranslation } from 'react-i18next';
import {MwInputText, MwTextArea} from '../../ui/';
import CardAmount from '../../ui/CardAmount';
import PromoCodeBox from '../PromoCodeBox';
import TagList from '../../ui/TagList';
import {amountFormat} from '../../../utils/invoceHelper';

const InvFormFooter = ({
    invNotes,
    setInvNotes,
    invTotalAmount,
    totalAmountWithDiscount,
    promoCode,
    setPromoCode,
    discount,
    vat,
    netTotal,
    havePromoCode,
    setHavePromoCode,
    promoCodeHandler,
    discountOnchange,
    SETTING_PRICE_INCLUDE_VAT,
    SETTING_INV_CURRENCY,
    tagList,
    setTagList,
    disabledDiscount,
    formatter
}) => {
    const [t] = useTranslation('global')


    console.log('footer => totalAmountWithDiscount',totalAmountWithDiscount)
    return (
        <div id='footer-inv' className='py-2' >
                        {/* <MwTextArea 
                            placeholder={t('invoice.enterNotes')}
                            value={invNotes}
                            onChange={(e)=>setInvNotes(e.target.value)}
                            /> */}
                    <div  className='flex md:flex-row flex-col justify-between '>

                        <TagList
                            tagList = {tagList} 
                            setTagList = {setTagList}
                        />
                        <PromoCodeBox
                            havePromoCode = {havePromoCode}
                            setHavePromoCode = {setHavePromoCode}
                            promoCode = {promoCode}
                            setPromoCode = {setPromoCode}
                            promoCodeHandler = {promoCodeHandler}
                            />
                    </div>
 
                   
                     {/* <div className='pt-2 flex flex-col justify-center items-center'>
                        { SETTING_PRICE_INCLUDE_VAT === 'true' ?
                            <div className='flex justify-center items-center text-xs text-slate-400 '>{t('invoice.priceIncludesVat')}</div> : 
                            <div className='text-xs text-slate-400 text-center'>{t('invoice.priceNotIncludesVat')}</div> 
                            
                        }
                    </div> */}
                </div>
    )
}

export default InvFormFooter