import {
    Chart as ChartJS,
    LineElement,
    PointElement,
    CategoryScale,
    LinearScale,
    Tooltip,
    Legend
} from 'chart.js';
import {Chart, Line} from 'react-chartjs-2'
import { MwSpinnerButton } from '../ui';
import { useTranslation } from 'react-i18next';


ChartJS.register(
    LineElement,
    PointElement,
    CategoryScale,
    LinearScale,
    Tooltip,
    Legend
)

const LineChart = ({datasetData,loading,className}) => {
        const [t] = useTranslation('global')
    
    const data = {
        labels: [ t("DashBoard.months.january"),
            t("DashBoard.months.february"),
            t("DashBoard.months.march"),
            t("DashBoard.months.april"),
            t("DashBoard.months.may"),
            t("DashBoard.months.june"),
            t("DashBoard.months.july"),
            t("DashBoard.months.august"),
            t("DashBoard.months.september"),
            t("DashBoard.months.october"),
            t("DashBoard.months.november"),
            t("DashBoard.months.december")],
        datasets:[
            {
                label:t("DashBoard.currentYearSales") ,
                data:datasetData,
                borderColor: 'gray',
                backgroundColor: 'indigo',
                tension:0.4,

            }
        ]
    }
    const options = {
        plugins: {
            // defaultFontFamily:'"Tajawal", sans-serif',
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    font: {
                        size: 14,
                        family: '"Tajawal", sans-serif',
                    }
                }
            }
        }
    }

    return (
        <div className='rounded-lg bg-slate-50  flex-1 border-2 border-slate-50 flex items-center justify-center md:px-12 '>
            { !loading ? <Line className={` ${className}`}
            data={data}
            options={options}
            >
            </Line> :  <MwSpinnerButton withLabel={false} isFullCenter={true} />}
        </div>
            
    )
}

export default LineChart