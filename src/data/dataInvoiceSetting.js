export const dataInvoiceSettings =
[
    {
        "name": "SHOW_ADDRESS_ON_PRINT",
        "value": "true"
    },
    {
        "name": "SHOW_PHONE_ON_PRINT",
        "value": "false"
    },
    {
        "name": "SHOW_QR_KSA_ON_PRINT",
        "value": "true"
    },
    {
        "name": "SHOW_VAT_ON_PRINT",
        "value": "true"
    },
    {
        "name": "SHOW_LOGO_ON_PRINT",
        "value": "false"
    },
    {
        "name": "SHOW_TERMS_ON_PRINT",
        "value": "true"
    },
    {
        "name": "SHOW_EMAIL_ON_PRINT",
        "value": "true"
    },
    {
        "name": "TYPE_INV_PRINT",
        "value": "sm"
    },
    {
        "name": "INV_TERMS_PRINT",
        "value": "[{\"text\":\"terms\" , \"lang\" : \"en\"},{\"text\":\"يحق للعميل رفع طلب استبدال او استرجااع للمنتج في حالة وجود اي مشكله في المنتج وتعذر فريق الصيانه صيانتها\" , \"lang\" : \"ar\"}]"
    },
    {
        "name": "PRODUCT_LEVEL",
        "value": "TenantAndCompany"
    },
    {
        "name": "PRICE_INCLUDE_VAT",
        "value": "false"
    },
    {
        "name": "INV_NO_FORMAT",
        "value": "UserAndNumber"
    },
    {
        "name": "INV_NO_SEPERATE",
        "value": "-"
    },
    {
        "name": "INV_PREFIX",
        "value": "inv-"
    },
    {
        "name": "INV_VAT_PERCENT",
        "value": "15"
    },
    {
        "name": "INV_TO_FIXED_NUMBER",
        "value": "1"
    },
    {
        "name": "INV_ITEMS_ROW_NUMBER",
        "value": "1"
    },
    {
        "name": "INV_NO_START",
        "value": "100"
    },
    {
        "name": "INV_CURRENCY",
        "value": "[ {\"code\" : \"SAR\" , \"lang\" : \"en\" } , {\"code\" : \"ر.س\" , \"lang\" : \"ar\" }]"
    }
]