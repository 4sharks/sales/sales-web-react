# Use an official Node.js runtime as the base image
FROM node:14-alpine AS builder

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install project dependencies
#RUN npm ci --quiet
RUN npm install --quiet


# Copy the entire project to the container
COPY . .

# Build the React application
RUN npm run build

# Use a lightweight HTTP server to serve the built React app
FROM nginx:alpine 

# Copy the built app to the Nginx public directory
COPY --from=builder /app/build /usr/share/nginx/html/

# Expose port 80
EXPOSE 3010

# Start Nginx when the container starts
CMD ["nginx", "-g", "daemon off;"]